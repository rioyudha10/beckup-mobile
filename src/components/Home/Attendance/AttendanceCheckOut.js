import React, { Component } from 'react';
import {StyleSheet, View, Text, Dimensions, Button, TouchableOpacity, ScrollView, Image, 
  AsyncStorage, Animated, Easing, ImageBackground,TextInput} from 'react-native';
import { Navigation } from 'react-native-navigation';
import * as axios from 'axios';
import { TextField } from 'react-native-material-textfield';
import IconFA from 'react-native-vector-icons/FontAwesome';
import VIcon from '../../../styles/VIcon';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action/index';
import { Fumi } from 'react-native-textinput-effects';
import CompressImage from 'react-native-compress-image';
import LottieView from 'lottie-react-native';
import {anim_gratika_red, background_image} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import ImagePicker from 'react-native-image-picker';
import {openLightbox, openLightboxLoading} from '../HomeNav';
import moment from 'moment'
import Loading from 'react-native-whc-loading';


var screen = Dimensions.get('window');

export class AttendanceCheckOut extends Component {
  constructor(props){
    super(props);
    this.state = {
      latitude: '',
      longitude: '',
      error: '',
      place: '',
      lineWidth: 0,
      imageRect: require('./../../../images/grey-rec.jpg'),
      img_status: '0',
      img_uri: '',
      keterangan: '',
      respon_msg: '',
      date : ''
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  static navigatorStyle = {
    tabBarHidden: true,
    tabBarShowLabels: 'hidden',
  }

  onLottieLoad = () => {
    console.log('play lottie');
    this.animation.play();
  }
  
  // componentWillUnmount(){
  //   this.props.onUnmount();
  // }

  onNavigatorEvent = (event) => {
    console.log(JSON.stringify(event));
    if (event.type == 'NavBarButtonPress') {
      if(event.id == 'submit'){
          // this.addLogbookAPI();
          this.compressPicture()
      }
    }
  }

  openCamera(index){
   var options = {
    title: 'Select Picture',
    storageOptions: {
      skipBackup: true,
      path: 'images'
    }
    };
    ImagePicker.launchCamera(options, (response)  => {
        console.log('Response = ', response);
        if (response.didCancel) {
            console.log('User cancelled image picker');
        }
        else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        }
        else {
        let source = response.uri;
        let path = response.path;
        console.log("response uri = "+source);
        console.log("response path = "+path);
        console.log("response AJA = "+JSON.stringify(response));
        this.setState({
            isImage: true,
            imageUri: source,
            img_uri: 'file://'+path
        });
        }
    });
    console.log("image uri = "+this.state.img_uri);
  }

  showToken(){
    this.loadImage();
    console.log(this.state.img_uri);
  }

  componentDidMount() {
    this.refs.loading1.show();
    this.loadImage();
    this.getGeocode();
    this.setState({
      //Setting the value of the date time
      date: moment().format(' dddd MMM Do YYYY, h:mm:ss a')
    });
  }

  async getImgStatus(){
    try{
      this.setState({img_status: await AsyncStorage.getItem('image_status')});
      console.log("hasil camera image status = "+this.state.img_status);
    } catch(error){
      console.log(error);
    }
  }

  async loadImage(){
    try{
      this.setState({img_uri: await AsyncStorage.getItem('image_uri')});
    } catch(error){
      console.log(error);
    }
  }

  getGeocode() {
    axios.get(this.props.apiState.apiReverseLocation+ this.props.locationState.lat +','+ this.props.locationState.long +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
   .then(response => {
     console.log(response);
      this.setState({
        place: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
      });
      this.refs.loading1.close();
    }).catch((error) => { // catch is called after then
      this.refs.loading1.close();
      const data = {
        act: true,
        goAct: ()=>this.closeBack(),
        message: error.message,
        confirm: 'OK',
        status: -1
      }
      openLightbox(data, 500, this.props.navigator);
    });
  }

  compressPicture = () =>{
    const {keterangan, img_uri} = this.state;
    this.refs.loading1.show();

    if(keterangan === "" || img_uri === null){
      console.log("Validation null")
      const data = {
        act: false,
        message: 'Please Complete All The Field',
        confirm: 'OK',
        status: 0
      }
      openLightbox(data, 0, this.props.navigator);
    }else{
      console.log("Validation input")
      CompressImage.createCompressedImage(this.state.img_uri, "Picture").then((responseCompress) => {
        this.storePicture(responseCompress.uri);
      }).catch((err) => {
        this.props.actions.setLoading(false);
        console.log('err = '+JSON.stringify(err));
        const data = {
          act: true,
          goAct: ()=>this.closeBack(),
          message: 'Error Compress Image',
          confirm: 'OK',
          status: -1
        }
        openLightbox(data, 500, this.props.navigator);
      });
    }
  }

  async storePicture(imageCompress){
    console.log('img_uri = '+imageCompress);
    this.refs.loading1.show();
    if(this.state.place){
      if (this.state.img_uri && this.state.keterangan) {
        // openLightboxLoading(this.props.actions, this.props.navigator);
        console.log("image ada isinya");
        // Create the form data object
        var image_name = this.props.profileState.nik+"_"+this.state.img_uri.substring(this.state.img_uri.lastIndexOf("/")+1);
        var data = new FormData();
        data.append('keyapi', this.props.apiState.keyApiAbsen);
        data.append('dataapi', 5);
        data.append('image', {uri: imageCompress, name: image_name, type: 'image/jpg'});
        data.append('caption', this.state.keterangan);
        data.append('latitude', this.props.locationState.lat);
        data.append('longitude', this.props.locationState.long);
        data.append('nik', this.props.profileState.nik);
  
        const config = {
          headers: { 'content-type': 'multipart/form-data' },
          timeout: 10000
        }
        var res_msg;
        axios.post(this.props.apiState.apiAbsen, data, config)
         .then(response => {
           console.log("response stringify "+JSON.stringify(response));
           this.setState({
             keterangan: '',
             img_uri: null,
             img_status: '0',
             respon_msg: response.data.info
            });
            this.refs.loading1.close();
            try {
              AsyncStorage.setItem('image_uri', '');
              //this.getToken();
            } catch (error) {
              console.log("error nullified image uri");
              this.setState({ error: error });
              console.log("error " + error);
            }
           //alert(response.data.message);
            const data = {
              act: true,
              goAct: ()=>this.closeBack(),
              message: response.data.info,
              confirm: 'OK',
              status: 0
            }
            openLightbox(data, 500, this.props.navigator);
          })
         .catch((error) => {
           //console.log(error);
           console.log("================================================");
           console.log("Error gelondongan = "+error); //ini yang bener
           console.log("================================================");
           console.log("ERRORNYA _response = "+error.request._response);
           console.log("================================================");
           this.refs.loading1.close();
           const data = {
              act: true,
              goAct: ()=>this.closeBack(),
              message: response.data.info,
              confirm: 'OK',
              status: 0
            }
            openLightbox(data, 500, this.props.navigator);
          });
      } else{
        const data = {
          act: false,
          message: 'Please Complete All The Field',
          confirm: 'OK',
          status: 0
        }
        openLightbox(data, 0, this.props.navigator);
      }
    } else{
      const data = {
        act: true,
        goAct: ()=>this.closeBack(),
        message: "Location can't found, turn on GPS",
        confirm: 'OK',
        status: -1
      }
      openLightbox(data, 0, this.props.navigator);
    }
  }

  closeBack(){
    const {navigator} = this.props;
    this.props.navigator.popToRoot({
        //screen: 'panelg.attendance.menu',
        animated: true, // does the pop have transition animation or does it happen immediately (optional)
        animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
    });
    this.props.navigator.handleDeepLink({
        link: 'tab3/in/any/format',
        payload: '' // (optional) Extra payload with deep link
    });
  }
  
  render() {
    return (
      <ImageBackground
        source={background_image}
        style={GeneralStyle.imageBackground}
      >
       <Loading ref='loading1' image = {require('../../image/loading.png')}/>
      <ScrollView keyboardShouldPersistTaps="always" 
        style={GeneralStyle.scrollContainerAttendance}>
        <Text style={styles.welcome}>{this.state.date}</Text>
          <Text style={styles.welcome_1}>{this.state.place}</Text>
          <Text style={styles.welcome_2}>Keterangan</Text>
          <View style={styles.textAreaContainer} >
            <TextInput
              style={styles.textArea}
                underlineColorAndroid="transparent"
                placeholderTextColor="grey"
                numberOfLines={10}
                multiline={true}
                value={this.state.keterangan}
                onChangeText={(val) => this.setState({ keterangan: val })}
              />
          </View>
          {/* <TextField 
            label='Location'
            value={this.state.place}
            multiline={true}
            editable={false}
            lineWidth={this.state.lineWidth}
          /> */}

        {/* <Fumi
            label={'Description'}
            iconClass={IconFA}
            iconName={'edit'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={this.state.keterangan}
            onChangeText={(val) => this.setState({ keterangan: val })}
          /> */}

          <View style={{paddingTop:20, justifyContent: 'center',
                alignItems: 'center',}}>
                <TouchableOpacity onPress={this.openCamera.bind(this, 1)}>
                {this.state.img_uri ? 
                  <Image style={GeneralStyle.imageCameraAttendance} source={{uri:this.state.img_uri}}/> : 
                  <View style={GeneralStyle.iconCameraAttendance}>
                    <IconFA name="camera" size={70} color="#4F8EF7" />
                  </View>}
                </TouchableOpacity>
                {/* <View style={GeneralStyle.buttonContainerAttendanceCheck}>
                  <TouchableOpacity style={GeneralStyle.buttonSubmitAttendance} onPress={this.compressPicture}>
                    <Text style={GeneralStyle.textButtonSubmitAttendance}>Submit</Text>
                  </TouchableOpacity>
                </View> */}
          </View>
          {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
      
      </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  welcome: {
    backgroundColor: '#FFFFFF',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 15,
    color: 'grey', 
    paddingTop: 10,
    paddingBottom : 10,
    paddingLeft : 10,  
  },
  welcome_1: {
    backgroundColor: '#FFFFFF',
    fontSize: 18,
    color: 'black', 
    textAlign: 'center',
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom : 10,
    paddingLeft : 10,    
    borderBottomWidth: 1,
    borderColor : 'grey',
  },
  welcome_2: {
    backgroundColor: '#FFFFFF',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 15,
    fontWeight: 'bold',
    color: 'black', 
    paddingTop: 10,
    paddingBottom : 10,
    paddingLeft : 10,  
  },
  textAreaContainer: {
    borderColor: 'grey',
    borderWidth: 1,
    padding: 5
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start"
  }
});


function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        locationState: state.location,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceCheckOut);
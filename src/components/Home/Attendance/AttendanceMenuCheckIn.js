import React, { Component } from 'react';
import { AppRegistry, View, StatusBar, Button, Text, StyleSheet, TextInput, 
    Dimensions, AsyncStorage, TouchableOpacity, Animated, Easing, ImageBackground,  ScrollView } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import{Fab} from 'native-base';
import MapView from 'react-native-maps';
var screen = Dimensions.get('window');
import Geolocation from 'react-native-geolocation-service';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import {BoxShadow} from 'react-native-shadow';
import LottieView from 'lottie-react-native';
import JailMonkey from 'jail-monkey';
import DeviceInfo from 'react-native-device-info';
import { AppInstalledChecker, CheckPackageInstallation } from 'react-native-check-app-install';
import anim from '../../../images/bouncy_mapmaker.json';
import animCurrentLocation from '../../../images/location.json';
import {anim_gratika_red, background_image, iconMenuSize} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import {openLightbox, openLightboxLoading} from '../HomeNav';
import VIcon from '../../../styles/VIcon';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Fumi } from 'react-native-textinput-effects';
import moment from 'moment'
import Loading from 'react-native-whc-loading';
import axios from 'axios'


let topNotif = null;
topNotif = [{
    screenId: 'panelg.attendance.cuti',
    title: 'Cuti',
}, {
    screenId: 'panelg.attendance.izin',
    title: 'Izin',
}];

export class AttendanceMenuCheckIn extends Component {    
    constructor(props){
        super(props);
        this.state = {
            mapRegion: {
                latitude: 0.0,
                longitude: 0.0,
                latitudeDelta: 0.0018,
                longitudeDelta: 0.0018,
            },
            cur_lat: null,
            cur_long: null,
            lastLat: null,
            lastLong: null,
            error: '',
            error_msg: '',
            date: '',
            region_change: false,
            count_region_change: 0,
            tanggal_in : "",
            tanggal_out : "",
            checkin : "-",
            checkout : "-",
            lembur : "-",
            cuti : "-",
            izin : "-",
            status_in : '',
            status_out : ''

        };
    }

    onLottieLoad = () => {
        console.log('play lottie');
        this.animation.play();
    }

    onRegionChange(lastLat, lastLong) {
        this.setState({
          lastLat: lastLat || this.state.lastLat,
          lastLong: lastLong || this.state.lastLong
        });
      }

    static navigatorStyle = {
        tabBarHidden: true
    }

    // async storeLocation(_latitude, _longitude) {
    //     try {
    //         await AsyncStorage.setItem('latitude', ""+_latitude);
    //         await AsyncStorage.setItem('longitude', ""+_longitude);
    //     } catch (error) {
    //         console.log("something went wrong");
    //         this.setState({ error: error });
    //         console.log("error " + error);
    //     }
    // }

    componentDidMount() {
        this.refs.loading1.show();
        this.findLocation();
        // this.coba()
        this.menuBox_1()
        this.menuBox_2()
        this.menuBox_3()
        this.checkInButton()
        this.checkOutButton()
        this.lemburButton()
        this.getDetailStatus()
        // this.izinButton()
        setInterval (()=>{
            this.setState({
                //Setting the value of the date time
                date: moment().format(' dddd MMM Do YYYY, h:mm:ss a')
            });
        },1000)
    }

    // izinButton(){
    //     return(
    //         <View>
    //             <TouchableOpacity style={GeneralStyle. izinButton}   onPress={this.gotoMenu.bind(this, 3)}>>
    //                 <VIcon type={VIcon.TYPE_FONTAWESOME} name="file" size={28} color='#FFFFFF' />   
    //             </TouchableOpacity>
    //         </View>
    //     )
    // }

    menuBox_1(){
        return(
            <ScrollView>
                <Text style={styles.welcome}>{this.state.date}</Text>
                {this.checkInButton()} 
                <Text style={styles.welcome_1}> Check-In  :   {this.state.tanggal_in} - {this.state.checkin}</Text>
                <Text style={styles.welcome_1}> Check-Out  :  {this.state.tanggal_out} - {this.state.checkout}</Text>
                <Text style={styles.welcome_2}> Lembur  :  {this.state.lembur}</Text>
                <Text style={styles.welcome_1}> Cuti  :  {this.state.cuti}</Text>
                <View style = {{flexDirection : 'row', paddingRight : 10, paddingBottom : 10}}>
                    <Text style={styles.welcome_1}> Izin  :  {this.state.izin}</Text>
                    <TouchableOpacity style={GeneralStyle. izinButton}   onPress={this.gotoMenu.bind(this, 3)}>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="file-alt" size={28} color='#FFFFFF' />   
                    </TouchableOpacity>
                </View>
                
                {/* <Fab
                    // active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: 'red' }}
                    position="bottomRight"
                    onPress={this.goIzin()}
                    >
                    <VIcon type={VIcon.TYPE_FONTAWESOME} name="file" size={screen.height*0.05} color='#FFFFFF' />
                </Fab> */}
          </ScrollView>
        )
    }

menuBox_2(){
        return(
            <ScrollView>
                <Text style={styles.welcome}>{this.state.date}</Text>
                {this.checkOutButton()}
                {this.lemburButton()}
                <Text style={styles.welcome_1}> Check-In  :  {this.state.tanggal_in} - {this.state.checkin}</Text>
                <Text style={styles.welcome_1}> Check-Out  :  {this.state.tanggal_out} - {this.state.checkout}</Text>
                <Text style={styles.welcome_2}> Lembur  :  {this.state.lembur}</Text>
                <Text style={styles.welcome_1}> Cuti  :  {this.state.cuti}</Text>
                <View style = {{flexDirection : 'row', paddingRight : 10, paddingBottom : 10}}>
                    <Text style={styles.welcome_1}> Izin  :  {this.state.izin}</Text>
                    <TouchableOpacity style={GeneralStyle. izinButton}  onPress={this.gotoMenu.bind(this, 3)}>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="file-alt" size={28} color='#FFFFFF' />    
                    </TouchableOpacity>
                </View>
                {/* <Text style={styles.welcome_1}> Izin  :  {this.state.izin}</Text> */}
                {/* <Fab
                    // active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: 'red' }}
                    position="bottomRight"
                    // onPress={() => this.gotoIzin(this,1)}
                    >
                    <VIcon type={VIcon.TYPE_FONTAWESOME} name="file" size={screen.height*0.05} color='#FFFFFF' />
                </Fab> */}
          </ScrollView>
        )
    }

    menuBox_3(){
        return(
            <ScrollView>
                <Text style={styles.welcome}>{this.state.date}</Text>
                {this.lemburButton()}
                <Text style={styles.welcome_1}> Check-In  :   {this.state.tanggal_in} - {this.state.checkin}</Text>
                <Text style={styles.welcome_1}> Check-Out  :   {this.state.tanggal_out} - {this.state.checkout}</Text>
                <Text style={styles.welcome_2}> Lembur  :  {this.state.lembur}</Text>
                <Text style={styles.welcome_1}> Cuti  :  {this.state.cuti}</Text>
                <View style = {{flexDirection : 'row', paddingRight : 10}}>
                    <Text style={styles.welcome_1}> Izin  :  {this.state.izin}</Text>
                    <TouchableOpacity style={GeneralStyle. izinButton}>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="file-alt" size={28} color='#FFFFFF' />
                    </TouchableOpacity>
                </View>
                {/* <Text style={styles.welcome_1}> Izin  :  {this.state.izin}</Text>
                <Fab
                // active={this.state.active}
                direction="up"
                containerStyle={{ }}
                style={{ backgroundColor: 'red' }}
                position="bottomRight"
                // onPress={this.gotoIzin(this,1)}
                >
                    <VIcon type={VIcon.TYPE_FONTAWESOME} name="file" size={screen.height*0.05} color='#FFFFFF' />
                </Fab> */}
          </ScrollView>
        )
    }

    findLocation(){
        Geolocation.getCurrentPosition(
            (position) => {
                this.refs.loading1.close();
                console.log("STRINGIFY = "+JSON.stringify(position));
                console.log("latitude = "+position.coords.latitude);
                console.log("longitude = "+position.coords.longitude);
                if(position.mocked == true || JailMonkey.canMockLocation()){
                    //alert("FAKE GPS DETECTED");
                    const data = {
                        act: false,
                        message: 'Fake GPS Detected',
                        confirm: 'OK',
                        status: 0
                    }
                    openLightbox(data, 500, this.props.navigator);
                } else {
                    this.setLocation(position);
                    // // To check using package name (Android only):
                    // AppInstalledChecker
                    //     .isAppInstalledAndroid('fake gps pro') 
                    //     .then((isInstalled) => {
                    //         // isInstalled is true if the app is installed or false if not
                    //         console.log("check by package name = "+isInstalled);
                    //         if(isInstalled){
                    //             this.setState({
                    //                 error_msg: 'Fake GPS Detected'
                    //             });
                    //             this._toggleModal();
                    //         } else{
                    //             this.setLocation();
                    //         }
                    // });
                }
            },
            (error) => {
                this.refs.loading1.close();
                // See error code charts below.
                console.log(error.code, error.message);
                const data = {
                    act: false,
                    message: error.message,
                    confirm: 'OK',
                    status: -1
                }
                openLightbox(data, 500, this.props.navigator);
            },
            { enableHighAccuracy: true, timeout: 5000, maximumAge: 2000 }
        );
    }

    setLocation = (position) =>{
        this.setState(({mapRegion}) => ({mapRegion: {
        ...mapRegion,
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        }}));
        this.setState({
            cur_lat: position.coords.latitude,
            cur_long: position.coords.longitude,
        });
        this.onRegionChange(position.coords.latitude, position.coords.longitude),
        this.props.actions.setLocation(position.coords.latitude, position.coords.longitude);
    }

    currentLocation(){
        this.setState(({mapRegion}) => ({mapRegion: {
            ...mapRegion,
            latitude: this.state.cur_lat,
            longitude: this.state.cur_long,
        }}));
        this.refs.loading1.close();
    }

    gotoMenu(index){
        var scr, ttl;
        if(index == 1){
            scr = 'panelg.attendance.checkin';
            ttl = 'Check In'
            tp = null
            rb = [
                {
                    icon: iconsMapFa['check'],
                    buttonColor: 'white',
                    id: 'submit',
                }
            ]
        } else if(index == 2){
            scr = 'panelg.attendance.checkout';
            ttl = 'Check Out';
            tp = null
            rb = [
                {
                    icon: iconsMapFa['check'],
                    buttonColor: 'white',
                    id: 'submit',
                }
            ]
        } else if(index == 3){
            scr = 'panelg.plugin.TopTabsAttendance';
            ttl = 'Request';
            tp = topNotif
            rb = []
        } else if(index == 4){
            scr = 'panelg.attendance.cuti';
            ttl = 'Cuti';
            tp = null
        }
        iconsLoadedFa.then(() => {
            this.props.navigator.push({
                screen: scr,
                title: ttl,
                topTabs: tp,
                navigatorStyle: {
                    navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
                    navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
                    navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
                    navBarHidden: false, // make the nav bar hidden
                    navBarComponentAlignment: 'center',
                    navBarTitleTextCentered: true,
                }, 
                navigatorButtons: {
                    // rightButtons: [
                    //     {
                    //         icon: iconsMapFa['check'],
                    //         buttonColor: 'white',
                    //         id: 'submit',
                    //     }
                    // ],
                    rightButtons : rb,
                    leftButtons: [
                        {
                            icon: iconsMapFa['angle-left'],
                            text : 'Batal',
                            buttonColor: 'white',
                            id: 'back',
                        }
                    ]
                },
        //         passProps: {
        //   onUnmount: () => {
        //     this.removeItemImage();
        //   }
       
            });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
    }

    getDetailStatus(){
        var data = new FormData();
        data.append('keyapi', this.props.apiState.keyApiAbsen);
        data.append('dataapi', 10);
        data.append('nik', this.props.profileState.nik);
        var res_msg;
        axios.post(this.props.apiState.apiAbsen, data)
          .then(response => {
            // console.log("Detail Status= "+JSON.stringify(response.data));
            this.filterStatus(response.data)
          })
          .catch((error) => {
            //console.log(error);
            this.setState({ error, loading: false });
            console.log("================================================");
            console.log("Error gelondongan = "+error); //ini yang bener
            console.log("================================================");
            // this._toggleLoading();
          });
      }

      filterStatus = (data) => {
        return (
          array = data[1],
          console.log('array =' + JSON.stringify(array.absen_in)),
          this.setState ({
             status_in : array.status_in,
             status_out : array.status_out,
             checkin : array.absen_in,
             checkout : array.absen_out,
             tanggal_in : array.tanggal_in,
             tanggal_out : array.tanggal_out,
          })
        )
    }

    // async removeItemImage(){
    //     console.log('delete image uri');
    //     AsyncStorage.setItem('image_uri', '');
    // }

    mapContainer(){
        return(
             <View style={GeneralStyle.mapContainerAttendance}>
                <MapView
                    style={GeneralStyle.mapviewAttendance}
                    followUserLocation={true}
                    region={this.state.mapRegion}
                    showsMyLocationButton={true}
                    showsCompass={true}    
                >
                    <MapView.Marker
                        coordinate={{
                            latitude: (this.state.mapRegion.latitude) || -36.82339,
                            longitude: (this.state.mapRegion.longitude) || -73.03569,
                    }}>
                        <View>
                            <LottieView
                                onLayout={this.onLottieLoad}
                                ref={animation => {
                                    this.animation = animation;
                                }}
                                style={GeneralStyle.markerLottieAttendance}
                                source={anim}
                            />
                        </View>
                    </MapView.Marker>
                </MapView>
                <View style={GeneralStyle.mapCurrentLocationAttendanceContainer}>
                    <TouchableOpacity style={GeneralStyle.mapCurrentLocationAttendanceButton}
                    onPress={this.currentLocation.bind(this)}>
                        <VIcon type={VIcon.TYPE_MATERIALICONS} name="my-location" size={28} color='#4F8EF7'/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    checkInButton(){
        return(
            <View style={GeneralStyle.menuHomeContainerCheckBox}>
                <TouchableOpacity style={GeneralStyle.buttonHomeCheck}
                    onPress={this.gotoMenu.bind(this, 1)}>
                    <View style={GeneralStyle.buttonIconContainer} >
                        {/* <VIcon type={VIcon.TYPE_FONTAWESOME} name="arrow-circle-o-down" size={iconMenuSize} color='#FFFFFF'/> */}
                        <Text style={GeneralStyle.textMenuButtonHomeCheck}>Check-In</Text>
                        {/* <Text style={GeneralStyle.textSmallMenuButtonHome}>Tap untuk menandai anda sudah masuk</Text> */}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    checkOutButton(){
        return(
            <View style={GeneralStyle.menuHomeContainerCheckBox}>
                <TouchableOpacity style={GeneralStyle.buttonHomeCheck}
                    onPress={this.gotoMenu.bind(this, 2)}>
                    <View style={GeneralStyle.buttonIconContainer} >
                        {/* <VIcon type={VIcon.TYPE_FONTAWESOME} name="arrow-circle-o-down" size={iconMenuSize} color='#FFFFFF'/> */}
                        <Text style={GeneralStyle.textMenuButtonHomeCheck}>Check-Out</Text>
                        {/* <Text style={GeneralStyle.textSmallMenuButtonHome}>Tap untuk menandai anda ingin keluar</Text> */}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    lemburButton(){
        return(
            <View style={GeneralStyle.menuHomeContainerCheckBox}>
                <TouchableOpacity style={GeneralStyle.buttonHomeCheck}
                    onPress={this.gotoMenu.bind(this, 2)}>
                    <View style={GeneralStyle.buttonIconContainer} >
                        {/* <VIcon type={VIcon.TYPE_FONTAWESOME} name="arrow-circle-o-down" size={iconMenuSize} color='#FFFFFF'/> */}
                        <Text style={GeneralStyle.textMenuButtonHomeCheck}>Lembur</Text>
                        {/* <Text style={GeneralStyle.textSmallMenuButtonHome}>Tap untuk menandai anda ingin lembur</Text> */}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        // const _mapContainer = this.mapContainer();
        // console.log ("ini status =" + JSON.stringify(this.state.status_in) + '/' + JSON.stringify(this.state.status_out))
        const status = 0
        const shadowOpt = {
            width:screen.width,
            height:screen.height*0.13,
            color:"#000",
            border:3,
            opacity:0.2,
            x:0,
            y:-3,
            side: 'top'
        }
    return (
        <ImageBackground
            style={GeneralStyle.imageBackground}
        >
         <Loading ref='loading1' image = {require('../../image/loading.png')} />
            {/* {this.coba()} */}
            {this.mapContainer()}
            { this.state.status_in == 1 ?  this.menuBox_2() : this.state.status_out == 1 ? this.menuBox_3() :  this.menuBox_1() }
            {/* {status === 0 ? 
                this.menuBox_1()
            :
               status === 1 ? this.menuBox_2() : this.menuBox_3()
            } */}
        </ImageBackground>
    );
  }
};

const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textInput: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 40,
        fontSize: 25,
        paddingLeft: 20,
        paddingRight: 20
    },
    welcome: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 15,
        color: 'black', 
        fontWeight: 'bold',
        paddingTop: 10,
        paddingBottom : 10,
        paddingLeft : 10,   
        marginRight : 10,
        marginLeft : 10,     
        borderBottomWidth: 1,
        borderColor : 'grey',
    },
    welcome_1: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        fontSize: 15,
        color: 'black', 
        fontWeight: 'bold',
        paddingTop: 10,
        paddingBottom : 10,
        paddingLeft : 10,

    },
    welcome_2: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        fontSize: 15,
        paddingTop: 10,
        color: 'black', 
        fontWeight: 'bold',
        paddingBottom : 10,
        marginRight : 10,
        marginLeft : 10,
        borderBottomWidth: 1,
        borderColor : 'grey',
    },
});

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        locationState: state.location,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceMenuCheckIn);
import React, { Component } from 'react';
import {StyleSheet, View, Text, Dimensions, Button, TouchableOpacity, ScrollView, Image, TextInput,
  AsyncStorage, Animated, Easing, ImageBackground, Picker} from 'react-native';
import * as axios from 'axios';
import IconFA from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux';
import Modal from 'react-native-modal';
import { TextField } from 'react-native-material-textfield';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action/index';
import { Fumi } from 'react-native-textinput-effects';
import LottieView from 'lottie-react-native';
import {anim_gratika_red, background_image} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import VIcon from '../../../styles/VIcon';
import {openLightbox, openLightboxLoading} from '../HomeNav';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';

export class AttendanceIzin extends Component {
  constructor(props){
    super(props);
    this.state = {
        keterangan: '',
        jenis_izin: '0',
        error: '',
        place: '',
        respon_msg: '',
        lineWidth: 0,
        isModalVisible: false,
        // isModalLoading: true,       
    };
  }

  static navigatorStyle = {
    tabBarHidden: true,
    tabBarShowLabels: 'hidden',
  }

  onLottieLoad = () => {
    console.log('play lottie');
    this.animation.play();
  }

  // componentWillUnmount(){
  //   this.props.onUnmount();
  // }

  componentDidMount() {    
    this.getGeocode();
  }

  getGeocode() {
    axios.get(this.props.apiState.apiReverseLocation+ this.props.locationState.lat +','+ this.props.locationState.long +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
   .then(response => {
     console.log(response);
      this.setState({
        place: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
      });
      // this._toggleLoading();
    }).catch((error) => { // catch is called after then
      this.setState({ 
        error: error.message,
      });
      // this._toggleLoading();
    });
  }

  closeBack = () =>{
    const {navigator} = this.props;
    this.props.navigator.popToRoot({
        //screen: 'panelg.attendance.menu',
        animated: true, // does the pop have transition animation or does it happen immediately (optional)
        animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
    });
    this.props.navigator.handleDeepLink({
        link: 'tab3/in/any/format',
        payload: '' // (optional) Extra payload with deep link
    });
  }

  toggleModal(){
    this.setState({ isModalVisible: !this.state.isModalVisible });
    console.log("toggle modal");
  }

  _toggleLoading = () => {
    this.setState({ isModalLoading: !this.state.isModalLoading });
    console.log("ini toggle loading");
    //console.log("location state = "+JSON.stringify(this.props.locationState));
  }

  closeBack(){
    const {navigator} = this.props;
    this.props.navigator.popToRoot({
        //screen: 'panelg.attendance.menu',
        animated: true, // does the pop have transition animation or does it happen immediately (optional)
        animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
    });
    this.props.navigator.handleDeepLink({
        link: 'tab3/in/any/format',
        payload: '' // (optional) Extra payload with deep link
    });
  }

  splitPlace(alamat){
    let add = alamat;
    var result = add.split(",");
    return result[0];
    
  }

  goScreen() {
    console.log("ini masuk goScreen")
    const {navigator} = this.props;
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: 'panelg.notifscreenrequestizin',
        title: 'Leave Request Status',
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
        navigatorStyle: {
          navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
          navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
          navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
          navBarHidden: false, // make the nav bar hidden
          navBarComponentAlignment: 'center',
          navBarTitleTextCentered: true,
        },
        passProps: {
          onUnmount: () => {
            console.log('Notif Detail dismissed');
            this.makeRemoteRequestProgress();
            this.makeRemoteRequestHistory();
          }
        }
      });
    }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
    });
  }

  submitIzin(){
    //console.log("ini cuman contoh");
    // this._toggleLoading();
    console.log("jenis state = "+this.state.jenis_izin);
    if(this.state.jenis_izin == '0' || this.state.keterangan == ''){
      this.setState({
        respon_msg: 'Please Complete All The Field'
      });
      // this._toggleLoading();
      // this.toggleModal();
      const data = {
        act: false,
        message: 'Please Complete All The Field',
        confirm: 'OK',
        status: 0
      }
      openLightbox(data, 0, this.props.navigator);
    } else{
      var jenisIzin;
      if(this.state.jenis_izin == '1'){
        jenisIzin = 'i'
      } else if(this.state.jenis_izin == '2'){
        jenisIzin = 's'
      }
      var data = new FormData();
        data.append('keyapi', this.props.apiState.keyApiAbsen);
        data.append('dataapi', 6);
        data.append('caption', this.state.keterangan);
        data.append('nik', this.props.profileState.nik);
        data.append('jenis', jenisIzin);
      
      const config = {
        headers: { 'content-type': 'multipart/form-data' },
        timeout: 10000
      }

      axios.post(this.props.apiState.apiAbsen, data, config)
         .then(response => {
           console.log("response stringify "+JSON.stringify(response));
           this.setState({
             keterangan: '',
             jenis_izin: '0',
             respon_msg: response.data.info
            });
            // this._toggleLoading();
            // const data = {
            //   act: true,
            //   goAct: ()=>this.closeBack(),
            //   message: response.data.info,
            //   confirm: 'Done',
            //   status: 0
            // }
            // openLightbox(data, 500, this.props.navigator);
            this.toggleModal();
          })
         .catch((error) => {
           //console.log(error);
           console.log("================================================");
           console.log("Error gelondongan = "+error); //ini yang bener
           console.log("================================================");
           console.log("ERRORNYA _response = "+error.request._response);
           console.log("================================================");
           //this.setState({respon_msg: response.data.message});
            this.setState({
              respon_msg: error
            });
            // const data = {
            //   act: true,
            //   goAct: ()=>this.closeBack(),
            //   message: response.data.info,
            //   confirm: 'Done',
            //   status: 0
            // }
            // openLightbox(data, 500, this.props.navigator);
            // this._toggleLoading();
            this.toggleModal();
          });
    }
    //this._toggleLoading();
    //this.toggleModal();
  }

  render() {
    return(
      <ImageBackground
        source={background_image}
        style={GeneralStyle.imageBackground}
      >
      <ScrollView keyboardShouldPersistTaps="always" style={GeneralStyle.scrollContainerAttendance}>
        <Modal isVisible={this.state.isModalLoading}>
            <View style={GeneralStyle.modalLoading}>
                <LottieView
                    onLayout={this.onLottieLoad}
                    ref={animation => {
                        this.animation = animation;
                    }}
                    style={GeneralStyle.lottieLoading}
                    source={anim_gratika_red}
                />
            </View>
        </Modal>
        <Modal isVisible={this.state.isModalVisible}>
            <View style={GeneralStyle.modalParent}>
                <View style={GeneralStyle.modalContentAlert}>
                    <Text style={GeneralStyle.textModalContentAlert}>{this.state.respon_msg}</Text>
                    <TouchableOpacity style={GeneralStyle.modalButtonAlert} onPress={this.closeBack.bind(this)}>
                        <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        <View style = {{paddingHorizontal : 10}}>
          <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="map-pin" size={20} color='grey'/>
              <Text style = {{marginLeft : 10}}>Location</Text>
          </View>
            <View style={styles.AddressAreaContainer} >
              <TextInput
                style={styles.AddressArea}
                underlineColorAndroid="transparent"
                placeholderTextColor="grey"
                numberOfLines={10}
                multiline={true}
                value= {this.splitPlace(this.state.place)}
                // value={this.state.place}
              />
          </View>
          <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="male" size={20} color='grey'/>
              <Text style = {{marginLeft : 10}}>Keperluan</Text>
          </View>
          <View style={styles.textAreaContainer} >
            <Picker
              style={{ height: 40}}
              selectedValue={this.state.jenis_izin}
              onValueChange={(itemValue, itemIndex) => this.setState({ jenis_izin: itemValue })}>
              <Picker.Item label="-Select Subjact-" value="0" />
              <Picker.Item label="Izin" value="1" />
              <Picker.Item label="Sakit" value="2" />
            </Picker>        
          </View>
          <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="edit" size={20} color='grey'/>
              <Text style = {{marginLeft : 10}}>Penjelasan</Text>
          </View>
          <View style={styles.textAreaContainer} >
            <TextInput
                style={styles.textArea}
                underlineColorAndroid="transparent"
                placeholderTextColor="grey"
                numberOfLines={10}
                multiline={true}
                value={this.state.keterangan}
                onChangeText={(val) => this.setState({ keterangan: val })}
            />
          </View>
        </View>

        {/* <TextField 
            label='Location'
            value={this.state.place}
            multiline={true}
            editable={false}
            lineWidth={this.state.lineWidth}
        />
        <Picker
          selectedValue={this.state.jenis_izin}
          onValueChange={(itemValue, itemIndex) => this.setState({ jenis_izin: itemValue })}>
          <Picker.Item label="Jenis Keperluan" value="0" />
          <Picker.Item label="Izin" value="1" />
          <Picker.Item label="Sakit" value="2" />
        </Picker>
        <Fumi
            label={'Description'}
            iconClass={IconFA}
            iconName={'edit'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={this.state.keterangan}
            onChangeText={(val) => this.setState({ keterangan: val })}
        /> */}
        <View style={{paddingTop:20, justifyContent: 'center', alignItems: 'center',}}>
          <View style={GeneralStyle.buttonContainerAttendanceCheck}>
            <TouchableOpacity style={GeneralStyle.buttonSubmitCuti} onPress={this.submitIzin.bind(this)}>
              <Text style={GeneralStyle.textButtonSubmitAttendance}>Ajukan</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ paddingBottom : 20}}>
            <View style={GeneralStyle.buttonContainerDetail}>
                <TouchableOpacity style={GeneralStyle. izinButton} onPress={this.goScreen.bind(this)}>
                    <VIcon type={VIcon.TYPE_MATERIALICONS} name="restore" size={28} color='#FFFFFF' />   
                </TouchableOpacity>
            </View>
        </View>
      </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  AddressAreaContainer: {
    borderColor: '#FFFFFF',
    borderWidth: 1,
    paddingHorizontal : 10
  },
  textAreaContainer: {
    borderColor: 'black',
    borderWidth: 1,
    paddingHorizontal : 10
  },
  AddressArea: {
    height: 40,
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start"
  }
})

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        locationState: state.location,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceIzin);
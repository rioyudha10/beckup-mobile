import React, { Component } from 'react';
import {StyleSheet, View, Text, Dimensions, Button, TouchableOpacity, ScrollView, Image, TextInput,
  AsyncStorage, Animated, Easing, ImageBackground, Picker, Keyboard} from 'react-native';
import * as axios from 'axios';
import IconFA from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import { TextField } from 'react-native-material-textfield';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action/index';
import { Fumi } from 'react-native-textinput-effects';
import LottieView from 'lottie-react-native';
import {anim_gratika_red, background_image} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import {openLightbox, openLightboxLoading} from '../HomeNav';
import VIcon from '../../../styles/VIcon';

export class AttendanceCuti extends Component {
    constructor(props){
        super(props);
        this.state = {
            tanggal_awal: '',
            tanggal_akhir: '',
            alamat_cuti: '',
            keterangan: '',
            jenis_izin: '0',
            error: '',
            place: '',
            respon_msg: '',
            lineWidth: 0,
            isModalVisible: false,
            isDatePickerVisibleAwal: false,
            isDatePickerVisibleAkhir: false
        };
    }

    static navigatorStyle = {
        tabBarHidden: true,
        tabBarShowLabels: 'hidden',
    }

    onLottieLoad = () => {
        console.log('play lottie');
        this.animation.play();
    }

    // componentWillUnmount(){
    //     this.props.onUnmount();
    // }

    closeBack(){
        const {navigator} = this.props;
        this.props.navigator.popToRoot({
            //screen: 'panelg.attendance.menu',
            animated: true, // does the pop have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
        });
        this.props.navigator.handleDeepLink({
            link: 'tab3/in/any/format',
            payload: '' // (optional) Extra payload with deep link
        });
    }

    _showDatePickerAwal = () => {
        this.setState({ isDatePickerVisibleAwal: true })
        Keyboard.dismiss();
    };

    _showDatePickerAkhir = () => {
        this.setState({ isDatePickerVisibleAkhir: true })
        Keyboard.dismiss();
    };

    _hideDatePickerAwal = () => this.setState({ isDatePickerVisibleAwal: false });
    _hideDatePickerAkhir = () => this.setState({ isDatePickerVisibleAkhir: false });

    _handleDatePickedAwal = (date) => {
        this._hideDatePickerAwal();
        Moment.locale('en');
        this.setState({ tanggal_awal: Moment(date).format('DD/MM/YYYY') })
    };

    _handleDatePickedAkhir = (date) => {
        this._hideDatePickerAkhir();
        Moment.locale('en');
        this.setState({ tanggal_akhir: Moment(date).format('DD/MM/YYYY') })
    };

    closeBack(){
        const {navigator} = this.props;
        this.props.navigator.popToRoot({
            //screen: 'panelg.attendance.menu',
            animated: true, // does the pop have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
        });
        this.props.navigator.handleDeepLink({
            link: 'tab3/in/any/format',
            payload: '' // (optional) Extra payload with deep link
        });
      }

    submitCuti(){
        console.log("submit cuti");
        console.log("tanggal awal = "+this.state.tanggal_awal);
        console.log("tanggal akhir = "+this.state.tanggal_akhir);
        // openLightboxLoading(this.props.actions, this.props.navigator);
        if(this.state.tanggal_awal == '' || this.state.tanggal_akhir == '' || 
            this.state.keterangan == '' || this.state.alamat_cuti == ''){
            const data = {
                act: false,
                message: 'Please Complete All The Field',
                confirm: 'OK',
                status: 0
            }
            openLightbox(data, 0, this.props.navigator);
            // this.setState({
            //     respon_msg: 'Please Complete All The Field'
            // });
            // this.props.actions.setLoading(false);
            // this.toggleModal();
        } else{
            var data = new FormData();
            data.append('keyapi', this.props.apiState.keyApiAbsen);
            data.append('dataapi', 7);
            data.append('caption', this.state.keterangan);
            data.append('nik', this.props.profileState.nik);
            data.append('tanggal_awal', this.state.tanggal_awal);
            data.append('tanggal_akhir', this.state.tanggal_akhir);
            data.append('lokasi_cuti', this.state.alamat_cuti);
      
            const config = {
                headers: { 'content-type': 'multipart/form-data' },
                timeout: 10000
            }

      axios.post(this.props.apiState.apiAbsen, data, config)
         .then(response => {
           console.log("response stringify "+JSON.stringify(response));
           this.setState({
             tanggal_awal: '',
             tanggal_akhir: '',
             keterangan: '',
             alamat_cuti: '',
             respon_msg: response.data.info
            });
            const data = {
                act: true,
                goAct: ()=>this.closeBack(),
                message: response.data.info,
                confirm: 'Done',
                status: 0
            }
            openLightbox(data, 500, this.props.navigator);
          })
         .catch((error) => {
           //console.log(error);
           console.log("================================================");
           console.log("Error gelondongan = "+error); //ini yang bener
           console.log("================================================");
           console.log("ERRORNYA _response = "+error.request._response);
           console.log("================================================");
           //this.setState({respon_msg: response.data.message});
            this.setState({
              respon_msg: error
            });
            const data = {
                act: true,
                goAct: ()=>this.closeBack(),
                message: response.data.info,
                confirm: 'Done',
                status: 0
            }
            openLightbox(data, 500, this.props.navigator);
          });
        }
    }

    toggleModal(){
        this.setState({ isModalVisible: !this.state.isModalVisible });
        console.log("toggle modal");
    }

    goScreen() {
        console.log("ini masuk goScreen")
        const {navigator} = this.props;
        iconsLoadedFa.then(() => {
          navigator.push({
            screen: 'panelg.notifscreenrequestizin',
            title: 'Leave Request Status',
            navigatorButtons: {
              leftButtons: [
                {
                  icon: iconsMapFa['angle-left'],
                  buttonColor: 'white',
                  id: 'back',
                }
              ]
            },
            navigatorStyle: {
              navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
              navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
              navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
              navBarHidden: false, // make the nav bar hidden
              navBarComponentAlignment: 'center',
              navBarTitleTextCentered: true,
            },
            passProps: {
              onUnmount: () => {
                console.log('Notif Detail dismissed');
                this.makeRemoteRequestProgress();
                this.makeRemoteRequestHistory();
              }
            }
          });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
      }
    

    openPluginPlacePicker = () =>{
        const {navigator} = this.props;
        var scr, ttl;
        scr = 'panelg.plugin.placepicker';
        ttl = 'Alamat Cuti';
        iconsLoadedFa.then(() => {
            navigator.push({
                screen: scr,
                title: ttl,
                navigatorButtons: {
                    leftButtons: [
                        {
                            icon: iconsMapFa['angle-left'],
                            buttonColor: 'white',
                            id: 'back',
                        }
                    ]
                },
                passProps: {
          onUnmount: () => {
            console.log("plugin picker dismis");
          }
        }
            });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
    }

    render() {
        return(
            <ImageBackground
                source={background_image}
                style={GeneralStyle.imageBackground}
            >
            <ScrollView keyboardShouldPersistTaps="always" style={GeneralStyle.scrollContainerAttendance}>
                <Modal isVisible={this.state.isModalVisible}>
                    <View style={GeneralStyle.modalParent}>
                        <View style={GeneralStyle.modalContentAlert}>
                            <Text style={GeneralStyle.textModalContentAlert}>{this.state.respon_msg}</Text>
                            <TouchableOpacity style={GeneralStyle.modalButtonAlert} onPress={this.closeBack.bind(this)}>
                                <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <View style = {{paddingHorizontal : 10}}>
                    <View style = {{flexDirection : 'row', paddingTop : 10, marginBottom : 10}}>
                        <VIcon type={VIcon.TYPE_FONTAWESOME} name="calendar" size={20} color='grey'/>
                        <Text style = {{marginLeft : 10}}>Tanggal Mulai</Text>
                    </View>
                    <ScrollView keyboardShouldPersistTaps="always" style={{backgroundColor: 'white'}}>
                        <View style={styles.DateAreaContainer} >  
                            <TextInput
                                style={styles.DateArea}
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                numberOfLines={10}
                                multiline={true}
                                value={this.state.tanggal_awal}
                                onFocus={this._showDatePickerAwal}
                                onChangeText={(val) => this.setState({ tanggal_awal: val })}
                            />
                            <DateTimePicker
                                isVisible={this.state.isDatePickerVisibleAwal}
                                mode={'date'}
                                onConfirm={this._handleDatePickedAwal}
                                onCancel={this._hideDatePickerAwal}
                            />
                        </View>
                    </ScrollView>
                    <View style = {{flexDirection : 'row', paddingTop : 10, marginBottom : 10}}>
                        <VIcon type={VIcon.TYPE_FONTAWESOME} name="calendar" size={20} color='grey'/>
                        <Text style = {{marginLeft : 10}}>Tanggal Selesai</Text>
                    </View>
                    <ScrollView keyboardShouldPersistTaps="always" style={{backgroundColor: 'white'}}>
                        <View style={styles.DateAreaContainer} >
                            <TextInput
                                style={styles.DateArea}
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                numberOfLines={10}
                                multiline={true}
                                value={this.state.tanggal_akhir}
                                onFocus={this._showDatePickerAkhir}
                                onChangeText={(val) => this.setState({ tanggal_akhir: val })}
                            />
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isDatePickerVisibleAkhir}
                            mode={'date'}
                            onConfirm={this._handleDatePickedAkhir}
                            onCancel={this._hideDatePickerAkhir}
                        />
                    </ScrollView>
                    <View style = {{flexDirection : 'row', paddingTop : 10, paddingHorizontal : 10, marginBottom : 10}}>
                        <VIcon type={VIcon.TYPE_FONTAWESOME} name="male" size={20} color='grey'/>
                        <Text style = {{marginLeft : 10}}>Keperluan</Text>
                    </View>
                    <View style={styles.textAreaContainer} >
                        <TextInput
                            style={styles.textArea}
                            underlineColorAndroid="transparent"
                            placeholderTextColor="grey"
                            numberOfLines={10}
                            multiline={true}
                            value={this.state.keterangan}
                            onChangeText={(val) => this.setState({ keterangan: val })}
                        />
                    </View>
                    <View style = {{flexDirection : 'row', paddingTop : 10, paddingHorizontal : 10, marginBottom : 10}}>
                        <VIcon type={VIcon.TYPE_FONTAWESOME} name="map-pin" size={20} color='grey'/>
                        <Text style = {{marginLeft : 10}}>Alamat Ketika Cuti</Text>
                    </View>
                    <View style={styles.textAreaContainer} >
                        <TextInput
                            style={styles.textArea}
                            underlineColorAndroid="transparent"
                            placeholderTextColor="grey"
                            numberOfLines={10}
                            multiline={true}
                            value={this.state.alamat_cuti}
                            onChangeText={(val) => this.setState({ alamat_cuti: val })}
                        />
                    </View>
                </View>  
                <View style={{paddingTop:20, justifyContent: 'center', alignItems: 'center',}}>
                    <View style={GeneralStyle.buttonContainerAttendanceCheck}>
                        <TouchableOpacity style={GeneralStyle. buttonSubmitCuti} onPress={this.submitCuti.bind(this)}>
                            <Text style={GeneralStyle.textButtonSubmitAttendance}>Ajukan</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ paddingBottom : 20}}>
                    <View style={GeneralStyle.buttonContainerDetail}>
                        <TouchableOpacity style={GeneralStyle. izinButton}  onPress={this.goScreen.bind(this)}>
                            <VIcon type={VIcon.TYPE_MATERIALICONS} name="restore" size={28} color='#FFFFFF' />   
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    DateArea: {
        height: 40,
    },
    DateAreaContainer: {
        borderColor: 'black',
        borderWidth: 2,
    },
    textAreaContainer: {
        borderColor: 'black',
        borderWidth: 2,
        padding: 5
    },
    textArea: {
        height: 100,
        justifyContent: "flex-start"
    }
})

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        locationState: state.location,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceCuti);
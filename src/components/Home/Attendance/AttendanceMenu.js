import React, { Component } from 'react';
import { AppRegistry, View, StatusBar, Button, Text, StyleSheet, 
    Dimensions, AsyncStorage, TouchableOpacity, Animated, Easing, ImageBackground } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import MapView from 'react-native-maps';
var screen = Dimensions.get('window');
import Geolocation from 'react-native-geolocation-service';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import {BoxShadow} from 'react-native-shadow';
import LottieView from 'lottie-react-native';
import JailMonkey from 'jail-monkey';
import DeviceInfo from 'react-native-device-info';
import { AppInstalledChecker, CheckPackageInstallation } from 'react-native-check-app-install';
import anim from '../../../images/bouncy_mapmaker.json';
import animCurrentLocation from '../../../images/location.json';
import {anim_gratika_red, background_image, iconMenuSize} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import {openLightbox, openLightboxLoading} from '../HomeNav';
import VIcon from '../../../styles/VIcon';

export class AttendanceMenu extends Component {    
    constructor(props){
        super(props);
        this.state = {
            mapRegion: {
                latitude: 0.0,
                longitude: 0.0,
                latitudeDelta: 0.0018,
                longitudeDelta: 0.0018,
              },
            cur_lat: null,
            cur_long: null,
            lastLat: null,
            lastLong: null,
            error: '',
            error_msg: '',
            region_change: false,
            count_region_change: 0,
        };
    }

    onLottieLoad = () => {
        console.log('play lottie');
        this.animation.play();
    }

    onRegionChange(lastLat, lastLong) {
        this.setState({
          lastLat: lastLat || this.state.lastLat,
          lastLong: lastLong || this.state.lastLong
        });
      }

    static navigatorStyle = {
        tabBarHidden: true
    }

    async storeLocation(_latitude, _longitude) {
        try {
            await AsyncStorage.setItem('latitude', ""+_latitude);
            await AsyncStorage.setItem('longitude', ""+_longitude);
        } catch (error) {
            console.log("something went wrong");
            this.setState({ error: error });
            console.log("error " + error);
        }
    }

    componentDidMount() {
        openLightboxLoading(this.props.actions, this.props.navigator);
        this.findLocation();
    }

    findLocation(){
        Geolocation.getCurrentPosition(
            (position) => {
                this.props.actions.setLoading(false);
                console.log("STRINGIFY = "+JSON.stringify(position));
                console.log("latitude = "+position.coords.latitude);
                console.log("longitude = "+position.coords.longitude);
                if(position.mocked == true || JailMonkey.canMockLocation()){
                    //alert("FAKE GPS DETECTED");
                    const data = {
                        act: false,
                        message: 'Fake GPS Detected',
                        confirm: 'OK',
                        status: 0
                    }
                    openLightbox(data, 500, this.props.navigator);
                } else {
                    this.setLocation(position);
                    // // To check using package name (Android only):
                    // AppInstalledChecker
                    //     .isAppInstalledAndroid('fake gps pro') 
                    //     .then((isInstalled) => {
                    //         // isInstalled is true if the app is installed or false if not
                    //         console.log("check by package name = "+isInstalled);
                    //         if(isInstalled){
                    //             this.setState({
                    //                 error_msg: 'Fake GPS Detected'
                    //             });
                    //             this._toggleModal();
                    //         } else{
                    //             this.setLocation();
                    //         }
                    // });
                }
            },
            (error) => {
                this.props.actions.setLoading(false);
                // See error code charts below.
                console.log(error.code, error.message);
                const data = {
                    act: false,
                    message: error.message,
                    confirm: 'OK',
                    status: -1
                }
                openLightbox(data, 500, this.props.navigator);
            },
            { enableHighAccuracy: true, timeout: 5000, maximumAge: 2000 }
        );
    }

    setLocation = (position) =>{
        this.setState(({mapRegion}) => ({mapRegion: {
        ...mapRegion,
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        }}));
        this.setState({
            cur_lat: position.coords.latitude,
            cur_long: position.coords.longitude,
        });
        this.onRegionChange(position.coords.latitude, position.coords.longitude),
        this.props.actions.setLocation(position.coords.latitude, position.coords.longitude);
    }

    currentLocation(){
        this.setState(({mapRegion}) => ({mapRegion: {
            ...mapRegion,
            latitude: this.state.cur_lat,
            longitude: this.state.cur_long,
        }}));
        this.props.actions.setLocationRegion(false);
    }

    gotoMenu(index){
        var scr, ttl;
        if(index == 1){
            scr = 'panelg.attendance.checkin';
        } else if(index == 2){
            scr = 'panelg.attendance.checkout';
            ttl = 'Check Out';
        } else if(index == 3){
            scr = 'panelg.attendance.izin';
            ttl = 'Izin';
        } else if(index == 4){
            scr = 'panelg.attendance.cuti';
            ttl = 'Cuti';
        }
        iconsLoadedFa.then(() => {
            this.props.navigator.push({
                screen: scr,
                title: ttl,
                navigatorStyle: {
                    navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
                    navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
                    navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
                    navBarHidden: false, // make the nav bar hidden
                    navBarComponentAlignment: 'center',
                }, 
                navigatorButtons: {
                    leftButtons: [
                        {
                            icon: iconsMapFa['angle-left'],
                            buttonColor: 'white',
                            id: 'back',
                        }
                    ]
                },
                passProps: {
          onUnmount: () => {
            this.removeItemImage();
          }
        }
            });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
    }

    async removeItemImage(){
        console.log('delete image uri');
        AsyncStorage.setItem('image_uri', '');
    }

    mapContainer(){
        return(
            <View style={GeneralStyle.mapContainerAttendance}>
                <MapView
                    style={GeneralStyle.mapviewAttendance}
                    followUserLocation={true}
                    region={this.state.mapRegion}
                    showsMyLocationButton={true}
                    showsCompass={true}    
                >
                    <MapView.Marker
                        coordinate={{
                            latitude: (this.state.mapRegion.latitude) || -36.82339,
                            longitude: (this.state.mapRegion.longitude) || -73.03569,
                    }}>
                        <View>
                            <LottieView
                                onLayout={this.onLottieLoad}
                                ref={animation => {
                                    this.animation = animation;
                                }}
                                style={GeneralStyle.markerLottieAttendance}
                                source={anim}
                            />
                        </View>
                    </MapView.Marker>
                </MapView>
                <View style={GeneralStyle.mapCurrentLocationAttendanceContainer}>
                    <TouchableOpacity style={GeneralStyle.mapCurrentLocationAttendanceButton}
                    onPress={this.currentLocation.bind(this)}>
                        <VIcon type={VIcon.TYPE_MATERIALICONS} name="my-location" size={28} color='#4F8EF7'/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    menuButtonContainer(){
        return(
            <View style={GeneralStyle.menuContainerAttendance}>
                <View style={GeneralStyle.menuHomeContainer}>
                    <TouchableOpacity style={GeneralStyle.buttonHome}
                        onPress={this.gotoMenu.bind(this, 1)}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="arrow-circle-o-down" size={iconMenuSize} color='#FFFFFF'/>
                            <Text style={GeneralStyle.textMenuButtonHome}>Check In</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={GeneralStyle.separatorButtonHomeSpace}/>
                    <TouchableOpacity style={GeneralStyle.buttonHome}
                        onPress={this.gotoMenu.bind(this, 2)}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="arrow-circle-o-up" size={iconMenuSize} color='#FFFFFF'/>
                            <Text style={GeneralStyle.textMenuButtonHome}>Check Out</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={GeneralStyle.menuHomeContainer}>
                    <TouchableOpacity style={GeneralStyle.buttonHome}
                        onPress={this.gotoMenu.bind(this, 3)}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_MATERIALCOMMUNITYICONS} name="note-text" size={iconMenuSize} color='#FFFFFF'/>
                            <Text style={GeneralStyle.textMenuButtonHome}>Izin</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={GeneralStyle.separatorButtonHomeSpace}/>
                    <TouchableOpacity style={GeneralStyle.buttonHome}
                        onPress={this.gotoMenu.bind(this, 4)}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="calendar-times-o" size={iconMenuSize} color='#FFFFFF'/>
                            <Text style={GeneralStyle.textMenuButtonHome}>Cuti</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

  render() {
      const _mapContainer = this.mapContainer();
      const _menuButtonContainer = this.menuButtonContainer();
      const shadowOpt = {
                            width:screen.width,
                            height:screen.height*0.13,
                            color:"#000",
                            border:3,
                            opacity:0.2,
                            x:0,
                            y:-3,
                            side: 'top'
                        }
    return (
        <ImageBackground
            source={background_image}
            style={GeneralStyle.imageBackground}
        >
        <View style={GeneralStyle.containerParentAttendance}>

        <StatusBar barStyle='light-content' />
        {this.mapContainer()}
        <BoxShadow setting={shadowOpt}>
                {this.menuButtonContainer()}
        </BoxShadow>
      </View>
      </ImageBackground>
    );
  }
};

const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        locationState: state.location,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceMenu);
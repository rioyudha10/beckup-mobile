import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, TouchableOpacity, ScrollView,
Image, Animated, ActivityIndicator, FlatList, ListView, 
Picker, Keyboard, KeyboardAvoidingView} from 'react-native';
import { Container, Header, Button, Icon, Fab, ListItem, List,
  Thumbnail, Body, Right, Item, Form, Input} from 'native-base';
import ElevatedView from 'react-native-elevated-view';
import axios from 'axios';
import Moment from 'moment';
import {styleConst} from '../../../styles/styleconst';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { connect } from 'react-redux';
import * as appActions from '../../../redux/action';
import VIcon from '../../../styles/VIcon';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import LottieView from 'lottie-react-native';
var screen = Dimensions.get('window');
import Loading from 'react-native-whc-loading';
const img_size = screen.height*0.15;


export class AddLogbook extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        idLogbook: null,
        idPegawai: null,
        idproject: null,
        allProject: [],
        datepickerTarget: false,
        datepickerStart: false,
        datepickerComplete: false,
        timepickerTarget: false,
        timepickerStart: false,
        timepickerCompelete: false,
        dateStart: null,
        dateTarget: null,
        dateComplete: null,
        timeTarget: null,
        timeStart: null,
        timeComplete: null,
        inputIssue: null,
        inputActivity: null,
        inputProgres: null,
        inputNote: null,
        editable: true,
      }
      this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    static navigatorStyle = {
      tabBarHidden: true,
      navBarBackgroundColor: 'red',
      navBarButtonColor: '#FFFFFF',
      navBarTitleTextCentered: true,
    }

    onNavigatorEvent = (event) => {
      console.log(JSON.stringify(event));
      if (event.type == 'NavBarButtonPress') {
        if (event.id == 'backPress' || event.id == 'back') {
          console.log("go back");
          this.goBack();
        }
        if(event.id == 'submit'){
            this.addLogbookAPI();
        }
      }
    }

    goBack = () =>{
        this.props.getViewAllLogbookAPI();
        this.props.navigator.pop({
            animated: true,
            animationType: 'fade'
        })
    }
    
    componentDidMount(){
        console.log('props data = '+JSON.stringify(this.props.act));
        if(this.props.detailLogbook != 0){
            let ds = this.props.detailLogbook.date_start.split(" ");
            let dt = this.props.detailLogbook.date_target.split(" ");
            let dc = null;
            if(this.props.detailLogbook.id_progress == 1){
                this.setState({editable: true});
            } else{
                dc = this.props.detailLogbook.actual_corp_date.split(" ");
                this.setState({
                    editable: false,
                    dateComplete: dc[0],
                    timeComplete: dc[1],
                });
            }
            this.setState({
                idLogbook: this.props.detailLogbook.id_logbook,
                idproject: this.props.detailLogbook.id_project,
                idPegawai: this.props.detailLogbook.id_pegawai,
                inputIssue: this.props.detailLogbook.issue,
                dateStart: ds[0],
                timeStart: ds[1],
                dateTarget: dt[0],
                timeTarget: dt[1],
                inputActivity: this.props.detailLogbook.activity,
                inputProgres: this.props.detailLogbook.id_progress,
                inputNote: this.props.detailLogbook.note
            });
        }
        this.getProjectAPI();
    }

    componentWillUnmount(){
        this.props.getViewAllLogbookAPI();
    }

    addLogbookAPI = () =>{
        console.log('add logbook');
        console.log('selected proyek = '+this.state.idproject);
        console.log('date target = '+this.state.dateTarget);
        console.log('time target = '+this.state.timeTarget);
        console.log('activity = '+this.state.inputActivity);
        console.log('date start = '+this.state.dateStart);
        console.log('time start = '+this.state.timeStart);
        console.log('date complete = '+this.state.dateComplete);
        console.log('time complete = '+this.state.timeComplete);
        console.log('progress = '+this.state.inputProgres);
        this.refs.loading1.show();
        if(this.state.idproject == null || this.state.idproject == 0 || 
            this.state.dateTarget == null || this.state.inputActivity == null ||
            this.state.dateStart == null || this.state.inputProgres == null){
            const data = {
                act: false,
                message: 'Please Complete All Fields',
                confirm: 'OK'
            }
            this.openLightbox(data, 500);
            this.refs.loading1.close();
        } else{
            let d = new Date();
            var data = new FormData();
            if(this.props.act != 'add'){
                data.append('id_logbook', this.state.idLogbook);
            }
            data.append('id_project', this.state.idproject);
            data.append('issue', this.state.inputIssue);
            data.append('date_target', this.state.dateTarget+' '+this.state.timeTarget);
            data.append('activity', this.state.inputActivity);
            data.append('date_start', this.state.dateStart+' '+this.state.timeStart);
            data.append('actual_corp_date', this.state.dateComplete+' '+this.state.timeComplete);
            data.append('id_progress', this.state.inputProgres);
            data.append('id_pegawai', this.props.profileState.iduser);
            data.append('note', this.state.inputNote);
            data.append('date_input', Moment(d).format('YYYY-MM-DD'));

            const config = {
                headers: {Authorization: this.props.apiState.keyApiLogbook, 
                    'content-type': 'multipart/form-data' },
                timeout: 10000
            }
            let pathUrl = null;
            console.log ( "ini props act = " + this.props.act)
            if(this.props.act == 'add'){
                pathUrl = 'logbook_insert.php';
            } else{
                pathUrl = 'logbook_update.php';
            }
            axios.post(this.props.apiState.apiLogbook+pathUrl, data, config)
            .then(response => {
                this.props.actions.setLoading(false);
                console.log("response stringify "+JSON.stringify(response));
                const data = {
                    act: true,
                    message: response.data.message,
                    confirm: 'OK',
                    goAct: ()=>this.goBack(),
                }
                this.openLightbox(data, 500);
                this.refs.loading1.close();
            })
            .catch((error) => {
                this.props.actions.setLoading(false);
                console.log('error = '+JSON.stringify(error));
                const data = {
                    act: true,
                    goAct: ()=>this.goBack(),
                    message: error.message,
                    confirm: 'OK',
                    status: -1
                }
                this.openLightbox(data, 500);
                this.refs.loading1.close();
            });
        }
    }

    getProjectAPI = () =>{
        axios({
            method: 'get',
            headers: {Authorization: this.props.apiState.keyApiLogbook},
            url: this.props.apiState.apiLogbook+'project_viewall.php',
            timeout: 10000
        }).then((response) =>{
            console.log('response api project = '+JSON.stringify(response));
            this.props.actions.setLoading(false);
            if(response.data.re == 0){
                const data = {
                    act: false,
                    message: response.data.message,
                    confirm: 'OK'
                }
                this.openLightbox(data, 500);
            } else{
                this.setState({allProject: response.data.data});
            }
        }).catch((err) =>{
            this.props.actions.setLoading(false);
            const data = {
                act: false,
                message: err.message,
                confirm: 'OK'
            }
            this.openLightbox(data, 500);
        });
    }

    toggleDatePickerTarget = () =>{
        Keyboard.dismiss();
        this.setState({ datepickerTarget: !this.state.datepickerTarget });
    }

    toggleDatePickerStart = () =>{
        Keyboard.dismiss();
        this.setState({ datepickerStart: !this.state.datepickerStart });
    }

    toggleDatePickerComplete = () =>{
        Keyboard.dismiss();
        this.setState({ datepickerComplete: !this.state.datepickerComplete });
    }

    toggleTimePickerTarget = () =>{
        Keyboard.dismiss();
        this.setState({ timepickerTarget: !this.state.timepickerTarget });
    }

    toggleTimePickerStart = () =>{
        Keyboard.dismiss();
        this.setState({ timepickerStart: !this.state.timepickerStart });
    }

    toggleTimePickerComplete = () =>{
        Keyboard.dismiss();
        this.setState({ timepickerCompelete: !this.state.timepickerCompelete });
    }

    handleDatePickedTarget = (date) =>{
        Moment.locale('en');
        this.toggleDatePickerTarget();
        this.setState({ dateTarget: Moment(date).format('YYYY-MM-DD') })
    }

    handleDatePickedStart = (date) =>{
        Moment.locale('en');
        this.toggleDatePickerStart();
        this.setState({ dateStart: Moment(date).format('YYYY-MM-DD') })
    }

    handleDatePickedComplete = (date) =>{
        Moment.locale('en');
        this.toggleDatePickerComplete();
        this.setState({ dateComplete: Moment(date).format('YYYY-MM-DD') })
    }

    handleTimePickedTarget = (time) =>{
        Moment.locale('en');
        this.toggleTimePickerTarget();
        this.setState({ timeTarget: Moment(time).format('HH:mm:ss') })
    }

    handleTimePickedStart = (time) =>{
        Moment.locale('en');
        this.toggleTimePickerStart();
        this.setState({ timeStart: Moment(time).format('HH:mm:ss') })
    }

    handleTimePickedComplete = (time) =>{
        Moment.locale('en');
        this.toggleTimePickerComplete();
        this.setState({ timeComplete: Moment(time).format('HH:mm:ss') })
    }

    openLightbox = (data, timeout) => {
        setTimeout(()=>{
          this.props.navigator.showLightBox({
            screen: 'panelg.modal.alert',
            style: {
                backgroundBlur: 'dark',
                backgroundColor: '#44444480',
                tapBackgroundToDismiss: false
            },
            passProps: {
                data: () => (data),                
            }
          })
        },timeout);
    }

    openLightboxLoading = () => {
        this.props.actions.setLoading(true);
        this.props.navigator.showLightBox({
            screen: 'panelg.modal.loadingGratika',
            style: {
                backgroundBlur: 'dark',
                backgroundColor: '#44444480',
                tapBackgroundToDismiss: false
            },
        })
    }

    renderTitle = (title) =>{
        return(
            <Text style={{marginLeft: 5, marginTop: 5, fontWeight: 'bold'}}>{title}</Text>
        )
    }

    render() {      
      return (
        <View style={{flex: 1, paddingTop: 20, backgroundColor : '#FFFFFF'}}>
            <Loading ref='loading1' image = {require('../../image/loading.png')}/>
            <ScrollView style={{flex: 1}} keyboardShouldPersistTaps='always'>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="briefcase" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Picker
                            style={{height : screen.height*0.055, width : screen.width * 0.9, backgroundColor : '#FFFFFF'}}
                            selectedValue={this.state.idproject}
                            onValueChange={(itemValue, itemIndex) => this.setState({ idproject: itemValue })}>
                            <Picker.Item label="Pilih Proyek" value="0" />
                            {this.state.allProject.map((res, i) => {return <Picker.Item value={res.id_project} label={"" + res.nama_project} key={res.id_project}/> })}                        
                        </Picker>
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="exclamation-circle" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            value={this.state.inputIssue}
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Issue'}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_issue)=> this.inputIssue = _issue}
                            returnKeyType="next"
                            onSubmitEditing={() => this.inputDateTarget._root.focus()}
                            onChangeText={(val) => this.setState({inputIssue: val})}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="calendar-plus" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Date Target'}
                            value={this.state.dateTarget}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_dateTarget)=> this.inputDateTarget = _dateTarget}
                            returnKeyType="next"
                            onFocus={this.toggleDatePickerTarget}
                            onSubmitEditing={() => this.inputTimeTarget._root.focus()}
                        />
                        <DateTimePicker
                            isVisible={this.state.datepickerTarget}
                            mode={'date'}
                            onConfirm={this.handleDatePickedTarget}
                            onCancel={this.toggleDatePickerTarget}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="clock" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Time Target'}
                            value={this.state.timeTarget}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_timeTarget)=> this.inputTimeTarget = _timeTarget}
                            returnKeyType="next"
                            onFocus={this.toggleTimePickerTarget}
                            onSubmitEditing={() => this.inputActivity._root.focus()}
                        />
                        <DateTimePicker
                            isVisible={this.state.timepickerTarget}
                            mode={'time'}
                            onConfirm={this.handleTimePickedTarget}
                            onCancel={this.toggleTimePickerTarget}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="question-circle" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            value={this.state.inputActivity}
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Activity'}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_act)=> this.inputActivity = _act}
                            returnKeyType="next"
                            onSubmitEditing={() => this.inputDateStart._root.focus()}
                            onChangeText={(val) => this.setState({inputActivity: val})}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME} name="calendar" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Date Start'}
                            value={this.state.dateStart}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_dateStart)=> this.inputDateStart = _dateStart}
                            onFocus={this.toggleDatePickerStart}
                            returnKeyType="next"
                        />
                        <DateTimePicker
                            isVisible={this.state.datepickerStart}
                            mode={'date'}
                            onConfirm={this.handleDatePickedStart}
                            onCancel={this.toggleDatePickerStart}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="clock" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Time Start'}
                            value={this.state.timeStart}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_timeStart)=> this.inputTimeStart = _timeStart}
                            onFocus={this.toggleTimePickerStart}
                            returnKeyType="next"
                        />
                        <DateTimePicker
                            isVisible={this.state.timepickerStart}
                            mode={'time'}
                            onConfirm={this.handleTimePickedStart}
                            onCancel={this.toggleTimePickerStart}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="calendar-check" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Actual Complete Date'}
                            value={this.state.dateComplete}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_dateComplete)=> this.inputDateComplete = _dateComplete}
                            onFocus={this.toggleDatePickerComplete}
                            returnKeyType="next"
                        />
                        <DateTimePicker
                            isVisible={this.state.datepickerComplete}
                            mode={'date'}
                            onConfirm={this.handleDatePickedComplete}
                            onCancel={this.toggleDatePickerComplete}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="clock" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            editable={this.state.editable}
                            style = {styles.input}                            
                            placeholder={'Input Actual Complete Time'}
                            value={this.state.timeComplete}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_timeComplete)=> this.inputTimeComplete = _timeComplete}
                            onFocus={this.toggleTimePickerComplete}
                            returnKeyType="next"
                        />
                        <DateTimePicker
                            isVisible={this.state.timepickerCompelete}
                            mode={'time'}
                            onConfirm={this.handleTimePickedComplete}
                            onCancel={this.toggleTimePickerComplete}
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="file" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Input 
                            value={this.state.inputNote}
                            editable={this.state.editable}
                            style = {styles.input}
                            placeholder={'Input Note'}
                            placeholderTextColor={styleConst.colorTextPlaceholder}
                            ref={(_note)=> this.inputNote = _note}
                            returnKeyType="next"
                            onChangeText={(val) => this.setState({inputNote: val})}                    
                        />
                    </View>
                </View>
                <View style = {styles.contentWrapper}>
                    <View style = { styles.iconWrapper }>
                        <VIcon type={VIcon.TYPE_FONTAWESOME5} name="tasks" size={20} color='grey'/>
                    </View>
                    <View style = { styles.inputWrapper }>
                        <Picker
                            enabled={this.state.editable}
                            style={{height : screen.height*0.055, width : screen.width * 0.9, backgroundColor : '#FFFFFF'}}
                            selectedValue={this.state.inputProgres}
                            onValueChange={(itemValue, itemIndex) => this.setState({ inputProgres: itemValue })}>
                            <Picker.Item label="Pilih Progress" value="0" />
                            <Picker.Item label="On Progress" value="1" />
                            <Picker.Item label="Done " value="2" />
                        </Picker>
                    </View>
                </View>
                {/* <View style={styles.contentWrapper}>                    
                    {this.renderTitle('Proyek')}
                    <Picker
                        enabled={this.state.editable}
                        selectedValue={this.state.idproject}
                        onValueChange={(itemValue, itemIndex) => this.setState({ idproject: itemValue })}>
                        <Picker.Item label="Pilih Proyek" value="0" />
                        {this.state.allProject.map((res, i) => {return <Picker.Item value={res.id_project} label={"" + res.nama_project} key={res.id_project}/> })}                        
                    </Picker>
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Issue')}
                    <Input 
                        value={this.state.inputIssue}
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Issue'}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_issue)=> this.inputIssue = _issue}
                        returnKeyType="next"
                        onSubmitEditing={() => this.inputDateTarget._root.focus()}
                        onChangeText={(val) => this.setState({inputIssue: val})}
                    />
                </View> */}
                {/* <View style={styles.contentWrapper}>
                    {this.renderTitle('Date Target')}
                    <Input 
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Date Target'}
                        value={this.state.dateTarget}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_dateTarget)=> this.inputDateTarget = _dateTarget}
                        returnKeyType="next"
                        onFocus={this.toggleDatePickerTarget}
                        onSubmitEditing={() => this.inputTimeTarget._root.focus()}
                    />
                    <DateTimePicker
                        isVisible={this.state.datepickerTarget}
                        mode={'date'}
                        onConfirm={this.handleDatePickedTarget}
                        onCancel={this.toggleDatePickerTarget}
                    />
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Time Target')}
                    <Input 
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Time Target'}
                        value={this.state.timeTarget}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_timeTarget)=> this.inputTimeTarget = _timeTarget}
                        returnKeyType="next"
                        onFocus={this.toggleTimePickerTarget}
                        onSubmitEditing={() => this.inputActivity._root.focus()}
                    />
                    <DateTimePicker
                        isVisible={this.state.timepickerTarget}
                        mode={'time'}
                        onConfirm={this.handleTimePickedTarget}
                        onCancel={this.toggleTimePickerTarget}
                    />
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Activity')}
                    <Input 
                        value={this.state.inputActivity}
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Activity'}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_act)=> this.inputActivity = _act}
                        returnKeyType="next"
                        onSubmitEditing={() => this.inputDateStart._root.focus()}
                        onChangeText={(val) => this.setState({inputActivity: val})}
                    />
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Date Start')}
                    <Input 
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Date Start'}
                        value={this.state.dateStart}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_dateStart)=> this.inputDateStart = _dateStart}
                        onFocus={this.toggleDatePickerStart}
                        returnKeyType="next"
                    />
                    <DateTimePicker
                        isVisible={this.state.datepickerStart}
                        mode={'date'}
                        onConfirm={this.handleDatePickedStart}
                        onCancel={this.toggleDatePickerStart}
                    />
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Time Start')}
                    <Input 
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Time Start'}
                        value={this.state.timeStart}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_timeStart)=> this.inputTimeStart = _timeStart}
                        onFocus={this.toggleTimePickerStart}
                        returnKeyType="next"
                    />
                    <DateTimePicker
                        isVisible={this.state.timepickerStart}
                        mode={'time'}
                        onConfirm={this.handleTimePickedStart}
                        onCancel={this.toggleTimePickerStart}
                    />
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Actual Complete Date')}
                    <Input 
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Actual Complete Date'}
                        value={this.state.dateComplete}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_dateComplete)=> this.inputDateComplete = _dateComplete}
                        onFocus={this.toggleDatePickerComplete}
                        returnKeyType="next"
                    />
                    <DateTimePicker
                        isVisible={this.state.datepickerComplete}
                        mode={'date'}
                        onConfirm={this.handleDatePickedComplete}
                        onCancel={this.toggleDatePickerComplete}
                    />
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Actual Complete Time')}
                    <Input 
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Actual Complete Time'}
                        value={this.state.timeComplete}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_timeComplete)=> this.inputTimeComplete = _timeComplete}
                        onFocus={this.toggleTimePickerComplete}
                        returnKeyType="next"
                    />
                    <DateTimePicker
                        isVisible={this.state.timepickerCompelete}
                        mode={'time'}
                        onConfirm={this.handleTimePickedComplete}
                        onCancel={this.toggleTimePickerComplete}
                    />
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Progress')}
                    <Picker
                        enabled={this.state.editable}
                        selectedValue={this.state.inputProgres}
                        onValueChange={(itemValue, itemIndex) => this.setState({ inputProgres: itemValue })}>
                        <Picker.Item label="Pilih Progress" value="0" />
                        <Picker.Item label="On Progress" value="1" />
                        <Picker.Item label="Done " value="2" />
                    </Picker>
                </View>
                <View style={styles.contentWrapper}>
                    {this.renderTitle('Note')}
                    <Input 
                        value={this.state.inputNote}
                        editable={this.state.editable}
                        style={{marginLeft: 10, fontFamily: styleConst.fontPrimaryRegular}}
                        placeholder={'Input Note'}
                        placeholderTextColor={styleConst.colorTextPlaceholder}
                        ref={(_note)=> this.inputNote = _note}
                        returnKeyType="next"
                        onChangeText={(val) => this.setState({inputNote: val})}                    
                    />
                </View>                 */}
            </ScrollView>
        </View>
      );
    }
}

const styles = StyleSheet.create({
    contentWrapper: {
        width: screen.width*0.95, 
        height: screen.height*0.06, 
        alignSelf: 'center', 
        marginBottom: 10,
        flexDirection : 'row'
    },
    iconWrapper : {
        height : screen.height*0.06,
        width : screen.height*0.06,
        alignItems : 'center',
        justifyContent : 'center'

    },
    inputWrapper : {
        height : screen.height*0.06,
        width : screen.width*0.7,
        borderColor: 'black',
        borderBottomWidth: 1,
    },
    input : {
        height: screen.height*0.55, 
        width : screen.width * 0.9, 
        backgroundColor : '#FFFFFF', 
        fontFamily: styleConst.fontPrimaryRegular, 
        color : '#000000',
    }
  });

function mapStateToProps(state, ownProps) {
  return {
    apiState: state.api,
    profileState: state.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddLogbook);
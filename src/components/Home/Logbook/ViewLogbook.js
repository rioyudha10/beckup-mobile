import React, { Component } from 'react';
import { Platform, StyleSheet, View, TouchableOpacity, Image, Dimensions, 
  AsyncStorage, ActivityIndicator, PermissionsAndroid, ScrollView, FlatList, ListView } from 'react-native';
import { Row, Container, Content, Grid, Text, Button, Form, Item, 
  Icon, Input, Right, List, ListItem, Body, Fab, Separator } from "native-base";
import ElevatedView from 'react-native-elevated-view';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import VIcon from '../../../styles/VIcon';
import axios from 'axios';
import Moment from 'moment';
import _ from 'lodash';
import { connect } from 'react-redux';
import * as appActions from '../../../redux/action';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import Loading from 'react-native-whc-loading';

var screen = Dimensions.get('window');
const ds = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 });
const contains = ({ projectName }, query) => {
  if (projectName.includes(query)) {
    return true;
  }
  return false;
};
type Props = {};
export class MainViewLogbook extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      dataAllProject: [],
      dataLogbookOri: [],
      dataLogbook: [],
    }
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  static navigatorStyle = {
    tabBarHidden: true,
    navBarBackgroundColor: '#579ED2',
    navBarButtonColor: '#FFFFFF',
    topBarElevationShadowEnabled: false
  }

  onNavigatorEvent = (event) => {
    // console.log(JSON.stringify(event));
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'backPress' || event.id == 'back') {
        console.log("go back");
        this.props.navigator.pop({
          animated: true,
          animationType: 'fade'
        })
      }
    }
  }

  componentDidMount(){
    this.refs.loading1.show();    
    this.getProjectNameAPI();
  }

  getViewAllLogbookAPI = () =>{
    console.log('getViewAllLogbookAPI = '+this.props.apiState.apiLogbook+'logbook_viewall.php');
    axios({
      method: 'get',
      headers: {Authorization: this.props.apiState.keyApiLogbook},
      url: this.props.apiState.apiLogbook+'logbook_viewall.php',
      timeout: 10000
    }).then((response) =>{
      console.log('response api logbook = '+JSON.stringify(response));
      this.refs.loading1.close();
      if(response.data.re == 0){
        const data = {
          act: false,
          message: response.data.message,
          confirm: 'OK'
        }
        this.openLightbox(data, 500);
      } else{
        console.log('response = '+JSON.stringify(response));
        let d = new Date();
        let dow = Moment(d).day();
        console.log('date of week = '+ dow);
        let startDateWeek = Moment(d, 'YYYY-MM-DD').add(-dow, 'days').format('YYYY-MM-DD');
        console.log('start date week = '+startDateWeek);
        console.log('start date week moment = '+Moment(startDateWeek).format('YYYY-MM-DD'));
        let dataResponse = response.data.data.filter(a => a.id_pegawai == this.props.profileState.iduser);
        dataResponse = dataResponse.filter(b => b.id_progress == 1 || b.date_start >= Moment(startDateWeek).format('YYYY-MM-DD'));        
        dataResponse.map((val)=>{
          let projName = this.state.dataAllProject.filter(a => a.id_project == val.id_project);
          console.log('projName = '+projName[0].nama_project);
          val.haz = 'test';
          val.projectName = projName[0].nama_project;
        });
        this.setState({dataLogbook: dataResponse, dataLogbookOri: dataResponse});
        // dataResponse.map((val)=>{
        //   val.project_name = this.getProjectNameAPI(val.id_project)
        // });
        // this.setState({dataLogbook: dataResponse});
        console.log ("dataResponse = " + JSON.stringify(dataResponse))
      }
    }).catch((err) =>{
      this.refs.loading1.close();      
      const data = {
        act: false,
        message: err.message,
        confirm: 'OK'
      }
      this.openLightbox(data, 500);
    });
  }

  getProjectNameAPI = () =>{
    let projectName;
    axios({
      method: 'get',
      headers: {Authorization: this.props.apiState.keyApiLogbook},
      url: this.props.apiState.apiLogbook+'project_viewall.php',
      timeout: 10000
    }).then((response) =>{
        console.log('response api project = '+JSON.stringify(response));
        if(response.data.re == 0){
          this.refs.loading1.close();
          projectName = 'Error'
            const data = {
                act: false,
                message: response.data.message,
                confirm: 'OK'
            }
            this.openLightbox(data, 500);
        } else{
          this.setState({dataAllProject: response.data.data});
          this.getViewAllLogbookAPI();    
        }
    }).catch((err) =>{
      this.refs.loading1.close();
        const data = {
            act: false,
            message: err.message,
            confirm: 'OK'
        }
        this.openLightbox(data, 500);
    });
  }

  addLogbook = (act, isprogress, detail) =>{
    const detailLogbook = detail;
      iconsLoadedFa.then(() => {
        let rb = [];
        if(act == 'add' || isprogress == 1){
          rb = [
            {
              icon: iconsMapFa['check'],
              buttonColor: 'white',
              id: 'submit',
            }
          ];
        } else{
          rb = [];
        }
        this.props.navigator.push({
            screen: 'panelg.logbook.add',
            title: 'Detail Logbook',
            navigatorButtons: {
                leftButtons: [
                    {
                        icon: iconsMapFa['angle-left'],
                        buttonColor: 'white',
                        id: 'back',
                    }
                ],
                rightButtons: rb
            },
            passProps: {
              getViewAllLogbookAPI: () => {
                this.getViewAllLogbookAPI();
              },
              detailLogbook: detailLogbook,
              act: act
            }
        });
      }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
      });
  }

  openLightboxFilterDate = () =>{
    this.props.navigator.showLightBox({
      screen: 'panelg.modal.filterDate',
      style: {
          backgroundBlur: 'dark',
          backgroundColor: '#44444480',
          tapBackgroundToDismiss: true
      },
      passProps: {
        setFilter: (dateStart, dateEnd) => this.setFilter(dateStart, dateEnd)
      }
    })
  }

  openLightbox = (data, timeout) => {
    setTimeout(()=>{
      this.props.navigator.showLightBox({
        screen: 'panelg.modal.alert',
        style: {
            backgroundBlur: 'dark',
            backgroundColor: '#44444480',
            tapBackgroundToDismiss: false
        },
        passProps: {
            data: () => (data),                
        }
      })
    },timeout);
  }

  openLightboxLoading = () => {
    this.refs.loading1.show();
    this.props.navigator.showLightBox({
        screen: 'panelg.modal.loadingGratika',
        style: {
            backgroundBlur: 'dark',
            backgroundColor: '#44444480',
            tapBackgroundToDismiss: false
        },
    })
  }

  setFilter = (dateStart, dateEnd) =>{
    const data = this.state.dataLogbookOri.filter(a => a.atch_date >= dateStart && a.atch_date <= dateEnd);
    this.setState({dataHistory: data});
  }

  onSearch = (val) =>{
    const data = _.filter(this.state.dataLogbookOri, projectName =>{
      return contains(projectName, val);
    });
    this.setState({dataLogbook: data});
  }

  autoCloseSwipe(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
  }

  openDetailLogbook = (data, secId, rowId, rowMap) =>{
    this.autoCloseSwipe(secId, rowId, rowMap);
    this.addLogbook('view',data.id_progress, data);
  }

  openLightboxFinalizeOrder = (data, secId, rowId, rowMap) =>{
    console.log('data item = '+JSON.stringify(data));
    this.autoCloseSwipe(secId, rowId, rowMap);
  }

  finalizeOrderAPI = (dataItem, secId, rowId, rowMap, finalizePayload) =>{
    console.log('finalize order API = '+JSON.stringify(dataItem));
  }

  _renderRow = (item, secId, rowId, rowMap) =>{
    console.log ( "_rederRow item= " + JSON.stringify(item));
    console.log ( "_rederRow secId= " + secId );
    console.log ( "_rederRow rowId= " + rowId );
    console.log ( "_rederRow rowMap= " + rowMap );
    return(
      // <Separator bordered>
      //       <Text>MIDFIELD</Text>
      // </Separator>
      <ListItem onPress={() => this.openDetailLogbook(item, secId, rowId, rowMap)} >
        <Body>
          <Text>{item.projectName}</Text>
          <Text note numberOfLines={1}>{item.issue}</Text>
        </Body>
        <Right>
          <VIcon type={item.id_progress == '1' ? VIcon.TYPE_EVILICONS : VIcon.TYPE_MATERIALCOMMUNITYICONS} name={item.id_progress == '1' ? 'spinner' : 'check-circle-outline'} size={screen.width*0.07} color={item.id_progress == '1' ? 'gray' : 'green'} />          
        </Right>
      </ListItem>
    )
  }

  render() {
    const datasource = ds.cloneWithRows(this.state.dataLogbook);
    console.log ( " data array : " + JSON.stringify ( this.state.dataLogbook) )
    return (
      <View style={{flex: 1}}>
        <Loading ref='loading1' image = {require('../../image/loading.png')}/>
        <View style={{backgroundColor: 'red', flexDirection: 'row', padding: 10, height: screen.height*0.1, justifyContent: 'center', alignItems: 'center', flex: 0.1}}>
          {/* <TouchableOpacity onPress={()=>this.openLightboxFilterDate()}>
            <VIcon type={VIcon.TYPE_FEATHER} name='sliders' size={screen.height*0.04} color='#FFFFFF'/>
          </TouchableOpacity> */}
          <Item style={{backgroundColor: 'white', borderRadius: 5, height: screen.height*0.05, flex: 1}}>
            <Input 
            //   style={{fontFamily: styleConst.fontPrimaryRegular}}
              placeholder='Search'
              onChangeText={(val) => this.onSearch(val)}
            />
            <Icon name="ios-search" />
          </Item>
        </View>
        <ScrollView style={{flex: 0.9}}>
          <List
            dataSource={datasource}
            dataArray={this.state.dataLogbook}
            renderRow={this._renderRow.bind(this)}
            renderRightHiddenRow={(data, secId, rowId, rowMap) =>
              <Button 
                full 
                onPress={() => this.openDetailLogbook(data, secId, rowId, rowMap)} 
                style={{backgroundColor: '#0072bc', justifyContent: 'center', alignItems: 'center'}}>
                <VIcon type={VIcon.TYPE_FONTAWESOME5} name="clock" size={screen.width*0.1} color='#FFFFFF' />
              </Button>
            }
            renderLeftHiddenRow={(data, secId, rowId, rowMap) =>
              <Button 
                full 
                onPress={() => this.openLightboxFinalizeOrder(data, secId, rowId, rowMap)} 
                style={{backgroundColor: '#FF0000', justifyContent: 'center', alignItems: 'center'}}>
                <VIcon type={VIcon.TYPE_FONTAWESOME5} name="trash-alt" size={screen.width*0.1} color='#FFFFFF' />
              </Button>
            }
            leftOpenValue={screen.width*0.25}
            rightOpenValue={-screen.width*0.25}
            closeOnRowBeginSwipe={false}
          /> 
        </ScrollView>

        <Fab
            // active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#FF0000' }}
            position="bottomRight"
            onPress={() => this.addLogbook('add','1','0')}
            >
            <VIcon type={VIcon.TYPE_IONICONS} name="ios-add-circle-outline" size={screen.height*0.05} color='#FFFFFF' />
        </Fab>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    rootState: state.root,
    apiState: state.api,
    profileState: state.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainViewLogbook);
import { iconsMapFa, iconsLoadedFa } from '../../config/app-icons-fa';
const topNotif = null
topNotif = [{
    screenId: 'panelg.notifscreenprogress',
    title: 'Attendence',
}, {
    screenId: 'panelg.notifscreenhistory',
    title: 'Simpok',
}];

export function gotoMenu(index, nav){
    var scr, ttl, top1;
    if(index == 1){
        scr = 'panelg.plugin.placepicker';
        ttl = 'Simpok';
    } else if(index == 2){
        scr = 'panelg.attendance.menucheckin';
        ttl = 'Attendance';
    } else if(index == 3){
        scr = 'panelg.logbook.menu';
        ttl = 'Logbook'
    } else if(index == 4){
        scr = 'example.test';
        ttl = 'Test';
    }else if(index == 5){
        scr = 'panelg.attendance.menu';
        ttl = 'Check In';
    }
    iconsLoadedFa.then(() => {
        nav.push({
            screen: scr,
            title: ttl,
            topTabs: top1,
            navigatorButtons: {
                leftButtons: [
                    {
                        icon: iconsMapFa['angle-left'],
                        buttonColor: 'white',
                        id: 'back',
                    }
                ]
            },
            navigatorStyle: {
                // navBarTextFontSize: 16,
                navBarTextColor: '#FFFFFF',
                navBarBackgroundColor: 'red',
                // navBarTextFontFamily: styleConst.fontFamily,
                navBarTitleTextCentered: true,
                navBarButtonColor: '#FFFFFF'
            },
        });
    }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
    });
};

export function openLightbox(data, timeout, nav){
    setTimeout(()=>{
      nav.showLightBox({
        screen: 'panelg.modal.alert',
        style: {
            backgroundBlur: 'dark',
            backgroundColor: '#44444480',
            tapBackgroundToDismiss: false
        },
        passProps: {
            data: () => (data),                
        }
      })
    },timeout);
};

export function openLightboxLoading(act, nav){
    act.setLoading(true);
    nav.showLightBox({
        screen: 'panelg.modal.loadingGratika',
        style: {
            backgroundBlur: 'dark',
            backgroundColor: '#44444480',
            tapBackgroundToDismiss: false
        },
    })
};
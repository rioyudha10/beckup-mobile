import React, { Component } from 'react'
import { View, Text, StyleeSheet } from 'react-native'
import NotificationPopup from 'react-native-push-notification-popup';

export default class Massage extends Component {

    componentDidMount() {
        this.popup.show({
          onPress: function() {console.log('Pressed')},
          appTitle: 'Some App',
          timeText: 'Now',
          title: 'Hello World',
          body: 'This is a sample message.\nTesting emoji 😀',
        });
      }

    render() {
        return (
          <View>
            <NotificationPopup ref={ref => this.popup = ref} />
          </View>
        );
      }
}
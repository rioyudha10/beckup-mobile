import React, { Component } from 'react';
import { AppRegistry, View, StatusBar, Button, Text, StyleSheet, AsyncStorage, Platform,
    TouchableOpacity, Dimensions, Image, ImageBackground, SafeAreaView, ScrollView, Picker, Switch,
    Animated, Easing } from 'react-native';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import { debounce } from 'lodash';
import { iconsMapFa, iconsLoadedFa } from '../../config/app-icons-fa';
//import DeviceInfo from 'react-native-device-info';
import axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../redux/action';
import Swiper from 'react-native-swiper';
import {anim_gratika_red, background_image, iconMenuSize} from '../../styles/assets';
import GeneralStyle from '../../styles/GeneralStyle';
import VIcon from '../../styles/VIcon';
import {gotoMenu, openLightbox, openLightboxLoading} from './HomeNav';
import * as Progress from 'react-native-progress';
import Pie from 'react-native-pie'
import Loading from 'react-native-whc-loading';
import NotificationPopup from 'react-native-push-notification-popup';


// var PushNotification = require('react-native-push-notification');

var screen = Dimensions.get('window');
const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;
//const iconMenuSize = screen.height*0.04;

let topNotif = null;
topNotif = [{
    screenId: 'panelg.notifDetailAttandance',
    title: 'Attendence',
}, {
    screenId: 'panelg.notifDetailSimpok',
    title: 'Simpok',
}];

let drawerModal = null;
drawerModal = {
    left: {
        screen: 'drawer.SideDrawer'
    }
}

export class HomeMenu extends Component {
    constructor(props){
        super(props);
        this.state = {
            nik: undefined,
            password: undefined,
            mock: undefined,
            dataSwiper: [{},{},{},{},{},{}],
            swiperIllustration: [{},{},{},{},{},{}],
            swiperTitle: [{},{},{},{},{},{}],
            swiperTitleText: undefined,
            car_all: 0,
            car_avail: 0,
            car_used: 0,
            car_pie: 0,
            isModalVisible: false,
            isModalLoading: false,
            isPop: 0,
            text : '',
            seconds : 5,
            attendance : [],
            switchValue: false,
            status_in : '',
            status_out : ''
          };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    toggleSwitch = value => {
        //onValueChange of the switch this function will be called
        this.setState({ switchValue: value });
        //state changes according to switch
        //which will result in re-render the text
      }

    onSelect({ item, index }) {
        switch (item.type) {
          case 'close':
            this.forceClose();
            break;
          default:
            const random = Math.floor(Math.random() * 1000 + 1);
            const title = item.type + ' #' + random;
            this.dropdown.alertWithType(item.type, title, item.message);
        }
      }
      forceClose() {
        this.dropdown.close();
      }
      onClose(data) {
        console.log(data);
      }
      onCancel(data) {
        console.log(data);
      }

    
    
    onNavigatorEvent = (event) => {
        // handle a deep link
        if (event.type === 'NavBarButtonPress') {
            if(event.id === 'profile'){
                this.props.navigator.showModal({
                    screen: 'panelg.profilemenuscreen',
                    title : "Profile",
                    navigatorStyle: {
                        navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
                        navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
                        navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
                        navBarHidden: false, // make the nav bar hidden
                        navBarComponentAlignment: 'center',
                        navBarTitleTextCentered: true,
                    },
                    animationType: 'fade',
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: iconsMapFa['angle-left'],
                                buttonColor: 'white',
                                id: 'back',
                            }
                        ]
                    },
                })
            }
            else if(event.id === 'notification'){
                this.props.navigator.showModal({
                    screen: 'panelg.plugin.TopTabsNotification',
                    title : "Notification",
                    topTabs: topNotif,
                    navigatorStyle: {
                        navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
                        navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
                        navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
                        navBarHidden: false, // make the nav bar hidden
                        navBarComponentAlignment: 'center',
                        navBarTitleTextCentered: true,
                    },
                    orientation: 'portrait',
                    animationType : 'slide',
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: iconsMapFa['angle-left'],
                                buttonColor: 'white',
                                id: 'back',
                            }
                        ],
                    },
                })
            }
            else if(event.id === 'settings'){
                this.props.navigator.toggleDrawer({
                    side: 'left',
                    animationType : 'slide',     
                });
            }
        }
    }

    // switchTab  = () => {
    //     this.props.navigator.switchToTab({
    //         tabIndex: 2 // (optional) if missing, this screen's tab will become selected
    //     });
    // }
    

    // async componentDidMount(){
    //     this.getDataSwiperAPI()
    // }

    // getMessageNotif = () => {
    //     PushNotification.configure({

    // // (optional) Called when Token is generated (iOS and Android)
    //         onRegister: function(token) {
    //             console.log( 'TOKEN:', token );
    //         },

    //         // (required) Called when a remote or local notification is opened or received
    //         onNotification: function(notification) {
    //             console.log( 'NOTIFICATION:', notification );

    //             // process the notification

    //             // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
    //             notification.finish(PushNotificationIOS.FetchResult.NoData);
    //         },

    //         // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
    //         senderID: "YOUR GCM (OR FCM) SENDER ID",

    //         // IOS ONLY (optional): default: all - Permissions to register.
    //         permissions: {
    //             alert: true,
    //             badge: true,
    //             sound: true
    //         },

    //         // Should the initial notification be popped automatically
    //         // default: true
    //         popInitialNotification: true,

    //         /**
    //          * (optional) default: true
    //          * - Specified if permissions (ios) and token (android and ios) will requested or not,
    //          * - if not, you must call PushNotificationsHandler.requestPermissions() later
    //          */
    //         requestPermissions: true,
    //     });
    // }

    getDetailAttendance () {
        this.refs.loading1.show();
        var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        axios.get ( this.props.apiState.apiSimpok + data )
        .then ( response  => {
            //   this.filterDetailProgress(response.data.info)
            // console.log ("getDetailProgress in Home =" + JSON.stringify(response.data.info))
            this.filterArrayAttendance(response.data.info)
            this.refs.loading1.close();
        }).catch((error) => {
            this.setState({ error, loading: false });
            console.log("Error gelondongan = "+error); //ini yang bener
            this.refs.loading1.close();
        });
    }

    getDetailSimpok () {
        this.refs.loading1.show();
        var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        axios.get ( this.props.apiState.apiSimpok + data )
        .then ( response  => {
            this.filterArraySimpok(response.data.info)
        }).catch((error) => {
            this.setState({ error, loading: false });
            console.log("Error gelondongan = "+error); //ini yang bener
        });
    }

    filterArrayAttendance = (data) => {
        let temp = [];
        data.map(ele=>{
            if (ele.type === "ATTENDANCE-OUT" || ele.type === "ATTENDANCE-IN" || ele.type === "ATTENDANCE-CUTI" || ele.type === "ATTENDANCE-IZIN"){            
                return(
                    temp.push(ele)
                )
            }
            return temp;
        })
        let df = temp[temp.length-1]
        // console.log(" ini item =" + df.type)
        this.popupAttendance.show({
            onPress: function() {console.log('Pressed')},
            appTitle: 'Notification Attendance',
            timeText: df.waktu,
            title: df.type,
            body: 'This is a sample message from ' + df.dari,
        });
    }

    filterArraySimpok = (data) => {
        let temp = [];
        data.map(ele=>{
            if (ele.type === "GSIMPOK-P2K"){            
                return(
                    temp.push(ele)
                )
            }
            return temp;
        })
        let df = temp[temp.length-1]
        // console.log(" ini item =" + df.type)
        this.popupSimpok.show({
            onPress: function() {console.log('Pressed')},
            appTitle: 'Notification G-Simpok',
            timeText: df.waktu,
            title: df.type,
            body: 'This is a sample message from ' + df.dari,
        });
        // return(
        //     df.map( item => {
        //         console,log(" ini item =" + JSON.stringify(item))
        //     })
        // )
    }



    getDataSwiperAPI = () =>{
        // console.log("nik nya = "+this.props.profileState.nik);
        axios.get(this.props.apiState.apiPortal)
            .then((res) => {
                this.refs.loading1.close()
                this.setState(() => ({ dataSwiper: res.data }))
                console.log("dataSwiper = "+JSON.stringify(this.state.dataSwiper));
                var dataSwiperIllustration = this.state.dataSwiper.info.map(function(item){
                    return{
                        illustration: item.illustration
                    }
                });
                var dataSwiperTitle = this.state.dataSwiper.info.map(function(item){
                    return{
                        title: item.title
                    }
                });
                this.setState({
                    swiperIllustration: dataSwiperIllustration,
                    swiperTitle: dataSwiperTitle
                });
                // console.log("datastateswiper index 0 ="+this.state.swiperIllustration[0].illustration);
            }).catch((error) => {
                this.props.actions.setLoading(false);
                console.log("error portal = "+error);
            });
    }

    async componentDidMount(){
        this.getCountDriver(0);
        this.getCountDriver(1);
        this.getCountDriver(2);
        this.getDataSwiperAPI()
        this.checkMenuText();
        this. getDetailAttendance();
        this.getDetailSimpok()
        this.getDetailStatus()
        // this.popup.show({
        //     onPress: function() {console.log('Pressed')},
        //     appTitle: 'Some App',
        //     timeText: 'Now',
        //     title: 'Hello World',
        //     body: 'This is a sample message.\nTesting emoji 😀',
        // });
        
    }

    gotoMenu = (index) =>{
        gotoMenu(index, this.props.navigator);
    }

    getDetailStatus(){
        var data = new FormData();
        data.append('keyapi', this.props.apiState.keyApiAbsen);
        data.append('dataapi', 10);
        data.append('nik', this.props.profileState.nik);
        var res_msg;
        axios.post(this.props.apiState.apiAbsen, data)
          .then(response => {
            // console.log("Detail Status= "+JSON.stringify(response.data));
            this.filterStatus(response.data)
          })
          .catch((error) => {
            //console.log(error);
            this.setState({ error, loading: false });
            console.log("================================================");
            console.log("Error gelondongan = "+error); //ini yang bener
            console.log("================================================");
            // this._toggleLoading();
          });
      }

      filterStatus = (data) => {
        return (
          array = data[1],
          this.setState ({
             status_in : array.status_in,
             status_out : array.status_out
          })
        )
    }

    getCountDriver(param){
        /*
        0 = all car
        1 = available car
        2 = used car
        */
        //this._toggleLoadi[ng();
        var data = '11004/'+'0'+'/'+'0'+'/'+param+'/'+this.props.apiState.keyApiSimpok;
        // console.log("getdriver = "+this.props.apiState.apiSimpok + data);
        axios.get(this.props.apiState.apiSimpok + data)
        .then(response => {
          //this._toggleLoading();
        //   console.log("DETAIL getCountDriver = "+JSON.stringify(response.data));
          let countCar = Object.keys(response.data.info).length;
          if(param == 0){
            this.setState({car_all: countCar});
          }
          if(param == 1){
            this.setState({car_avail: countCar});
          } else if(param == 2){
            this.setState({car_used: countCar});
            var carPie = this.state.car_used/this.state.car_all;
            // console.log("var carpie = "+carPie);
            this.setState({car_pie: carPie});
            // console.log("car all = "+this.state.car_all);
            // console.log("car avail = "+this.state.car_avail);
            // console.log("car used = "+this.state.car_used);
            // console.log("carpie state = "+this.state.car_pie);
          }
        }).catch((error) => {
          //this._toggleLoading();
          this.setState({ respon_msg: error, isPop: 1 });
          console.log("Error getCountDriver = "+error); //ini yang bener
          //this.toggleModal();
        });
    }

    checkMenuText(){
        const status = 0
        if (status === 0){
            this.setState({
                text : 'CHECKED-IN'
            })
        }else if(status === 1 || status === 2){
            this.setState({
                text : 'CHEKED-OUT'
            })
        }
    }

    menuChartPie () {
        return (
            <View style = {{marginTop: 40, justifyContent: 'center', alignItems: 'center'}}>
                <View>
                    <Pie
                        radius={100}
                        innerRadius={70}
                        series={[this.state.car_avail*10]}
                        colors={['#f00']}
                        backgroundColor='#ddd' 
                    />
                    <View style={styles.gauge}>
                        <Text style={styles.gaugeTextReady}> {this.state.car_avail} Ready </Text>
                        <Text style={styles.gaugeTextBusy}> {this.state.car_used} Busy </Text>
                    </View>
                </View>
            </View>
        )
    }

    menuChartDashboard(){
        return(
            <View style={{
                marginTop: 40,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                
                <Progress.Circle
                    style={{
                        margin: 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    color={'black'}
                    thickness={8}
                    size={150}
                    showsText={true}
                    textStyle={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        fontSize: 15
                    }}
                    formatText={()=>{return `${this.state.car_avail}`+" Ready"+"\n"+`${this.state.car_used}`+" Bussy"}}
                    progress={this.state.car_pie}
                    indeterminate={false}
                />
            </View>
        );
    }

    renderMenu = () =>{
        const _menuChartDashboard = this.menuChartDashboard();
        const _textCheck = this.checkMenuText 
        const _menuChartPie = this.menuChartPie()
        return(
            <View>
                <View>
                    <NotificationPopup ref={ref => this.popupAttendance = ref} />
                    <NotificationPopup ref={ref => this.popupSimpok = ref} />
                </View>
                <View style={GeneralStyle.menuHomeContainer}>
                    <TouchableOpacity style={GeneralStyle.buttonHome}
                        onPress={debounce(this.gotoMenu.bind(this, 1), 1000, {
                            leading: true,
                            trailing: false
                        })}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="car" size={iconMenuSize} color='red'/>
                            <Text style={GeneralStyle.textMenuButtonHome}>G-Simpok</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={GeneralStyle.separatorButtonHomeSpace}/>
                    <TouchableOpacity style={GeneralStyle.buttonHome}
                        onPress={debounce(this.gotoMenu.bind(this, 2), 1000, {
                            leading: true,
                            trailing: false
                        })}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="clock-o" size={iconMenuSize} color='red'/>
                            <Text style={GeneralStyle.textMenuButtonHome}>Attendance</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={GeneralStyle.separatorButtonHomeSpace}/>
                    <TouchableOpacity style={GeneralStyle.buttonHome} onPress={()=>this.gotoMenu(3)}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="book" size={iconMenuSize} color='red'/>
                            <Text style={GeneralStyle.textMenuButtonHome}>Log Book</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={GeneralStyle.separatorButtonHomeSpace}/>
                    <TouchableOpacity style={GeneralStyle.buttonHome} onPress={()=>this.gotoMenu(4)}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <VIcon type={VIcon.TYPE_FONTAWESOME} name="comments" size={iconMenuSize} color='grey'/>                            
                            <Text style={GeneralStyle.textMenuButtonHome}>HelpDesk</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                { _menuChartPie }
                {this.state.status_in == 1 ? (
                    <View style={GeneralStyle.buttonHome} >
                        <Text style={GeneralStyle.textMenuButtonHomeCheck}>CHECK OUT</Text>
                    </View>
                ) : this.state.status_out == 1 ? (
                    <View style={GeneralStyle.buttonHome} >
                        <Text style={GeneralStyle.textMenuButtonHomeCheck}>LEMBUR</Text>
                    </View>
                ) : this.state.status_in == 0 ? (
                    <View style={GeneralStyle.buttonHome} >
                        <Text style={GeneralStyle.textMenuButtonHomeCheck}>CHECK IN</Text>
                    </View>
                ): (
                    <View style={GeneralStyle.buttonHome} >
                        <Text style={GeneralStyle.textMenuButtonHomeCheck}>CHECK IN</Text>
                    </View>
                )}
            </View>
        );
    }

    openLightboxLoading = () => {
        openLightboxLoading(this.props.actions, this.props.navigator);
    }

  render() {
    //   console.log ("api =" + JSON.stringify(this.props.apiState.apiAbsen))
    
    
    return (
        <ImageBackground
            source={background_image}
            style={GeneralStyle.imageBackground}
        >
         <Loading ref='loading1' image = {require('../image/loading.png')}/>
        <ScrollView>
            {this.renderMenu()}
        </ScrollView>
      </ImageBackground>
    );
  }
};

const styles = StyleSheet.create({
    gauge: {
        position: 'absolute',
        width: 200,
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
    },
    gaugeTextReady: {
        backgroundColor: 'transparent',
        color: '#f00',
        fontSize: 18,
    },
    gaugeTextBusy: {
        backgroundColor: 'transparent',
        color: '#ddd',
        fontSize: 18,
    },
})

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        notifState: state.notification,
        spinkitState: state.spinkit,
        locationState: state.location,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeMenu);
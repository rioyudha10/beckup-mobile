import React, { Component } from 'react';
import { AppRegistry, View, StatusBar, Button, Text, TouchableOpacity, ImageBackground } from 'react-native';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { Navigation } from 'react-native-navigation';
import { debounce } from 'lodash';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import {anim_gratika_red, background_image, iconMenuSize} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import LottieView from 'lottie-react-native';
import axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import Modal from 'react-native-modal';
import PieChart from 'react-native-pie-chart';
import * as Progress from 'react-native-progress';
import DeviceInfo from 'react-native-device-info';

export class SimpokMenu extends Component {
    constructor(props){
        super(props);
        this.state = {
            car_all: 0,
            car_avail: 0,
            car_used: 0,
            car_pie: 0,
            isModalVisible: false,
            isModalLoading: false,
            isPop: 0,
          };
    }

    getCountDriver(param){
        /*
        0 = all car
        1 = available car
        2 = used car
        */
        //this._toggleLoading();
        var data = '11004/'+'0'+'/'+'0'+'/'+param+'/'+this.props.apiState.keyApiSimpok;
        console.log("getdriver = "+this.props.apiState.apiSimpok + data);
        axios.get(this.props.apiState.apiSimpok + data)
        .then(response => {
          //this._toggleLoading();
          console.log("DETAIL getCountDriver = "+JSON.stringify(response.data));
          let countCar = Object.keys(response.data.info).length;
          if(param == 0){
            this.setState({car_all: countCar});
          }
          if(param == 1){
            this.setState({car_avail: countCar});
          } else if(param == 2){
            this.setState({car_used: countCar});
            var carPie = this.state.car_used/this.state.car_all;
            console.log("var carpie = "+carPie);
            this.setState({car_pie: carPie});
            console.log("car all = "+this.state.car_all);
            console.log("car avail = "+this.state.car_avail);
            console.log("car used = "+this.state.car_used);
            console.log("carpie state = "+this.state.car_pie);
          }
        }).catch((error) => {
          //this._toggleLoading();
          this.setState({ respon_msg: error, isPop: 1 });
          console.log("Error getCountDriver = "+error); //ini yang bener
          //this.toggleModal();
        });
    }
    
    gotoMenu(index){
        const {navigator} = this.props;
        var scr, ttl;
        if(index == 1){
            scr = 'panelg.plugin.placepicker';
            ttl = 'Select Destination';
            this.props.actions.setSimpokType("P2K");
            if(this.state.car_pie == 1){
                this.setState({respon_msg: "No Car Available"});
                this.toggleModal();
                return;
            }
        } else if(index == 2){
            scr = 'panelg.plugin.placepicker';
            ttl = 'Select Destination';
            this.props.actions.setSimpokType("PK2S");
            this.setState({respon_msg: "Under Construction"});
            this.toggleModal();
            return;
        }
        iconsLoadedFa.then(() => {
            navigator.push({
                screen: scr,
                title: ttl,
                navigatorButtons: {
                    leftButtons: [
                        {
                            icon: iconsMapFa['angle-left'],
                            buttonColor: 'white',
                            id: 'back',
                        }
                    ]
                }
            });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
    }

    componentDidMount(){
        this.getCountDriver(0);
        this.getCountDriver(1);
        this.getCountDriver(2);
        /*
        console.log("car used = "+this.state.car_used);
        console.log("car avail = "+this.state.car_avail);
        var carPie = this.state.car_used/(this.state.car_avail+this.state.car_used);
        this.setState({car_pie: carPie});
        console.log("carpie = "+carPie);
        console.log("carpie state = "+this.state.car_pie);
        */
        /*
        var ar = [{"elements":[{"distance":{"text":"151 km","value":151003},
                "duration":{"text":"2 hours 46 mins","value":9966},"status":"OK"}]}]
                
        console.log("Device Manufacture = "+DeviceInfo.getManufacturer());
        console.log("Device Brand = "+DeviceInfo.getBrand());
        console.log("Device Model = "+DeviceInfo.getModel());
        console.log("Device System Name = "+DeviceInfo.getSystemName());
        console.log("Device System Version = "+DeviceInfo.getSystemVersion());
        console.log("Device Name = "+DeviceInfo.getDeviceName());
        console.log("Device User Agent = "+DeviceInfo.getUserAgent());
        console.log("Device Country = "+DeviceInfo.getDeviceCountry());
        console.log("Device Phone Number = "+DeviceInfo.getPhoneNumber());
        */
        
    }

    simpokInput(){
        iconsLoadedFa.then(() => {
            console.log("modul ok");
            this.props.navigator.push({
                screen: 'panelg.plugin.TopTabs',
                title: 'Simpok Input',
                navigatorButtons: {
                    leftButtons: [
                        {
                            icon: iconsMapFa['angle-left'],
                            buttonColor: 'white',
                            id: 'back',
                        }
                    ]
                },
                topTabs: [{
                  screenId: 'panelg.simpok.inputp2k',
                  title: 'P2K',
                }, {
                  screenId: 'panelg.simpok.inputpk2s',
                  title: 'PK2S',
                }],
            });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
    };

    static navigatorStyle = {
        tabBarHidden: true
    }

    menuChartDashboard(){
        return(
            <View style={{
                marginTop: 40,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                
                <Progress.Circle
                    style={{
                        margin: 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    color={'red'}
                    thickness={10}
                    size={250}
                    showsText={true}
                    textStyle={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        fontSize: 25
                    }}
                    formatText={()=>{return `${this.state.car_avail}`+" Ready"+"\n"+`${this.state.car_used}`+" Bussy"}}
                    progress={this.state.car_pie}
                    indeterminate={false}
                />
                
                {/*
                <PieChart
                    chart_wh={250}
                    series={[8,2]}
                    sliceColor={['#F44336','#2196F3']}
                />
                
                <View style={{
                    marginTop: 10,
                    flexDirection: 'row',
                    marginBottom: 20,
                }}>
                    <View style={{
                        flexDirection: 'column'
                    }}>
                        <View style={{width: 20, height: 20, backgroundColor: '#F44336', marginRight: 5, marginBottom: 5}}/>                        
                        <View style={{width: 20, height: 20, backgroundColor: '#2196F3', marginRight: 5}}/>
                    </View>
                    <View style={{
                        flexDirection: 'column',
                        justifyContent: 'center'
                    }}>
                        <Text style={{marginBottom: 5}}>8 Cars Bussy</Text>                        
                        <Text>2 Cars Available</Text>
                    </View>
                </View>
                */}

            </View>
        );
    }

    menuButton(){
        return(
            <View>
                <View style={GeneralStyle.menuHomeContainer}>
                    <TouchableOpacity style={GeneralStyle.buttonHomeMenu}
                        onPress={debounce(this.gotoMenu.bind(this, 1), 1000, {
                            leading: true,
                            trailing: false
                        })}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <IconFA name="car" size={iconMenuSize} color='white'/>
                            <Text style={GeneralStyle.textMenuButtonHomeMenu}>P2K</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={GeneralStyle.separatorButtonHomeSpace}/>
                    <TouchableOpacity style={GeneralStyle.buttonHomeMenu}
                        //onPress={this.gotoMenu.bind(this, 2)}>
                        onPress={debounce(this.gotoMenu.bind(this, 2), 1000, {
                            leading: true,
                            trailing: false
                        })}>
                        <View style={GeneralStyle.buttonIconContainer} >
                            <IconFA name="car" size={iconMenuSize} color='white'/>
                            <Text style={GeneralStyle.textMenuButtonHomeMenu}>PK2S</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    toggleModal(){
        this.setState({ isModalVisible: !this.state.isModalVisible });
        console.log("toggle modal");
    }

    closeBack(){
        if(this.state.isPop == 0){
          this.toggleModal();
        } else{
          const {navigator} = this.props;
            this.props.navigator.pop({
            //screen: 'panelg.attendance.menu',
            animated: true, // does the pop have transition animation or does it happen immediately (optional)
            //animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
          });
        }
    }

  render() {
      const _menuButton = this.menuButton();
      const _menuChartDashboard = this.menuChartDashboard();
    return (
      // Try setting `flexDirection` to `column`.
      <ImageBackground
            source={background_image}
            style={GeneralStyle.imageBackground}
        >
      <View style={{ //parent column
          //flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 10
      }}>
        <StatusBar barStyle='light-content' />
        <Modal isVisible={this.state.isModalVisible}>
            <View style={GeneralStyle.modalParent}>
                <View style={GeneralStyle.modalContentAlert}>
                    <Text>{this.state.respon_msg}</Text>
                    <TouchableOpacity onPress={this.closeBack.bind(this)}>
                        <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        {_menuChartDashboard}
      </View>
      {_menuButton}
      </ImageBackground>
    );
  }
};

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SimpokMenu);
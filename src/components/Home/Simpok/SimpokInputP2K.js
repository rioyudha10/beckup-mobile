import React, { Component } from 'react';
import {
    AppRegistry, View, StatusBar, Button, Text, ScrollView, TextInput,
    TouchableOpacity, Keyboard, Picker, AsyncStorage, ImageBackground, Dimensions, StyleSheet
} from 'react-native';
import { TextField } from 'react-native-material-textfield';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { Fumi } from 'react-native-textinput-effects';
import {anim_gratika_red, background_image, iconMenuSize} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import LottieView from 'lottie-react-native';
import axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import Modal from 'react-native-modal';
var screen = Dimensions.get('window');
import MapView from 'react-native-maps';
import DeviceInfo from 'react-native-device-info';
import Geolocation from 'react-native-geolocation-service';
import VIcon from '../../../styles/VIcon';
import {openLightbox, openLightboxLoading} from '../HomeNav';
import Loading from 'react-native-whc-loading';


const ASPECT_RATIO = screen.width / screen.height * 0.5

export class SimpokInputP2K extends Component {
    constructor(props) {
        super(props);

        this.state = {
            url: "http://www.gratika.co.id/api/web_service/api/",
            s_nik: "",
            lat_sinpok : -6.200110360379439,
            long_sinpok : 106.79951207712293,
            s_kode_divisi: "",
            s_kode_subdiv: "",
            s_kode_level: "",
            error: "",
            s_tujuan: "",
            s_alamat_tujuan: "",
            s_alamat_asal: "",
            s_keperluan: "",
            s_tanggal: "",
            s_jam: "",
            s_jam_jemput: "",
            s_proyek: "",
            s_proyek_array: [],
            s_id_ket: "0",
            s_ket: "",
            s_ket_props: [
                {label: 'Ditunggu', value: '2'},
                {label: 'Dijemput', value: '3'},
                {label: 'Diantar', value: '1'},
                {label: 'Antar-Jemput', value: '4'},
            ],
            isWaktuJemput: false,
            respon_msg: "",
            isPop: 0,
            isModalVisible: false,
            isModalLoading: false,
            mapRegion: {
                latitude: -6.2001023,
                longitude: 106.799506,
                latitudeDelta: 0.0922,
                longitudeDelta: ASPECT_RATIO,
            },
        }
    }

    static navigatorStyle = {
        tabBarHidden: true
    }

    state = { isDatePickerVisible: false, isTimePickerVisible: false, 
        isTimePickerJemputVisible: false };

    _showDatePicker = () => {
        this.setState({ isDatePickerVisible: true })
        Keyboard.dismiss();
    };

    _showTimePicker = () => {
        this.setState({ isTimePickerVisible: true })
        Keyboard.dismiss();
    }

    _showTimePickerJemput = () => {
        this.setState({ isTimePickerJemputVisible: true })
        Keyboard.dismiss();
    }

    _hideDatePicker = () => this.setState({ isDatePickerVisible: false });
    _hideTimePicker = () => this.setState({ isTimePickerVisible: false });
    _hideTimePickerJemput = () => this.setState({ isTimePickerJemputVisible: false });

    _handleDatePicked = (date) => {
        this._hideDatePicker();
        Moment.locale('en');
        this.setState({ s_tanggal: Moment(date).format('YYYY-MM-DD') })
    };

    _handleTimePicked = (time) => {
        this._hideTimePicker();
        Moment.locale('en');
        this.setState({ s_jam: Moment(time).format('HH:mm:ss') })
    };

    _handleTimePickedJemput = (time) => {
        this._hideTimePickerJemput();
        Moment.locale('en');
        this.setState({ s_jam_jemput: Moment(time).format('HH:mm:ss') })
    };

    async getProyek() {
        try {
            let response = await fetch(this.props.apiState.apiSimpok +
                '4' + '/' +
                this.props.profileState.nik + '/' +
                '0' + '/' +
                '0' + '/' +
                this.props.apiState.keyApiSimpok
            );

            let responseJSON = await response.json();
            if(response.status == 200 && response.status < 300){
                this.setState({error: ""});
                //let responseJSON = await response.json();
                //console.log(responseJSON);
                console.log("hasil json rst = "+responseJSON.rst);
                //this.storeToken(res);
                if(responseJSON.rst == 1){
                    console.log("getProyek sukses");
                    var textInfo = responseJSON.info;
                    this.setState({s_proyek_array: textInfo.map(
                        function(item){
                            return {
                                key: item.idProyek,
                                proyek: item.namaProyek
                            }
                        })
                    });

                } else{
                    console.log(responseJSON.info);
                    this.setState({ respon_msg: responseJSON.info, isPop: 1 });
                    // this._toggleModal(); 
                    const data = {
                        act: false,
                        message: 'Please Complete All The Field',
                        confirm: 'OK',
                        status: 0
                    }
                    openLightbox(data, 0, this.props.navigator);
                    console.log("masuk");  
                }
            } else{
                let error = res;
                throw error;
            }
        } catch (error) {
            console.log("error: " + error);
        }
    }

    componentDidMount(){
        this.getProyek();
        this.getGeocode();
        this.findLocation();
        this.getGeopoint();
        // this.refs.loading1.show();
        // setTimeout(() => {
        //     this.refs.loading1.close();
        // }, 2000);
    }

    getGeocode() {
        axios.get(this.props.apiState.apiReverseLocation+ this.props.simpokState.simpok_lat +','+ this.props.simpokState.simpok_long +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
        .then(response => {
            console.log("response alamat" + response);
            console.log("lat props :" + this.props.simpokState.simpok_lat);
            this.setState({
                s_alamat_tujuan: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
                lat_sinpok : this.props.simpokState.simpok_lat,
                long_sinpok : this.props.simpokState.simpok_long
            });
            // this._toggleLoading();
        }).catch((error) => { // catch is called after then
            this.setState({ 
                error: error.message,
            });
            // this._toggleLoading();
        });
    }

    getGeopoint() {
        axios.get(this.props.apiState.apiReverseLocation + this.state.mapRegion.latitude +','+  this.state.mapRegion.longitude +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
        .then(response => {
            console.log(JSON.stringify(response.data) + " from getGeopoint");
            this.setState({
                s_alamat_asal: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
            });
            // this._toggleLoading();
           
        }).catch((error) => { // catch is called after then
            this.setState({ 
                error: error.message,
            });
            // this._toggleLoading();
        });
    }

    closeBack(){
        const {navigator} = this.props;
        this.props.navigator.popToRoot({
            //screen: 'panelg.attendance.menu',
            animated: true, // does the pop have transition animation or does it happen immediately (optional)
            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
        });
        this.props.navigator.handleDeepLink({
            link: 'tab3/in/any/format',
            payload: '' // (optional) Extra payload with deep link
        });
    }

    
    openLightboxLoading = () => {
        openLightboxLoading(this.props.actions, this.props.navigator);
    }

    submitP2K() {
        console.log("NIK = "+this.props.profileState.nik);
        console.log("tujuan: "+this.state.s_alamat_tujuan);
        console.log("keperluan: "+this.state.s_keperluan);
        console.log("tanggal : "+this.state.s_tanggal);
        console.log("jam : "+this.state.s_jam);
        console.log("proyek : "+this.state.s_proyek);
        console.log("keterangan : "+this.state.s_ket);
        console.log("waktu jemput : "+this.state.s_jam_jemput);
        this.refs.loading1.show();
        if(this.state.s_alamat_tujuan == '' || this.state.s_keperluan == '' || 
            this.state.s_tanggal == '' || this.state.s_jam == '' || 
            this.state.s_proyek == '' || this.state.s_proyek == '0' || this.state.s_id_ket == '0'){
            this.refs.loading1.close();
            this.setState({ respon_msg: 'Please Complete All Field' });
            const data = {
                act: false,
                message: 'Please Complete All The Field',
                confirm: 'OK',
                status: 0
            }
            openLightbox(data, 0, this.props.navigator);
            console.log("masuk");
            
            return;
        }
        // this._toggleLoading();
        console.log("tujuan: "+this.state.s_alamat_tujuan);
        console.log("keperluan: "+this.state.s_keperluan);
        console.log("tanggal : "+this.state.s_tanggal);
        console.log("jam : "+this.state.s_jam);
        console.log("proyek : "+this.state.s_proyek);
        console.log("keterangan : "+this.state.s_ket);
        console.log("waktu jemput : "+this.state.s_jam_jemput);
        var namaHari = this.state.s_tanggal+" "+this.state.s_jam;
        Moment.locale('en');
        console.log("var namaHari : "+namaHari);
        //this.setState({ s_tanggal: Moment(date).format('YYYY-MM-DD') })
        var momentHari = Moment(namaHari, "YYYY-MM-DD HH:mm:ss");
        console.log("hasil nama hari : "+momentHari.format('dddd'));
        console.log("hasil angka hari : "+momentHari.format('d'));
        var angkaHari = momentHari.format('d');
        var namaHariID = "";
        if(angkaHari==0){
            namaHariID = "Minggu";
        } else if(angkaHari==1){
            namaHariID = "Senin";
        } else if(angkaHari==2){
            namaHariID = "Selasa";
        } else if(angkaHari==3){
            namaHariID = "Rabu";
        } else if(angkaHari==4){
            namaHariID = "Kamis";
        } else if(angkaHari==5){
            namaHariID = "Jumat";
        } else if(angkaHari==6){
            namaHariID = "Sabtu";
        }
        console.log("hasil nama hari Indonesia : "+namaHariID);

        if(this.state.s_id_ket != 4){
            this.setState({ s_jam_jemput: "0" });
        }

        this.saveP2K(namaHariID);
    }

    async saveP2K(paramNamaHariID){
        var repCharDestination = this.state.s_alamat_tujuan;
        repCharDestination = repCharDestination.replace("/"," ");
        this.setState({s_alamat_tujuan : repCharDestination});
        console.log("alamat tujuan = "+this.state.s_alamat_tujuan);
        console.log("repchar = "+repCharDestination.replace("/"," "));
        var param = this.props.profileState.nik+"|"+this.props.profileState.divisi+
                        "|"+this.props.profileState.subdiv+"|"+paramNamaHariID+" "+this.state.s_tanggal+
                        "|"+this.state.s_jam+"|"+this.state.s_alamat_tujuan+"|"+repCharDestination+
                        "|"+this.state.s_keperluan+"|"+this.state.s_proyek+"|"+this.state.s_ket+
                        "|"+this.state.s_jam_jemput+"|"+this.props.profileState.level;
        var param2 = "/"+"0"+"/"+"0"+"/"+this.props.apiState.keyApiSimpok;
        console.log("param 1 = "+ param);
        console.log("link api = "+this.props.apiState.apiSimpok);
        axios.get(this.props.apiState.apiSimpok+"5/"+encodeURIComponent(param)+param2)
        .then(response => {
            // this._toggleLoading();  
            // this.openLightboxLoading();
            this.refs.loading1.close();
            const data = {
                act: true,
                goAct: ()=>this.closeBack(),
                message: response.data.info,
                confirm: 'Done',
                status: 0
            }
            openLightbox(data, 500, this.props.navigator);
            console.log("RESPONSE saveP2K = "+JSON.stringify(response.data));            
            this.setState({ respon_msg: response.data.info, isPop: 1 });
            // this._toggleModal();
           
        }).catch((error) => {
            // this._toggleLoading();       
            // this.openLightboxLoading();    
            this.refs.loading1.close();
            console.log("Error saveP2K = "+error); //ini yang bener
            this.setState({ respon_msg: error, isPop: 0 });
            const data = {
                act: false,
                message:  respon_msg,
                confirm: 'OK',
                status: 0
            }
            openLightbox(data, 0, this.props.navigator);
        });
    }

    splitPlace(alamat){
        let add = alamat;
        var result = add.split(",");
        return result[0] ;
        
    }

    pickerKeteranganOnChange(value, index){
        console.log("onchange picker keterangan");
        this.setState({ s_ket: value });
        this.setState({ s_id_ket: index });
        console.log("s_ket : "+this.state.s_ket);
        console.log("value : "+value);
        console.log("index : "+index);
        console.log("tujuan : "+this.state.s_tujuan);
        //this.setState({ s_tujuan : ""});
        if(index == 4){
            this.setState({ isWaktuJemput: true });
        } else{
            this.setState({ isWaktuJemput: false });
            this.setState({ s_jam_jemput: "" });
        }
        console.log("iswaktuJemput : "+this.state.isWaktuJemput);
    }

    focusWaktuJemput(){
        if(isWaktuJemput == true){
            this._showTimePickerJemput();
        }
    }

    menuSimpokAlamatContainer(){
        console.log("alamat asal :" + this.state.s_alamat_asal);
        return(
            <View style = {{ marginLeft : 5, marginRight : 5, borderBottomWidth: 1, borderColor : 'grey' }}>
                <View style = {{ height : 30, flexDirection : 'row', alignItems : 'center', paddingLeft : 10}}>
                    <VIcon type={VIcon.TYPE_FONTAWESOME5} name="map-pin" size={15} color='#FF0000'/>
                    <Text style = {{marginLeft : 10, fontSize : 15, fontWeight : 'bold'}}>{this.splitPlace(this.state.s_alamat_asal)} </Text>
                </View>
                <View style = {{ height : 30, flexDirection : 'row', alignItems : 'center', paddingLeft : 10}}>
                    <VIcon type={VIcon.TYPE_FONTAWESOME5} name="ellipsis-v" size={15} color='grey'/> 
                </View>
                <View style = {{ height : 30, flexDirection : 'row', alignItems : 'center', paddingLeft : 10}}>
                    <VIcon type={VIcon.TYPE_FONTAWESOME5} name="map-pin" size={15} color='#00FF00'/>
                    <Text style = {{marginLeft : 10, fontSize : 15, fontWeight : 'bold'}}> {this.splitPlace(this.state.s_alamat_tujuan)}  </Text>
                </View>
                {/* <TextField
                    label='Alamat Tujuan'
                    //onChangeText={(val) => this.setState({ s_alamat_tujuan: val })}
                    editable={false}
                    value={this.state.s_alamat_tujuan}
                    lineWidth={0}
                    multiline={true}
                /> */}
            </View>
        );
    }

    menuSimpokContainer(){
        return (
            <View>
                <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
                    <Text style = {{marginLeft : 10}}>  Tujuan </Text>
                </View>
                <View style={styles.textAreaContainer} >
                    <TextInput
                        style={{ height: 40, marginRight : 10, marginLeft : 10}}
                        underlineColorAndroid="transparent"
                        placeholderTextColor="grey"
                        numberOfLines={10}
                        multiline={true}
                        editable={false}
                        value={this.state.s_alamat_tujuan}
                        lineWidth={0}
                        multiline={true}
                    />
                </View>
                <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
                    <Text style = {{marginLeft : 10}}>Tipe</Text>
                </View>
                <View style={styles.textAreaContainer} >
                    <Picker
                    style={{ height: 40, marginRight : 10, marginLeft : 10}}
                    // selectedValue={this.state.jenis_izin}
                    // onValueChange={(itemValue, itemIndex) => this.setState({ jenis_izin: itemValue })}
                    >
                    <Picker.Item label="-Select Subject-" value="0" />
                    <Picker.Item label="P2K" value="1" />
                    <Picker.Item label="P2KS" value="2" />
                    </Picker>        
                </View>
                <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
                    <Text style = {{marginLeft : 10}}>Proyek</Text>
                </View>
                <View style={styles.textAreaContainer} >
                    <Picker
                    style={{ height: 40, marginRight : 10, marginLeft : 10}}
                    selectedValue={this.state.s_proyek}
                    onValueChange={(itemValue, itemIndex) => this.setState({ s_proyek: itemValue })}
                    >
                    <Picker.Item label="Proyek" value="0" />
                    {this.state.s_proyek_array.map((res, i) => {return <Picker.Item value={res.key} label={"" + res.proyek} key={res.key}/> })}
                    </Picker>        
                </View>
                <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
                    <Text style = {{marginLeft : 10}}>  Keperluan </Text>
                </View>
                <View style={styles.textAreaContainer} >
                    <TextInput
                        style={styles.textArea}
                        underlineColorAndroid="transparent"
                        placeholderTextColor="grey"
                        numberOfLines={10}
                        multiline={true}
                        value={this.state.s_keperluan}
                        onChangeText={(val) => this.setState({ s_keperluan: val })}
                    />
                </View>
                <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
                    <Text style = {{marginLeft : 10}}> Jadwal Perjalanan </Text>
                </View>
                <View style = {{flexDirection : 'row', width : screen.width}}>
                    <View style = {{flexDirection : 'row', width : screen.width*0.5, marginLeft : 10, alignItems : 'center'}}>
                        <View style={styles.DateAreaContainer} >
                            <TextInput
                                style={styles.DateArea}
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                numberOfLines={10}
                                multiline={true}
                                onFocus={this._showDatePicker}
                                value={this.state.s_tanggal}
                                onChangeText={(val) => this.setState({ s_tanggal: val })}
                            />
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isDatePickerVisible}
                            mode={'date'}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDatePicker}
                        />
                        <VIcon type={VIcon.TYPE_FONTAWESOME} name="calendar" size={30} color='#000000'/>
                    </View>
                    <View style = {{flexDirection : 'row', width : screen.width*0.5, alignItems : 'center'}}>
                        <View style={styles.DateAreaContainer} >
                            <TextInput
                                style={styles.DateArea}
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                numberOfLines={10}
                                multiline={true}
                                onFocus={this._showTimePicker}
                                value={this.state.s_jam}
                                onChangeText={(val) => this.setState({ s_jam: val })}
                            />
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isTimePickerVisible}
                            mode={'time'}
                            onConfirm={this._handleTimePicked}
                            onCancel={this._hideTimePicker}
                        />
                        <VIcon type={VIcon.TYPE_FONTAWESOME} name="clock-o" size={30} color='#000000'/>
                    </View>
                </View>
                <View style = {{flexDirection : 'row', paddingTop : 10, paddingRight : 10, marginBottom : 10}}>
                    <Text style = {{marginLeft : 10}}> Jenis </Text>
                </View>
                <View style={styles.textAreaContainer} >
                    <Picker
                        selectedValue={this.state.s_ket}
                        //onValueChange={(itemValue, itemIndex) => this.setState({ s_ket: itemValue })}>
                        onValueChange={this.pickerKeteranganOnChange.bind(this)}>
                        <Picker.Item label="--Keterangan--" value="0" />
                        {this.state.s_ket_props.map((res, i) => {return <Picker.Item value={res.value} label={"" + res.label} key={i}/> })}
                    </Picker>        
                </View>
                {/* <Fumi
                    label={'Tujuan'}
                    iconClass={IconFA}
                    iconName={'edit'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    value={this.state.s_tujuan}
                    onChangeText={(val) => this.setState({ s_tujuan: val })}
                />

                <Fumi
                    label={'Keperluan'}
                    iconClass={IconFA}
                    iconName={'edit'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    value={this.state.s_keperluan}
                    onChangeText={(val) => this.setState({ s_keperluan: val })}
                />

                <View style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row'
                }}>
                    <ScrollView keyboardShouldPersistTaps="always" style={{
                        backgroundColor: 'red'
                    }}>
                        <Fumi
                            label={'Tanggal'}
                            iconClass={IconFA}
                            iconName={'calendar'}
                            iconColor={'#f95a25'}
                            iconSize={20}
                            onFocus={this._showDatePicker}
                            value={this.state.s_tanggal}
                            onChangeText={(val) => this.setState({ s_tanggal: val })}
                        />
                        <DateTimePicker
                            isVisible={this.state.isDatePickerVisible}
                            mode={'date'}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDatePicker}
                        />
                    </ScrollView>
                    <ScrollView keyboardShouldPersistTaps="always" style={{
                        backgroundColor: 'red'
                    }}>
                        <Fumi
                            label={'Jam'}
                            iconClass={IconFA}
                            iconName={'clock-o'}
                            iconColor={'#f95a25'}
                            iconSize={20}
                            onFocus={this._showTimePicker}
                            value={this.state.s_jam}
                            onChangeText={(val) => this.setState({ s_jam: val })}
                        />

                        <DateTimePicker
                            isVisible={this.state.isTimePickerVisible}
                            mode={'time'}
                            onConfirm={this._handleTimePicked}
                            onCancel={this._hideTimePicker}
                        />
                    </ScrollView>
                </View>

                <Picker
                    selectedValue={this.state.s_proyek}
                    onValueChange={(itemValue, itemIndex) => this.setState({ s_proyek: itemValue })}>
                    <Picker.Item label="Proyek" value="0" />
                    {this.state.s_proyek_array.map((res, i) => {return <Picker.Item value={res.key} label={"" + res.proyek} key={res.key}/> })}
                </Picker>

                <Picker
                    selectedValue={this.state.s_ket}
                    //onValueChange={(itemValue, itemIndex) => this.setState({ s_ket: itemValue })}>
                    onValueChange={this.pickerKeteranganOnChange.bind(this)}>
                    <Picker.Item label="--Keterangan--" value="0" />
                    {this.state.s_ket_props.map((res, i) => {return <Picker.Item value={res.value} label={"" + res.label} key={i}/> })}
                </Picker> */}
            </View>
        );
    }

    menuSimpokWaktuJemputContainer(){
        return(
            <View style = {{flexDirection : 'column'}}>
                <View style = {{flexDirection : 'row', paddingTop : 10, marginBottom : 10}}>
                    <Text style = {{marginLeft : 5}}> Jam Jemput </Text>
                </View>
                <View style = {{ flexDirection : 'row', alignItems : 'center', marginLeft : 10 }}>
                    <View style={{   
                        // marginRight : 10,
                        width : screen.width * 0.86,
                        borderColor: 'black',
                        borderWidth: 1,
                        marginRight : 10
                    }} >
                        <TextInput
                            style={{height : 40, width : screen.width * 0.85}}
                            underlineColorAndroid="transparent"
                            placeholderTextColor="black"
                            numberOfLines={10}
                            multiline={true}
                            onFocus={this._showTimePickerJemput}
                            value={this.state.s_jam_jemput}
                            onChangeText={(val) => this.setState({ s_jam_jemput: val })}
                        />
                    </View>
                    <DateTimePicker
                        isVisible={this.state.isTimePickerJemputVisible}
                        mode={'time'}
                        onConfirm={this._handleTimePickedJemput}
                        onCancel={this._hideTimePickerJemput}
                    />
                    <VIcon type={VIcon.TYPE_FONTAWESOME} name="clock-o" size={30} color='#000000'/>
                </View>
            </View>
            // <View>
            //     <Fumi
            //         label={'Jam'}
            //         iconClass={IconFA}
            //         iconName={'edit'}
            //         iconColor={'#f95a25'}
            //         iconSize={20}
            //         onFocus={this._showTimePickerJemput}
            //         value={this.state.s_jam_jemput}
            //         onChangeText={(val) => this.setState({ s_jam_jemput: val })}
            //     />
            //     <DateTimePicker
            //         isVisible={this.state.isTimePickerJemputVisible}
            //         mode={'time'}
            //         onConfirm={this._handleTimePickedJemput}
            //         onCancel={this._hideTimePickerJemput}
            //     />
            // </View>
        );
    }

    _toggleModal(){
        this.setState({ isModalVisible: !this.state.isModalVisible });
        console.log("toggle modal");
    }

    _toggleLoading = () => {
        this.setState({ isModalLoading: !this.state.isModalLoading });
        console.log("ini toggle loading");
        //console.log("location state = "+JSON.stringify(this.props.locationState));
    }

    onLottieLoad = () => {
        console.log('play lottie');
        this.animation.play();
    }

    checkBrand(){
        var troubleBrand = false;
        for (var i = 0; i < this.props.apiState.brandPhone.length; i++) {
            if(DeviceInfo.getBrand() == this.props.apiState.brandPhone[i]){
                troubleBrand = true;
            }
        }
        return troubleBrand;
    }

    onRegionChange(region){
        console.log("region change nih = "+JSON.stringify(region));
        /*
        this.setState({
          lastLat: region.latitude,
          lastLong: region.longitude
        });
        */
    }

    findLocation(){
        Geolocation.getCurrentPosition(
            (position) => {
                console.log("STRINGIFY = "+JSON.stringify(position));
                console.log("latitude = "+position.coords.latitude);
                console.log("longitude = "+position.coords.longitude);
                this.setState(({mapRegion}) => ({mapRegion: {
                    ...mapRegion,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }}));
                this.onRegionChange(position.coords.latitude, position.coords.longitude),
                this.props.actions.setLocation(position.coords.latitude, position.coords.longitude);
                this.setState({visibleSpinner: false});
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
                this.setState({
                    visibleSpinner: false,
                    error_msg: error.message
                });
                const data = {
                    act: false,
                    message: this.state.error_msg,
                    confirm: 'OK',
                    status: 0
                  }
                //   openLightbox(data, 0, this.props.navigator);
            },
            { enableHighAccuracy: true, timeout: 5000, maximumAge: 2000 }
        );
    }

    onRegionChangeApi(region, latitude, longitude) {
        this.setState({
            mapRegion: region,
            // If there are no new values set the current ones
            latitude: latitude || this.state.latitude,
            longitude: longitude || this.state.longitude
        });
    }


    onRegionChangeComplete(region){
        var lat,long;
        if(region.longitude != '0'){
            lat = region.latitude;
            long = region.longitude;
            console.log("region change complete nih = "+JSON.stringify(region));
            if(this.checkBrand()){
                this.setState(({mapRegion}) => ({mapRegion: {
                    ...mapRegion,
                    latitude: lat,
                    longitude: long,
                }}));
            } else{
                this.props.actions.setSimpokDestination(lat,long);
            }
            
            // axios.get(this.props.apiState.apiReverseLocation+ region.latitude +','+ region.longitude +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
            // .then(response => {
            //     /*
            //     console.log(response);
            //     this.setState({
            //         place: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
            //     });
            //     */
            //     this.GooglePlacesRef.setAddressText(response.data.results[0].formatted_address);
            // }).catch((error) => { // catch is called after then
            //     this.setState({ 
            //         error: error.message,
            //     });
            // });
        }
        /*
        this.setState({
          //lastLat: region.latitude,
          //lastLong: region.longitude
          error_msg: 'berhasil'
        });
        */

        //console.log("last lat = "+this.state.lastLat);
        //console.log("last long = "+this.state.lastLong);
        //this.setRegion("berhasil");
        //this.onRegionChangeApi(region, region.latitude, region.longitude);
    }

    closeBack(){
        //this.setState({ isModalVisible: false });
        if(this.state.isPop == 0){
            this._toggleModal();
        } else{
            //this._toggleModal();
            const {navigator} = this.props;
            this.props.navigator.popToRoot({
                //screen: 'panelg.attendance.menu',
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
            });
            this.props.navigator.handleDeepLink({
                link: 'tab3/in/any/format',
                payload: '' // (optional) Extra payload with deep link
            });
        }
    }

    render() {
        const _menuSimpokAlamatContainer = this.menuSimpokAlamatContainer();
        const _menuSimpokContainer = this.menuSimpokContainer();
        const _menuSimpokWaktuJemputContainer = this.menuSimpokWaktuJemputContainer();
        console.log("ini lat sinpok :" + this.state.lat_sinpok)
        console.log("ini long sinpok :" + this.state.long_sinpok)
        console.log("ini lat point :" + this.state.mapRegion.latitude)
        console.log("ini long point :" + this.state.mapRegion.longitude)
        return (
            <View style = {{ height : screen.height, width : screen.width}}>
                <Loading ref='loading1' image = {require('../../image/loading.png')}/>
                <Modal isVisible={this.state.isModalLoading}>
                    <View style={GeneralStyle.modalLoading}>
                        <LottieView
                            onLayout={this.onLottieLoad}
                            ref={animation => {
                                this.animation = animation;
                            }}
                            style={GeneralStyle.lottieLoading}
                            source={anim_gratika_red}
                        />
                    </View>
                </Modal>
                <View style = {{ height : screen.height*0.4, width: screen.width}}>
                    <MapView
                        style={ styles.map }
                        region={this.state.mapRegion}
                        //onRegionChange={this.onRegionChange.bind(this)}
                        //onPanDrag={this.onPanDragStop.bind(this)}
                        showsMyLocationButton={true}
                        provider='google'
                    >
                            <MapView.Marker  coordinate={
                                {
                                    latitude: (this.state.mapRegion.latitude),
                                    longitude: (this.state.mapRegion.longitude),
                                }
                            } 
                            pinColor='red'
                            />
                            <MapView.Marker  coordinate={{
                                latitude: (this.state.lat_sinpok),
                                longitude: (this.state.long_sinpok),
                            }} pinColor='green'/>
                    </MapView>
                </View>
                <View style = {{ height : 90, width: screen.width }}>
                    {_menuSimpokAlamatContainer}
                </View>
                <View style = {{ height : 300, width: screen.width }}>
                    <ScrollView>
                        {_menuSimpokContainer}
                        {this.state.isWaktuJemput ? _menuSimpokWaktuJemputContainer : null}
                        <View style={{paddingTop:20, justifyContent: 'center',
                            alignItems: 'center', marginBottom: 20}}>
                            <View style={GeneralStyle.buttonContainerAttendanceCheck}>
                                <TouchableOpacity style={{width : screen.width* 0.7, height : screen.height* 0.08, backgroundColor : 'red', justifyContent : 'center', alignItems : 'center'}} onPress={this.submitP2K.bind(this)}>
                                    <Text style={GeneralStyle.textButtonSubmitAttendance}> Ajukan </Text>
                                </TouchableOpacity>
                            </View>
                         </View>
                    </ScrollView> 
                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    map: {
        // position: 'absolute',
        // top: 50,
        // left: 0,
        // right: 0,
        // bottom: 0,
        flex: 1,
        width: screen.width,
        height: screen.height*0.5,
    },
    AddressAreaContainer: {
        borderColor: '#FFFFFF',
        borderWidth: 1,
        paddingHorizontal : 10
    },
    textAreaContainer: {
        borderColor: 'black',
        borderWidth: 1,
        paddingHorizontal : 10,
        marginRight : 10, 
        marginLeft : 10
    },
    AddressArea: {
        height: 40,
    },
    textArea: {
        height: 60,
        justifyContent: "flex-start"
    },
    DateArea: {
        height: 40,
        width : 150,
    },
    DateAreaContainer: {
        marginRight : 10,
        borderColor: 'black',
        borderWidth: 1,
    },
})

/*
SimpokInputP2K.navigationOptions = {
    title: 'P2K',
}
*/

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        simpokState: state.simpok
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SimpokInputP2K);
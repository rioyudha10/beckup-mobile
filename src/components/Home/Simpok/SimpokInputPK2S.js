import React, { Component } from 'react';
import { AppRegistry, View, StatusBar, Button, Text, ScrollView, 
    TouchableOpacity, Keyboard, Picker, ImageBackground } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { Fumi } from 'react-native-textinput-effects';
import {anim_gratika_red, background_image, iconMenuSize} from '../../../styles/assets';
import GeneralStyle from '../../../styles/GeneralStyle';
import LottieView from 'lottie-react-native';
import * as axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action/index';
import Modal from 'react-native-modal';

export class SimpokInputPK2S extends Component {
    constructor(props){
        super(props);

        this.state = {
            s_nama: "",
            s_alamat: "",
            s_tlp: "",
            s_tujuan: "",
            s_tanggal_pinjam: "",
            s_lama_pinjam: "",
            s_tanggal_kembali: "",
            s_jam_kembali: "",
            respon_msg: "",
            isPop: 0,
            isWaktuJemput: false,
            isModalVisible: false,
            isModalLoading: true,
        }
    }

    static navigatorStyle = {
        tabBarHidden: true
    }

    componentDidMount(){
        this.getGeocode();
    }

    getGeocode() {
        axios.get(this.props.apiState.apiReverseLocation+ this.props.simpokState.simpok_lat +','+ this.props.simpokState.simpok_long +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
        .then(response => {
            console.log(response);
            this.setState({
                s_tujuan: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
            });
            this._toggleLoading();
        }).catch((error) => { // catch is called after then
            this.setState({ 
                error: error.message,
            });
            this._toggleLoading();
            this._toggleModal();
        });
    }

    state = { isDatePickerVisiblePinjam: false, isDatePickerVisibleKembali: false, isTimePickerVisible: false};
 
    _showDatePickerPinjam = () => {
      this.setState({ isDatePickerVisiblePinjam: true })
      Keyboard.dismiss();
    };

    _showDatePickerKembali = () => {
      this.setState({ isDatePickerVisibleKembali: true })
      Keyboard.dismiss();
    };

    _showTimePicker = () => {
        this.setState({ isTimePickerVisible: true })
        Keyboard.dismiss();
    }
 
    _hideDatePickerPinjam = () => this.setState({ isDatePickerVisiblePinjam: false });
    _hideDatePickerKembali = () => this.setState({ isDatePickerVisibleKembali: false });
    _hideTimePicker = () => this.setState({ isTimePickerVisible: false });
 
    _handleDatePickedPinjam = (date) => {
        this._hideDatePickerPinjam();
        Moment.locale('en');
        this.setState({s_tanggal_pinjam: Moment(date).format('YYYY-MM-DD')})
    };

    _handleDatePickedKembali = (date) => {
        this._hideDatePickerKembali();
        Moment.locale('en');
        this.setState({s_tanggal_kembali: Moment(date).format('YYYY-MM-DD')})
    };

    _handleTimePicked = (time) => {
        this._hideTimePicker();
        Moment.locale('en');
        this.setState({s_jam_kembali: Moment(time).format('HH:mm:ss')})
    };

    onClick(){
        this.setState({ respon_msg: 'Under Construction', isPop: 1 });
        this._toggleModal();
    }

    menuSimpokTujuanContainer(){
        return(
            <View style={{
                paddingLeft: 12,
                paddingRight: 12
            }}>
                <TextField
                    label='Tujuan'
                    editable={false}
                    value={this.state.s_tujuan}
                    lineWidth={0}
                    multiline={true}
                />
            </View>
        );
    }

    menuSimpokPeminjamContainer(){
        return(
            <View>
                <Fumi
                    label={'Alamat Peminjam'}
                    iconClass={IconFA}
                    iconName={'edit'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    value={this.state.s_alamat}
                    onChangeText={(val) => this.setState({ s_alamat: val })}
                />

                <Fumi
                    label={'No. Telepon'}
                    iconClass={IconFA}
                    iconName={'phone'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    value={this.state.s_tlp}
                    onChangeText={(val) => this.setState({ s_tlp: val })}
                />
            </View>
        );
    }

    menuSimpokTanggalWaktuContainer(){
        return(
            <View>
                <Fumi
                    label={'Tanggal Pinjam'}
                    iconClass={IconFA}
                    iconName={'calendar'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    onFocus={this._showDatePickerPinjam}
                    value={this.state.s_tanggal_pinjam}
                    onChangeText={(val) => this.setState({ s_tanggal_pinjam: val })}
                />
                <DateTimePicker
                    isVisible={this.state.isDatePickerVisiblePinjam}
                    mode={'date'}
                    onConfirm={this._handleDatePickedPinjam}
                    onCancel={this._hideDatePickerPinjam}
                />

                <Fumi
                    label={'Tanggal Pengembalian'}
                    iconClass={IconFA}
                    iconName={'calendar'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    onFocus={this._showDatePickerKembali}
                    value={this.state.s_tanggal_kembali}
                    onChangeText={(val) => this.setState({ s_tanggal_kembali: val })}
                />
                <DateTimePicker
                    isVisible={this.state.isDatePickerVisibleKembali}
                    mode={'date'}
                    onConfirm={this._handleDatePickedKembali}
                    onCancel={this._hideDatePickerKembali}
                />
                <Fumi
                    label={'Jam Pengembalian'}
                    iconClass={IconFA}
                    iconName={'clock-o'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    onFocus={this._showTimePicker}
                    value={this.state.s_jam_kembali}
                    onChangeText={(val) => this.setState({ s_jam_kembali: val })}
                />
                <DateTimePicker
                    isVisible={this.state.isTimePickerVisible}
                    mode={'time'}
                    onConfirm={this._handleTimePicked}
                    onCancel={this._hideTimePicker}
                />
            </View>
        );
    }

    _toggleModal(){
        this.setState({ isModalVisible: !this.state.isModalVisible });
        console.log("toggle modal");
    }

    onLottieLoad = () => {
        console.log('play lottie');
        this.animation.play();
    }

    closeBack(){
        //this.setState({ isModalVisible: false });
        if(this.state.isPop == 0){
            this._toggleModal();
        } else{
            this._toggleModal();
            const {navigator} = this.props;
            this.props.navigator.popToRoot({
                //screen: 'panelg.attendance.menu',
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
            });
            this.props.navigator.handleDeepLink({
                link: 'tab3/in/any/format',
                payload: '' // (optional) Extra payload with deep link
            });
        }
    }

    _toggleLoading = () => {
        this.setState({ isModalLoading: !this.state.isModalLoading });
        console.log("ini toggle loading");
        //console.log("location state = "+JSON.stringify(this.props.locationState));
    }

  render() {      
    const _menuSimpokTujuanContainer = this.menuSimpokTujuanContainer();
    const _menuSimpokPeminjamContainer = this.menuSimpokPeminjamContainer();
    const _menuSimpokTanggalWaktuContainer = this.menuSimpokTanggalWaktuContainer();
    return (
        <ImageBackground
            source={background_image}
            style={GeneralStyle.imageBackground}
        >
            <ScrollView keyboardShouldPersistTaps="always" 
                style={GeneralStyle.scrollContainerAttendance}>
                <StatusBar barStyle='light-content' />
                <Modal isVisible={this.state.isModalLoading}>
                    <View style={GeneralStyle.modalLoading}>
                        <LottieView
                            onLayout={this.onLottieLoad}
                            ref={animation => {
                                this.animation = animation;
                            }}
                            style={GeneralStyle.lottieLoading}
                            source={anim_gratika_red}
                        />
                    </View>
                </Modal>
                <Modal isVisible={this.state.isModalVisible}>
                    <View style={GeneralStyle.modalParent}>
                        <View style={GeneralStyle.modalContentAlert}>
                            <Text style={GeneralStyle.textModalContentAlert}>{this.state.respon_msg}</Text>
                            <TouchableOpacity style={GeneralStyle.modalButtonAlert} onPress={this.closeBack.bind(this)}>
                                <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                {_menuSimpokTujuanContainer}
                {_menuSimpokPeminjamContainer}
                {_menuSimpokTanggalWaktuContainer}
        
                <View style={{ //parent column
                //flex: 1,
                flexDirection: 'row',
                justifyContent: 'center',
                paddingHorizontal: 10
                }}>
                </View>

                <View style={{paddingTop:20, justifyContent: 'center',
                    alignItems: 'center', marginBottom: 20}}>
                    <View style={GeneralStyle.buttonContainerAttendanceCheck}>
                        <TouchableOpacity style={GeneralStyle.buttonSubmitAttendance} onPress={this.onClick.bind(this)}>
                            <Text style={GeneralStyle.textButtonSubmitAttendance}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        
            </ScrollView>
        </ImageBackground>
    );
  }
};

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        simpokState: state.simpok
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SimpokInputPK2S);
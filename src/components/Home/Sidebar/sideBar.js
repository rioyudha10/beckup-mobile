import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import {connect} from 'react-redux';

const screen = Dimensions.get("window")
import startMainTab from '../../Bantuan/tentang';

export class SideBar extends Component {

  onButtonPress = () => {
    startMainTab();
  }

  goScreen(index){
    var scr, ttl;
    if(index == 1){
        scr = 'panelg.pengaturan';
        ttl = 'Pengaturan'
        tp = null
    } else if(index == 2){
        scr = 'panelg.bantuan';
        ttl = 'Bantuan';
        tp = null
    } else if(index == 3){
      scr = 'panelg.tentang';
      ttl = 'Tentang';
      tp = null
    }
    iconsLoadedFa.then(() => {
        this.props.navigator.showModal({
            screen: scr,
            title: ttl,
            topTabs: tp,
            navigatorStyle: {
                navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
                navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
                navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
                navBarHidden: false, // make the nav bar hidden
                navBarComponentAlignment: 'center',
                navBarTitleTextCentered: true,
            }, 
            navigatorButtons: {
                // rightButtons: [
                //     {
                //         icon: iconsMapFa['check'],
                //         buttonColor: 'white',
                //         id: 'submit',
                //     }
                // ],
                leftButtons: [
                    {
                        icon: iconsMapFa['angle-left'],
                        text : 'Batal',
                        buttonColor: 'white',
                        id: 'back',
                    }
                ]
            },
        });
    }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
    });
}

  render() {
    return (
      <View style = { styles.container }>
        <View style = {styles.imageContainer }>
          <Image 
              resizeMode='cover'
              style={{
                  height: 120,
                  width: 120,
                  borderRadius : 120/2,
                  alignSelf: 'center',
                  borderWidth : 2,
                  borderColor : '#FFFFFF'
              }}
              source={{uri:this.props.apiState.pathImageUser+this.props.profileState.pic}}
          />
          <Text style = {{ color : '#FFFFFF', fontSize : 18, fontWeight : 'bold'}}> { this.props.profileState.full_name } </Text>
          <Text style = {{ color : '#FFFFFF', fontSize : 15, fontWeight : 'bold'}}> { this.props.profileState.nik } </Text>
        </View>
        {/* <View style = { styles.settingItem }>
            <TouchableOpacity style = { styles.button }>
              <Text style = { styles.textButton }> Pengaturan Aplikasi </Text>
            </TouchableOpacity>
        </View> */}
        <View style = { styles.settingItem }>
          <TouchableOpacity style = { styles.button } onPress={()=>this.goScreen(2)}>
            <Text style = { styles.textButton }> Bantuan </Text>
          </TouchableOpacity>
        </View>
        <View style = { styles.settingItem }>
          <TouchableOpacity style = { styles.button } onPress={()=>this.goScreen(3)}>
            <Text style = { styles.textButton }> Tentang </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: screen.width*0.7,
    height : screen.height,
    backgroundColor : '#FFFFFF'
  },
  imageContainer: {
    backgroundColor: 'white',
    width: screen.width*0.7,
    height : screen.height*0.3,
    backgroundColor : '#FF0000',
    justifyContent : 'center',
    alignItems : 'center',
  },
  settingItem : {
    width : screen.width*0.7,
    height : screen.height*0.08,
    borderBottomWidth : 1,
    borderColor : '#000000',
    backgroundColor : '#FFFFFF',
    alignItems : 'center',
    justifyContent : 'center'
  },
  textButton : {
    color : '#000000',
    fontSize : 15
  },
  button : {
    width : screen.width*0.65,
    height : screen.height*0.1,
    justifyContent : 'center',
    alignItems : 'center'
  }
});


function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        locationState: state.location,
	};
}

export default connect(mapStateToProps)(SideBar);

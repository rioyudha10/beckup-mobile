import React, { Component } from 'react';
import {
    View, ScrollView, Image, Text, StyleSheet, TextInput, TouchableOpacity,
    TouchableHighlight, StatusBar, Alert, AsyncStorage, Keyboard, Dimensions, KeyboardAvoidingView
} from 'react-native';
import { iconsMapFa, iconsLoadedFa } from '../../config/app-icons-fa';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import {connect} from 'react-redux';
import * as appActions from '../../redux/action';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import Loading from 'react-native-whc-loading';

var screen = Dimensions.get('window');

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nik: "",
            password: "",
        }
    }

    async storeToken(result) {
        try {
            console.log("result.info.username = "+result.info.username);
            await AsyncStorage.setItem('isLogin', '1');
            await AsyncStorage.setItem('nik', result.info.username);
            await AsyncStorage.setItem('password', result.info.password);
            await AsyncStorage.setItem('full_name', result.info.full_name);
            await AsyncStorage.setItem('nmdivisi', result.info.nmdivisi);
            await AsyncStorage.setItem('nmsubdiv', result.info.nmsubdiv);
            await AsyncStorage.setItem('image_uri', '');
            await AsyncStorage.setItem('image_status', '0');
            await AsyncStorage.setItem('user_type', result.info.user_type);
        } catch (error) {
            console.log("something went wrong");
            this.setState({ error: error });
            console.log("error " + error);
        }
    }

    hashmd5(textin) {
        md5 = require('js-md5');
        md5(textin);
        var hash = md5.create();
        hash.update(textin);
        return hash.hex();
    }

    onLoginPressed = () =>{
        Keyboard.dismiss();
        this.refs.loading1.show();
        if (this.state.nik == "" || this.state.password == "") {
            const data = {
                act: false,
                message: 'Please Fill The User & Password!',
                confirm: 'OK',
                status: 0
            }
            this.openLightbox(data, 0);
            this.refs.loading1.close();
            return;
        } else{
            this.refs.loading1.close();
            axios.get(this.props.apiState.apiSimpok +
                '1' + '/' +
                this.state.nik + '/' +
                this.hashmd5(this.state.password) + '/' +
                '0' + '/' +
                this.props.apiState.keyApiAbsen)
                .then((response)=>{
                    this.refs.loading1.close();
                    console.log(JSON.stringify(response));
                    if(response.status >= 200 && response.status < 300){
                        if(response.data.rst == 1){
                            this.storeToken(response.data);
                            this.props.actions.setIdUser(response.data.info.iduser);
                            this.props.actions.setProfile(
                                response.data.info.username, response.data.info.full_name, response.data.info.password,
                                response.data.info.email, response.data.info.level, response.data.info.divisi,
                                response.data.info.nmdivisi, response.data.info.subdiv, response.data.info.nmsubdiv,
                                response.data.info.jabatan, response.data.info.nmjbtn, response.data.info.setatasan,
                                response.data.info.user_type);
                            this.props.actions.setImageUser(response.data.info.pic);
                            this.props.actions.login();
                        } else{
                            const data = {
                                act: false,
                                message: response.data.info,
                                confirm: 'OK',
                                status: 0
                            }
                            this.openLightbox(data, 500);
                        }
                    } else{
                        const data = {
                            act: false,
                            message: "Error Login "+response.status,
                            confirm: 'OK',
                            status: 0
                        }
                        this.openLightbox(data, 500);
                    }
                }).catch((err)=>{
                    this.refs.loading1.close();
                    const data = {
                        act: false,
                        message: "Error Login "+err,
                        confirm: 'OK',
                        status: 0
                    }
                    this.openLightbox(data, 500);
                });
        }
    }

    openLightbox = (data, timeout) => {
        setTimeout(()=>{
          this.props.navigator.showLightBox({
            screen: 'panelg.modal.alert',
            style: {
                backgroundBlur: 'dark',
                backgroundColor: '#44444480',
                tapBackgroundToDismiss: false
            },
            passProps: {
                data: () => (data),                
            }
          })
        },timeout);
    }

    openLightboxLoading = () => {
        this.props.actions.setLoading(true);
        this.props.navigator.showLightBox({
            screen: 'panelg.modal.loadingGratika',
            style: {
                backgroundBlur: 'dark',
                backgroundColor: '#44444480',
                tapBackgroundToDismiss: false
            },
        })
    }
    
    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <Loading ref='loading1' image = {require('../image/loading.png')}/>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../../images/logo_gina.png')}
                    />
                </View>
                <View styles={styles.formContainer}>
                    <View style={styles.formfieldContainer}>
                        <TextInput
                            onChangeText={(val) => this.setState({ nik: val })}
                            placeholder="NIK"
                            placeholderTextColor="rgba(0,0,0,0.3)"
                            returnKeyType="next"
                            onSubmitEditing={() => this.passwordInput.focus()}
                            autoCorrect={false}
                            style={styles.input}
                            underlineColorAndroid='rgba(0,0,0,0)'
                        />

                        <TextInput
                            onChangeText={(val) => this.setState({ password: val })}
                            placeholder="Password"
                            placeholderTextColor="rgba(0,0,0,0.3)"
                            returnKeyType="go"
                            secureTextEntry={true}
                            style={styles.input}
                            ref={(input) => this.passwordInput = input}
                            onSubmitEditing={() => this.onLoginPressed}
                        />

                        <TouchableOpacity style={styles.buttonContainer} onPress={this.onLoginPressed}>
                            <Text style={styles.buttonText}> LOGIN</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EDEDED'
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        width: 150,
        height: 150,
        resizeMode: 'contain'
    },
    title: {
        color: '#FFF',
        marginTop: 10,
        textAlign: 'center',
        opacity: 0.9
    },
    formfieldContainer: {
        padding: 20
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.7)',
        marginBottom: 10,
        color: '#000000',
        paddingHorizontal: 10
    },
    buttonContainer: {
        backgroundColor: '#E31F26',
        paddingVertical: 10,
        marginBottom: 100
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFF',
        fontWeight: '700',
    },
});

function mapStateToProps(state, ownProps) {
	return {
        apiState: state.api,
        spinkitState: state.spinkit,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
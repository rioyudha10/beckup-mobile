import React, { Component } from 'react';
import {StyleSheet, View, Text, Dimensions, Button, TouchableOpacity, ScrollView, 
  AsyncStorage, ImageBackground} from 'react-native';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import LottieView from 'lottie-react-native';
import anim from '../../images/bouncy_mapmaker.json';
import { TextField } from 'react-native-material-textfield';
import IconFA from 'react-native-vector-icons/FontAwesome';
import IconE from 'react-native-vector-icons/Entypo';
import IconMI from 'react-native-vector-icons/MaterialIcons';
import Spinkit from 'react-native-spinkit';
import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Fumi, Kohana } from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import MapView from 'react-native-maps';
import Lightbox from 'react-native-lightbox';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../redux/action/index';
import Image from 'react-native-image-progress';
import Progress from 'react-native-progress/Pie';
import GeneralStyle from '../../styles/GeneralStyle';
import {anim_gratika_red, background_image} from '../../styles/assets';
import VIcon from '../../styles/VIcon';
import moment from 'moment';
import Loading from 'react-native-whc-loading';

var screen = Dimensions.get('window');

export class NotificationAttendance extends Component {
  constructor(props){
    super(props);
    this.state = {
      nik: '',
      in: 'IN',
      out: 'OUT',
      cuti : 'CUTI',
      latitude_in: -6.200110360379439,
      longitude_in: 106.79951207712293,
      latitude_out: -6.200110360379439,
      longitude_out: 106.79951207712293,
      tanggal_awal_cuti : '',
      tanggal_akhir_cuti : '',
      keperluan_cuti : '',
      alamat_cuti : '',
      error: '',
      place_masuk: '',
      apikey:  ' AIzaSyA3dZ9IpmuK7rIWaKqw0JhDTziZDHBROiA ',
      lineWidth: 0,
      img_uri: '',
      image_in : '',
      image_out : '',
      tanggal_in : '',
      tanggal_out : '',
      absen_in : '',
      absen_out : '',
      keterangan_masuk: '',
      keterangan_keluar: '',
      alamat_checkout : 'll',
      respon_msg: '',
      own: '',
      status_approval: '',
      uris: this.props.apiState.pathImageAbsen + this.props.notifState.notif_absen_image,
      shortName: '',
      isModalLoading: false,
      visibleSpinner: false,
      isVisibleSpinkit: true,
      isModalVisible: false,
      isBack: true,

      mapRegion: {
        latitude: -36.82339,
        longitude: -73.03569,
        latitudeDelta: 0.0018,
        longitudeDelta: 0.0018,
      },

      //spinner
      visibleSpinner: false,
      //spinkit
      isVisibleSpinkit: true,
      //modal
      isModalVisible: false,
    };
  }

  static navigatorStyle = {
    tabBarHidden: true,
    tabBarShowLabels: 'hidden',
  }

  onLottieLoad = () => {
    // console.log('play lottie');
    this.animation.play();
  }

  _markerLottie(){
      return(
          <LottieView
              onLayout={this.onLottieLoad}
              ref={animation => {
                  this.animation = animation;
              }}
              style={GeneralStyle.markerLottieAttendance}
              source={anim}
          />
      );
  }
  
  componentWillReceiveProps(){
  // console.log("INI WILL")
    if (this.state.longitude_out && this.state.latitude_out){
      // console.log("INI ADA DATA")
      // this.getGeocodeCheckIn();
    } 
  }

  componentDidMount() {
    this.refs.loading1.show();    
    this.getDetailStatus()
    this.getDetailCheckIn()
    this.getDetailCheckOut()
    this.getDetailCuti()
    this.getGeocodeCheckIn();
    // this.getGeocode()
  }


  // componentWillUnmount() {
  //   this.props.onUnmount(this);
  // }

  getDetailStatus(){
    var data = new FormData();
    data.append('keyapi', this.props.apiState.keyApiAbsen);
    data.append('dataapi', 3);
    data.append('nik', this.props.profileState.nik);
    data.append('id_pesan', this.props.notifState.notif_id);
    data.append('own', this.props.notifState.notif_own);
    data.append('source', this.props.notifState.notif_sumber);
    data.append('type', this.props.notifState.notif_type);
    data.append('id_type', this.props.notifState.notif_id_type);
    data.append('jenis_pesan', this.props.notifState.notif_jenis);
    console.log("GETDETAIL = "+this.props.notifState);
    // console.log("GETDETAIL jenis_pesan = "+this.props.notifState.notif_jenis);
    // console.log("ini formdata " + JSON.stringify(data))
    const config = {
      headers: { 'content-type': 'multipart/form-data' },
      timeout: 10000
    }
    axios.post(this.props.apiState.apiAbsen, data, config)
    .then(response => {
      // console.log("AXIOS DETAIL STATUS = "+JSON.stringify(response.data));
      // console.log("BEFORE ACTION = "+JSON.stringify(this.props.notifState));
      this.refs.loading1.close();    
     if(response.data.info.type_pesan === 'IN'){
       this.setState({
        status_approval: response.data.info.status_in,
       })
     }
     else if(response.data.info.type_pesan === 'OUT'){
      this.setState({
        status_approval: response.data.info.status_out,
       })
     }
      //(notif_absen_id, notif_absen_tanggal, notif_absen_jam, notif_absen_image,
      //notif_absen_caption, notif_absen_latitude, notif_absen_longitude, notif_absen_status)
      // console.log("AFTER ACTION STATUS= "+JSON.stringify(this.props.notifState));
    })
    .catch((error) => {
      //console.log(error);
      this.refs.loading1.close();    ;
      console.log("================================================");
      console.log("Error gelondongan = "+error); //ini yang bener
      console.log("================================================");
    });
  }
  
  getDetailCheckIn(){
    var data = new FormData();
    data.append('keyapi', this.props.apiState.keyApiAbsen);
    data.append('dataapi', 3);
    data.append('nik', this.props.profileState.nik);
    data.append('id_pesan', this.props.notifState.notif_id);
    data.append('own', this.props.notifState.notif_own);
    data.append('source', this.props.notifState.notif_sumber);
    data.append('type', this.state.in);
    data.append('id_type', this.props.notifState.notif_id_type);
    data.append('jenis_pesan', this.props.notifState.notif_jenis);
    // console.log("GETDETAIL id_type = "+this.props.notifState.notif_id_type);
    // console.log("GETDETAIL jenis_pesan = "+this.props.notifState.notif_jenis);
    // console.log("ini formdata " + JSON.stringify(data))
    //console.log("state own = "+this.props.notifState.notif_own);
    const config = {
      headers: { 'content-type': 'multipart/form-data' },
      timeout: 10000
    }
    var res_msg;
    axios.post(this.props.apiState.apiAbsen, data, config)
      .then(response => {
        console.log("AXIOS DETAIL CHECK IN = "+JSON.stringify(response.data));
        // console.log("BEFORE ACTION = "+JSON.stringify(this.props.notifState));
        // this.props.actions.setNotifDetailAbsen(
        //   response.data.info.id_absen, response.data.info.tanggal_in, 
        //   response.data.info.absen_in, response.data.info.image_in, 
        //   response.data.info.caption_in, response.data.info.latitude_in, 
        //   response.data.info.longitude_in, response.data.info.status_in,
        // );
        // console.log(JSON.stringify(response.data.info.id_absen)+" "+JSON.stringify(response.data.info.tanggal_in));
        // console.log("ini pake in");
        this.setState({
          keterangan_masuk: response.data.info.caption_in,
          image_in : response.data.info.image_in,
          tanggal_in : response.data.info.tanggal_in,
          absen_in : response.data.info.absen_in
        });
        //(notif_absen_id, notif_absen_tanggal, notif_absen_jam, notif_absen_image,
        //notif_absen_caption, notif_absen_latitude, notif_absen_longitude, notif_absen_status)
        // console.log("AFTER ACTION CHECK IN= "+JSON.stringify(this.props.notifState));
        // this._toggleLoading();
      })
      .catch((error) => {
        //console.log(error);
        this.setState({ error, loading: false });
        console.log("================================================");
        console.log("Error gelondongan = "+error); //ini yang bener
        console.log("================================================");
        // this._toggleLoading();
      });
  }

  getDetailCheckOut(){
    var data = new FormData();
    data.append('keyapi', this.props.apiState.keyApiAbsen);
    data.append('dataapi', 3);
    data.append('nik', this.props.profileState.nik);
    data.append('id_pesan', this.props.notifState.notif_id);
    data.append('own', this.props.notifState.notif_own);
    data.append('source', this.props.notifState.notif_sumber);
    data.append('type', this.state.out);
    data.append('id_type', this.props.notifState.notif_id_type);
    data.append('jenis_pesan', this.props.notifState.notif_jenis);
    // console.log("GETDETAIL id_type = "+this.props.notifState.notif_id_type);
    // console.log("GETDETAIL jenis_pesan = "+this.props.notifState.notif_jenis);
    // console.log("ini formdata " + JSON.stringify(data))
    //console.log("state own = "+this.props.notifState.notif_own);
    const config = {
      headers: { 'content-type': 'multipart/form-data' },
      timeout: 10000
    }
    var res_msg;
    axios.post(this.props.apiState.apiAbsen, data, config)
      .then(response => {
        // console.log("AXIOS DETAIL CHECK Out = "+JSON.stringify(response.data));
        // console.log("BEFORE ACTION = "+JSON.stringify(this.props.notifState));
        // this.props.actions.setNotifDetailAbsen(
        //   response.data.info.id_absen, response.data.info.tanggal_out, 
        //   response.data.info.absen_out, response.data.info.image_out, 
        //   response.data.info.caption_out, response.data.info.latitude_out, 
        //   response.data.info.longitude_out, response.data.info.status_out
        // );
        // console.log(JSON.stringify(response.data.info.id_absen)+" "+JSON.stringify(response.data.info.tanggal_in));
        // console.log("ini pake in");
        this.setState({
          keterangan_keluar: response.data.info.caption_out,
          image_out : response.data.info.image_out,
          tanggal_out : response.data.info.tanggal_out,
          absen_out : response.data.info.absen_out,
          latitude_out :parseFloat(this.props.notifState.notif_absen_latitude),
          longitude_out : parseFloat(this.props.notifState.notif_absen_longitude),
        });
        // console.log ("lat_out axios =" + this.state.latitude_out)
        // console.log ("lang_out axios =" + this.state.longitude_out)
        //(notif_absen_id, notif_absen_tanggal, notif_absen_jam, notif_absen_image,
        //notif_absen_caption, notif_absen_latitude, notif_absen_longitude, notif_absen_status)
        // console.log("AFTER ACTION CHECK_OUT = "+JSON.stringify(this.props.notifState));
        // this._toggleLoading();
      })
      .catch((error) => {
        //console.log(error);
        this.setState({ error, loading: false });
        console.log("================================================");
        console.log("Error gelondongan = "+error); //ini yang bener
        console.log("================================================");
        // this._toggleLoading();
      });
  }

  getDetailCuti () {
    var data = new FormData();
    data.append('keyapi', this.props.apiState.keyApiAbsen);
    data.append('dataapi', 3);
    data.append('nik', this.props.profileState.nik);
    data.append('id_pesan', this.props.notifState.notif_id);
    data.append('own', this.props.notifState.notif_own);
    data.append('source', this.props.notifState.notif_sumber);
    data.append('type', this.state.cuti);
    data.append('id_type', this.props.notifState.notif_id_type);
    data.append('jenis_pesan', this.props.notifState.notif_jenis);
    console.log("GETDETAIL id_type = "+this.props.notifState.notif_id_type);
    console.log("GETDETAIL jenis_pesan = "+this.props.notifState.notif_jenis);
    console.log("ini formdata " + JSON.stringify(data))
    const config = {
      headers: { 'content-type': 'multipart/form-data' },
      timeout: 10000
    }
    axios.post(this.props.apiState.apiAbsen, data, config)
    .then(response => {
      // console.log("AXIOS DETAIL CUTI = "+JSON.stringify(response.data));
      console.log("BEFORE ACTION = "+JSON.stringify(this.props.notifState));
      // this.props.actions.setNotifDetailAbsenCuti(
      //   response.data.info.id_cuti, response.data.info.tanggal_cuti,
      //   response.data.info.tanggal_awal_cuti, response.data.info.tanggal_akhir_cuti,
      //   response.data.info.caption_cuti, response.data.info.lokasi_cuti,
      //   response.data.info.status_cuti
      // );
      console.log("ini pake in");
      this.setState({
        tanggal_awal_cuti : response.data.info.tanggal_awal_cuti,
        tanggal_akhir_cuti : response.data.info.tanggal_akhir_cuti,
        keperluan_cuti : response.data.info.caption_cuti,
        alamat_cuti : response.data.info.lokasi_cuti
      });
      console.log ("lat_out axios =" + this.state.latitude_out)
      console.log ("lang_out axios =" + this.state.longitude_out)
      //(notif_absen_id, notif_absen_tanggal, notif_absen_jam, notif_absen_image,
      //notif_absen_caption, notif_absen_latitude, notif_absen_longitude, notif_absen_status)
      console.log("AFTER ACTION = "+JSON.stringify(this.props.notifState));
      // this._toggleLoading();
    })
    .catch((error) => {
      //console.log(error);
      this.setState({ error, loading: false });
      console.log("================================================");
      console.log("Error gelondongan = "+error); //ini yang bener
      console.log("================================================");
      // this._toggleLoading();
    });
  }


  testClick(){
    console.log("test click");
  }

//   getGeocode() {
//     axios.get(this.props.apiState.apiReverseLocation+ this.state.latitude_out +','+ this.state.longitude_out +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
//     .then(response => {
//         console.log("response alamt out" + response.data.results[0].formatted_address);
//         console.log("lat props :" + this.props.simpokState.simpok_lat);
//         this.setState({
//             alamat_checkout : response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
//         });
//         // this._toggleLoading();
//     }).catch((error) => { // catch is called after then
//         this.setState({ 
//             error: error.message,
//         });
//         // this._toggleLoading();
//     });
// }

  getGeocodeCheckIn() {
    // console.log("ini getGeoCode =" + this.props.notifState.notif_absen_latitude +','+ this.props.notifState.notif_absen_longitude)
    // console.log("ini lat_out =" + this.state.latitude_out)
    // console.log("ini lang_out =" + this.state.longitude_out)
    axios.get(this.props.apiState.apiReverseLocation+ this.props.notifState.notif_absen_latitude +','+ this.props.notifState.notif_absen_longitude +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
   .then(response => {
      // this.props.actions.setLoading(false);
      console.log(response);
      this.setState({
        place_masuk: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
      });
    }).catch((error) => {
      this.props.actions.setLoading(false);
    });
  }

  approval(idApprove){
    // const idApprove 1 = Approve
    // idApprove 2 = Decline
    this.refs.loading1.show();  
    
    var data = new FormData();
    data.append('keyapi', this.props.apiState.keyApiAbsen);
    data.append('dataapi', 4);
    data.append('nik', this.props.profileState.nik);
    data.append('id_pesan', this.props.notifState.notif_id);
    data.append('approval', idApprove);
    data.append('id_type', this.props.notifState.notif_id_type);
    data.append('type', this.props.notifState.notif_type);

    const config = {
      headers: { 'content-type': 'multipart/form-data' },
      timeout: 10000
    }
    var res_msg;
    console.log("ini app" + JSON.stringify(data))
    axios.post(this.props.apiState.apiAbsen, data, config)
      .then(response => {
        console.log("INI HASIL RESPONSE AXIOS NOTIF = "+JSON.stringify(response.data));
        this.setState({
          respon_msg: response.data.info.proses
        });
        this.toggleModal();
        this.refs.loading1.close();  
      })
      .catch((error) => {
        //console.log(error);
        this.setState({ error, loading: false });
        this.setState({
          respon_msg: error
        });
        console.log("================================================");
        console.log("Error gelondongan = "+error); //ini yang bener
        console.log("================================================");
        this.toggleModal();
        this.refs.loading1.close();  
      });
  }

  closeBack(){
    //this.setState({ isModalVisible: false });
    const {navigator} = this.props;
    this.props.navigator.pop({
      //screen: 'panelg.attendance.menu',
      animated: true, // does the pop have transition animation or does it happen immediately (optional)
      animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
    });
  }

  toggleModal(){
    this.setState({ isModalVisible: !this.state.isModalVisible });
    console.log("toggle modal");
  }

  testMethod(){
    console.log("test method");
  }

  tanggalCheck(data){
    return moment(data).format("dddd, MMM Do YYYY")
  }

  photoContainer_1(){
    var uris = this.props.apiState.pathImageAbsen + this.state.image_in;
    console.log("uris" + uris)
    return(
      <View style={GeneralStyle.textAreaContainerCheckIn}>
        <Text style = {styles.dateText}> {this.tanggalCheck(this.state.tanggal_in)}, {this.state.absen_in} </Text>
        <Image style={GeneralStyle.imageCameraAttendance} source={{uri:uris}}/> 
        <Text style = {styles.dateText}> Check-In</Text>
        {/* <View style={GeneralStyle.imageCameraAttendance}>
            <VIcon type={VIcon.TYPE_FONTAWESOME} name="camera" size={70} color='#4F8EF7'/> 
        </View> */}
      </View>
    )

  }

  photoContainer_2(){
    var uri_in =  this.props.apiState.pathImageAbsen + this.state.image_in;
    var uri_out =  this.props.apiState.pathImageAbsen + this.state.image_out;
    return(
      <View style={GeneralStyle.textAreaContainerCheckOut} >
      <View style = {styles.boxRow}>
        <Text style = {styles.dateText}> {this.tanggalCheck(this.state.tanggal_in)}, {this.state.absen_in} </Text>
        <Image style={GeneralStyle.imageCameraAttendance} source={{uri:uri_in}}/> 
        <Text style = {styles.dateText}> Check-In </Text>
      </View>
      <View style = {styles.boxRow}>
      <Text style = {styles.dateText}> {this.tanggalCheck(this.state.tanggal_out)}, {this.state.absen_out} </Text>
        <Image style={GeneralStyle.imageCameraAttendance} source={{uri:uri_out}}/> 
        <Text style = {styles.dateText}> Check-Out</Text>
      </View>
    </View>
    )

  }

  splitPlace(alamat){
    let add = alamat;
    var result = add.split(",");
    return result[0];
    
  }

  checkContainer(){
    
    const status = this.props.notifState.notif_type
    // const status = "1"
    // console.log("status_jenis" + this.props.notifState.notif_absen_status)
    console.log("place" + this.state.place_masuk)
    return(
      <View style={{flex : 1}}>
        {/* <Text style = {styles.welcome}>Check-In</Text>
        <Text style = {styles.welcome}>Check-Out</Text> */}
        <Fumi
          label={'Lokasi Check In'}
          iconClass={FontAwesomeIcon}
          iconName={'map-pin'}
          iconColor={'grey'}
          iconSize={20}
          value= {this.splitPlace(this.state.place_masuk)}
          editable={false}
        />

        <Fumi
          label={'Lokasi Check Out'}
          iconClass={FontAwesomeIcon}
          iconName={'map-pin'}
          iconColor={'grey'}
          iconSize={20}
          value= {this.splitPlace(this.state.alamat_checkout)}
          editable={false}
        />
        {status === "1" ? (
          <Fumi
            label={'Keterangan'}
            iconClass={FontAwesomeIcon}
            iconName={'paperclip'}
            iconColor={'grey'}
            iconSize={20}
            // value={this.state.keterangan}
            value= {this.state.keterangan_keluar}
            editable={false}
          />
        ):(
          <Fumi
            label={'Keterangan'}
            iconClass={FontAwesomeIcon}
            iconName={'paperclip'}
            iconColor={'grey'}
            iconSize={20}
            value={this.state.keterangan_masuk}
            // value= {this.props.notifState.notif_absen_caption}
            editable={false}
          />
        )}   
        { status === "IN" ? (
            this.photoContainer_1()
        ):(
          this.photoContainer_2()
        )}
        {/* <View style={{flex: 1}}>
            <Lightbox>
              <Image 
                resizeMode="contain"
                style={styles.image}
                source={{uri:this.state.uris}}
                indicator={Progress}
                indicatorProps={{
                  //size: 80,
                  borderWidth: 0,
                  color: '#4F8EF7',
                  unfilledColor: 'rgba(200, 200, 200, 0.2)'
                }}
              />
            </Lightbox>
        </View>
        <View style={{width: screen.width*0.7, backgroundColor: 'white'}}>
          <Fumi
            label={'Nama'}
            iconClass={FontAwesomeIcon}
            iconName={'user'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={this.state.shortName}
            editable={false}
          />
          <Fumi
            label={'Waktu'}
            iconClass={FontAwesomeIcon}
            iconName={'clock-o'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={`${this.props.notifState.notif_absen_tanggal} ${this.props.notifState.notif_absen_jam}`}
            editable={false}
          />
        </View> */}
      </View>
    );
  }

  izinContainer(){
    return(
      <View style={{ /*justifyContent: 'center', alignItems: 'center', flexDirection: 'row',*/}}>
        <View style={{backgroundColor: 'white'}}>
          <Fumi
            label={'Nama'}
            iconClass={FontAwesomeIcon}
            iconName={'user'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={this.props.profileState.level == '3' ? this.props.profileState.full_name : this.props.notifState.notif_dari}
            editable={false}
          />
          <Fumi
            label={'Tanggal'}
            iconClass={FontAwesomeIcon}
            iconName={'calendar'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={this.props.notifState.notif_absen_izin_tanggal}
            editable={false}
          />
          <Fumi
            label={'Jenis Izin'}
            iconClass={FontAwesomeIcon}
            iconName={'info'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={this.props.notifState.notif_absen_izin_jenis == 'i' ? 'Izin' : 'Sakit'}
            editable={false}
          />
        </View>
      </View>
    );
  }

  cutiContainer(){
    return(
      <View style={{ /*justifyContent: 'center', alignItems: 'center', flexDirection: 'row',*/}}>
        <View style={{backgroundColor: 'white'}}>
          <Fumi
            label={'Nama'}
            iconClass={FontAwesomeIcon}
            iconName={'user'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={this.props.profileState.level == '3' ? this.props.profileState.full_name : this.props.notifState.notif_dari}
            editable={false}
          />
          <View style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row'
                }}>
                <ScrollView style={{
                    backgroundColor: 'red'
                }}>
                <Fumi
                    label={'Tanggal Mulai'}
                    iconClass={IconFA}
                    iconName={'calendar'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    value={ this.state.tanggal_awal_cuti }
                    editable={false}
                />
                </ScrollView>
                <ScrollView style={{
                    backgroundColor: 'green'
                }}>
                <Fumi
                    label={'Tanggal Selesai'}
                    iconClass={IconFA}
                    iconName={'calendar'}
                    iconColor={'#f95a25'}
                    iconSize={20}
                    value={ this.tanggal_akhir_cuti }
                    editable={false}
                />
                </ScrollView>
                </View>
          <Fumi
            label={'Keperluan'}
            iconClass={FontAwesomeIcon}
            iconName={'info'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={ this.state.keperluan_cuti }
            editable={false}
          />
          <Fumi
            label={'Alamat Ketika Cuti'}
            iconClass={FontAwesomeIcon}
            iconName={'info'}
            iconColor={'#f95a25'}
            iconSize={20}
            value={ this.state.lokasi_cuti }
            editable={false}
          />
        </View>
      </View>
    );
  }

  _toggleLoading = () => {
    this.setState({ isModalLoading: !this.state.isModalLoading });
    console.log("ini toggle loading");
  }

  mapContainer(){
    return(
      <View style={{height: screen.height*0.4}}>
        <MapView
            style={styles.map}
            //showsUserLocation={true}
            followUserLocation={false}
            zoomEnabled={false}
            pitchEnabled={false}
            scrollEnabled={false}
            loadingEnabled={true}
            region={this.state.mapRegion}>
            <MapView.Marker
                coordinate={{
                    latitude: (this.state.mapRegion.latitude) || -36.82339,
                    longitude: (this.state.mapRegion.longitude) || -73.03569,
            }}>
            <View>
                    <IconE name="location-pin" size={50} color="#E31F26"/>
                </View>
            </MapView.Marker>
        </MapView>
      </View>
    );
  }

  approvalContainer(){
    return(
      <View>
         {this.statusContainer()}
        { this.props.profileState.level == "4" ? (
          <View style={{paddingTop:20, justifyContent: 'center', alignItems: 'center', flexDirection: 'row',}}>
            <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
              <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.approval(2)}}>
                <Text style={styles.buttonText}>Decline</Text>
              </TouchableOpacity>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
              <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.approval(1)}}>
                <Text style={styles.buttonText}>Approve</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (null)}
      </View>
    );
  }

  statusContainer(){
    console.log("status approval = " + this.state.status_approval)
    return(
      <Fumi
        label={'Status'}
        iconClass={FontAwesomeIcon}
        iconName={'info-circle'}
        iconColor={'#f95a25'}
        iconSize={20}
        value={this.state.status_approval == '1' ? 'Approved' : 
              this.state.status_approval == '2' ? 'Decline' : 'Waiting for approval' }
        editable={false}
      />
    );
  }

  openLightboxLoading = () => {
    this.props.actions.setLoading(true);
    this.props.navigator.showLightBox({
        screen: 'panelg.modal.loadingGratika',
        style: {
            backgroundBlur: 'dark',
            backgroundColor: '#44444480',
            tapBackgroundToDismiss: false
        },
    })
  }

  render() {
    const markerLottie = this._markerLottie();
    console.log("notifstateDari"  + JSON.stringify( this.props.notifState))
    // console.log("profilState"  + JSON.stringify( this.props.profileState))
    const _checkContainer = this.checkContainer();
    const _izinContainer = this.izinContainer();
    const _cutiContainer = this.cutiContainer();
    const _mapContainer = this.mapContainer();
    const _approvalContainer = this.approvalContainer();
    const _statusContainer = this.statusContainer();
    return (
      <ImageBackground
        source={background_image}
        style={GeneralStyle.imageBackground}
      >
      <Loading ref='loading1' image = {require('../../components/image/loading.png')}/>
      <ScrollView>
      <View style={{paddingLeft: 15, paddingRight: 15,}}>
        <Spinner visible={this.state.visibleSpinner} /*textContent={"Loading..."}*/ textStyle={{color: '#FFF'}}>
          <View style={styles.spinner}>
          <Spinkit isVisible={this.state.isVisibleSpinkit} size={this.state.sizeSpinkit} type={this.state.typeSpinkit} color={this.state.colorSpinkit}/>
          </View>
        </Spinner>
        <Modal isVisible={this.state.isModalVisible}>
            <View style={styles.modalParent}>
                <View style={styles.modalContent}>
                    <Text>{this.state.respon_msg}</Text>
                    <TouchableOpacity onPress={this.closeBack.bind(this)}>
                        <Text style={{color: 'red', marginTop: 10 }}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        <View>
          {this.props.notifState.notif_type == 'IN' || this.props.notifState.notif_type == 'OUT' ? _checkContainer : this.props.notifState.notif_type == 'IZIN' ? _izinContainer : _cutiContainer}
          { this.props.notifState.notif_dari !== 'ATTENDANCE' ? _approvalContainer : _statusContainer }
          </View>
      </View>
      </ScrollView>
      </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
  scrolview: {
    padding: 50, 
    justifyContent: 'center', 
    alignItems: 'center',
  },container: {
    width: Dimensions.get('window').width * 0.8,
    height: Dimensions.get('window').height * 0.6,
    //backgroundColor: '#ffffff',
    borderRadius: 5,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 17,
    fontWeight: '700',
  },
  content: {
    marginTop: 8,
  },
  buttonContainer: {
    backgroundColor: '#E31F26',
    //paddingVertical: 10,
    //marginBottom: 100
    width: screen.width*0.3,
    height: screen.height*0.06,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  buttonText: {
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 18
  },
  imageContainer:{
    justifyContent: 'center', 
    alignItems: 'center',
    position: 'relative',
    flex: 1,
    borderRadius: 100
    //height: 100,
    //width: 100,
    //height: screen.height*0.4
    //top: 0,
    //left: 0,
    //bottom: 0,
    //right: 0,
  },
  image: {
    flex: 1,
    //height: screen.height,
    //width: screen.width,
    //alignSelf: 'stretch',
    height: screen.height*0.1,
    borderRadius: screen.height*0.1*0.5,
    //borderRadius:100,
    //width: 100,
    //borderRadius: 100,
    //width: 100,
    //borderRadius: 100,
    //position: 'absolute',
    //top: 0,
    //left: 0,
    //bottom: 0,
    //right: 0,
  },
  spinner: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalParent: {
    overflow: 'hidden',
    //height: screen.height * 0.3,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    overflow: 'hidden',
    flex: 0,
    width: screen.width*0.9,
    height: screen.height*0.08,
    borderBottomWidth: 2,
    borderBottomColor: 'grey',
    backgroundColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  modalContent: {
    //backgroundColor: 'red',
    height: screen.height * 0.15,
    //padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
   map: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
  },

  welcome: {
    backgroundColor: '#FFFFFF',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 15,
    color: 'grey', 
    paddingTop: 10,
    paddingBottom : 10,
    paddingLeft : 10,  
  },

  welcome_2: {
    backgroundColor: '#FFFFFF',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 15,
    fontWeight: 'bold',
    color: 'black', 
    paddingTop: 10,
    paddingBottom : 10,
    borderTopWidth: 1,
    borderColor : 'grey',
  },

  dateText: {
    fontSize: 10,
    borderColor : 'black',
  },

  boxRow: {
    alignItems : 'center',
    marginRight : 10
  }
});

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        notifState: state.notification,
        profileState: state.profile,
        spinkitState: state.spinkit,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

//AppRegistry.registerComponent('HomeMenu', () => HomeMenu);
export default connect(mapStateToProps, mapDispatchToProps)(NotificationAttendance);
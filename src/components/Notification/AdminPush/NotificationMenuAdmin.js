import React, { Component } from "react";
import { StyleSheet, Alert, View, Text, FlatList, ActivityIndicator, 
  TouchableOpacity, AsyncStorage, TouchableHighlight, Dimensions, ImageBackground, 
  InteractionManager, SafeAreaView } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import IconFA from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import {AfterInteractions} from 'react-native-interactions';
import Modal from 'react-native-modal';
import GeneralStyle from '../../../styles/GeneralStyle';
import {anim_gratika_red, background_image} from '../../../styles/assets';
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';
const avatarAttendance = (<IconFA name="clock-o" size={30} color="#E31F26" />)
const avatarSimpok = (<IconFA name="car" size={28} color="#E31F26" />)
var screen = Dimensions.get('window');

export class NotificationMenuAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      banyakPesan: 0,
      error: null,
      refreshing: false,
      fcm_token: "",
      respon_msg: '',
      notif_status_absen: null,
      player_id: undefined,
      interactionsComplete: false,

      //spinner
      visibleSpinner: false,
      //spinkit
      isVisibleSpinkit: true,
      //modal
      isModalVisible: false,
    };
  }

  gotoSetting = () =>{
    this.setState({
      respon_msg: 'Under Construction'
    });
    this.toggleModal();
  }

  switchTab  = () => {
    this.props.navigator.switchToTab({
      //tabIndex: 2 // (optional) if missing, this screen's tab will become selected
    });
  }

  async componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({interactionsComplete: true});
    });
    if(this.props.profileState.nik == undefined){
      let nik = await AsyncStorage.getItem('nik');
      let password = await AsyncStorage.getItem('password');
      console.log("nik = "+nik);
      console.log("password = "+password);
      let response = await fetch(this.props.apiState.apiSimpok +
          '1' + '/' +
          nik + '/' +
          password + '/' +
          '0' + '/' +
          this.props.apiState.keyApiSimpok
      );

      let responseJSON = await response.json();
      if (response.status >= 200 && response.status < 300) {
          if (responseJSON.rst == 1) {
              this.props.actions.setProfile(
                  responseJSON.info.username, responseJSON.info.full_name, responseJSON.info.password,
                  responseJSON.info.email, responseJSON.info.level, responseJSON.info.divisi,
                  responseJSON.info.nmdivisi, responseJSON.info.subdiv, responseJSON.info.nmsubdiv,
                  responseJSON.info.jabatan, responseJSON.info.nmjbtn, responseJSON.info.setatasan,
                  responseJSON.info.user_type);
          }
      }
    }
    
    //this.makeRemoteRequest();
  }

  gotoScreen = () => {
    console.log("test goto screen");
    const {navigator} = this.props;
    var scr, ttl;
    scr = 'panelg.attendance.menu';
    ttl = 'Attendance Menu';
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: scr,
        title: ttl,
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
          passProps: {
            onUnmount: () => {
              console.log('Notif Detail dismissed');
              this.makeRemoteRequest();
            }
          }
      });
    }).catch((error) => {
      console.log("Modul error");
      console.log("" + error);
    });
  }

  bukaNotif = (idPesan, typePesan, jenisPesan, dariPesan, dariNikPesan, idType) =>{
    var own = 0;
    var dariOwn = '';
    console.log("dariNikPesan = "+dariNikPesan);
    console.log("profileState.nik = "+this.props.profileState.nik);
    if(dariNikPesan == this.props.profileState.nik){
      own = 1;
      dariOwn = this.props.profileState.full_name;
    } else{
      own = 0;
      dariOwn = dariPesan;
    }
    var notif_sumber = '0';
    var arr_typePesan = typePesan.split('-');
    this.props.actions.setNotifGeneral(idPesan, arr_typePesan[1], jenisPesan, dariOwn, dariNikPesan, own, notif_sumber, idType);
    const {navigator} = this.props;
    var index = 1;
    var scr, ttl;
    //console.log("type pesan = "+typePesan);
    //var arr_typePesan = typePesan.split('-');
    if(arr_typePesan[0] == 'ATTENDANCE'){ //buat buka detail attendance
      if(arr_typePesan[1] == 'IN' || arr_typePesan[1] == 'OUT' ||
          arr_typePesan[1] == 'IZIN' || arr_typePesan[1] == 'CUTI'){
        scr = 'panelg.notifscreen.attendance';
        ttl = 'Detail Notification';
      } else{
          scr = 'panelg.attendance.menu';
          ttl = 'Attendance';
      }
    } else if(arr_typePesan[0] == 'GSIMPOK'){
      if(arr_typePesan[1] == 'P2K' || arr_typePesan[1] == 'PK2S'){
        scr = 'panelg.notifscreen.simpok';
        ttl = 'Detail Notification';
      } else{
          scr = 'panelg.attendance.menu';
          ttl = 'Attendance';
      }
    } else{
      this.setState({
        respon_msg: 'Under Construction'
      });
      this.toggleModal();
      return;
    }
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: scr,
        title: ttl,
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
        passProps: {
          onUnmount: () => {
            console.log('Notif Detail dismissed');
            this.makeRemoteRequest();
          }
        }
      });
    }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
    });
  };

  makeRemoteRequest = () => {
    var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
    axios.get(this.props.apiState.apiSimpok + data)
    .then(response => {
      console.log("NOTIF PROGRESS = "+JSON.stringify(response.data));
      var countBadge;
      if(response.data.rst == 0){
        countBadge = 0;
      } else{
        countBadge = Object.keys(response.data.info).length;
      }
      this.props.actions.setNotifCount(countBadge);
      this.setState({
        banyakPesan: countBadge,
        data: response.data.info,
        error: response.data.info || null,
        loading: false,
        refreshing: false
      });
    }).catch((error) => {
      this.setState({ error, loading: false });
      console.log("Error gelondongan = "+error); //ini yang bener
    });
    this.props.actions.setNotifRemoteHistory(true);
    /*
    const { page, seed } = this.state;
    //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
      */
  };

  setBadge(count){
    this.props.navigator.setTabBadge({
      badge: count
    });
  }

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  toggleModal(){
    console.log("toggle modal");
    this.setState(
      {
        isModalVisible: !this.state.isModalVisible
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar placeholder="Type Here..." lightTheme round />;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

  renderMessage = () => {
    return <Text>Notification Empty</Text>;
  };

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    if (!this.state.interactionsComplete) {
      return <Text>loading...</Text>;
    }
    return (
      <SafeAreaView>
      <AfterInteractions>
      <ImageBackground
        source={background_image}
        style={GeneralStyle.imageBackground}
      >
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <Modal isVisible={this.state.isModalVisible}>
            <View style={GeneralStyle.modalParent}>
                <View style={GeneralStyle.modalContentAlert}>
                    <Text style={GeneralStyle.textModalContentAlert}>{this.state.respon_msgHistory}</Text>
                    <TouchableOpacity style={GeneralStyle.modalButtonAlert} onPress={this.toggleModal.bind(this)}>
                        <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        {this.state.banyakPesan == 0 ? <Message /> : 
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
              <ListItem
                roundAvatar
                component={TouchableHighlight}
                title={`${item.dari}`}
                subtitle={`${item.type}: ${item.waktu}`}
                avatar={ `${item.icon}`== 1 ? avatarSimpok : avatarAttendance }
                containerStyle={{ borderBottomWidth: 0 }}
                onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
              />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={this.renderSeparator}
          //ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />
        }
      </List>
      </ImageBackground>
      </AfterInteractions>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        notifState: state.notification,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export class Message extends Component {
  render(){
    return (
      <View style={{alignItems: 'center', justifyContent: 'center', }}>
        <Text>Notification Empty</Text>
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationMenuAdmin);
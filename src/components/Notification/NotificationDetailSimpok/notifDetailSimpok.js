import React, {Component} from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView, FlatList, TouchableHighlight } from 'react-native'
import { List, ListItem, SearchBar } from "react-native-elements";
import axios from 'axios'
import {connect} from 'react-redux';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';

const avatarP2KProgress = (<IconFA name="car" size={28} color="orange" />)
const avatarP2KAccept =  (<IconFA name="car" size={28} color="green" />)
const avatarP2KDecline=  (<IconFA name="car" size={28} color="red" />)


export class NotifDetailSimpok extends Component {

    constructor (props) {
        super(props)
        this.state = {
            progress : [],
            history : [],
            searchProgress : [],
            searchHistory : [],
            allData : [],
        }
    }

    componentDidMount () {
        this.getDetailProgress();
        this.getDetailHistory();
    }

    bukaNotif = (idPesan, typePesan, jenisPesan, dariPesan, dariNikPesan, idType) =>{
        var own = 0;
        var dariOwn = '';
        if(dariNikPesan == this.props.profileState.nik){
          own = 1;
          dariOwn = this.props.profileState.full_name;
        } else{
          own = 0;
          dariOwn = dariPesan;
        }
        var notif_sumber = '0';
        var arr_typePesan = typePesan.split('-');
        this.props.actions.setNotifGeneral(idPesan, arr_typePesan[1], jenisPesan, dariOwn, dariNikPesan, own, notif_sumber, idType);
        const {navigator} = this.props;
        var index = 1;
        var scr, ttl;
        //console.log("type pesan = "+typePesan);
        //var arr_typePesan = typePesan.split('-');
        arr_typePesan[0] == 'GSIMPOK'
        if(arr_typePesan[1] == 'P2K' || arr_typePesan[1] == 'PK2S'){
            scr = 'panelg.notifscreen.simpok';
            ttl = 'Detail Notification';
        } else{
            scr = 'panelg.attendance.menu';
            ttl = 'Attendance';
        }
        iconsLoadedFa.then(() => {
          navigator.push({
            screen: scr,
            title: ttl,
            navigatorButtons: {
              leftButtons: [
                {
                  icon: iconsMapFa['angle-left'],
                  buttonColor: 'white',
                  id: 'back',
                }
              ]
            },
            navigatorStyle: {
              navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
              navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
              navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
              navBarHidden: false, // make the nav bar hidden
              navBarComponentAlignment: 'center',
              navBarTitleTextCentered: true,
            },
          });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
      };

    getDetailProgress () {
        var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        axios.get ( this.props.apiState.apiSimpok + data )
            .then ( response  => {
                this.filterDetailProgress(response.data.info)
            }).catch((error) => {
                this.setState({ error, loading: false });
                console.log("Error gelondongan = "+error); //ini yang bener
            });
    }

    getDetailHistory () {
        var data = '11002/0/2/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        axios.get ( this.props.apiState.apiSimpok + data )
            .then ( response => {
                this.filterDetailHistory (response.data.info)
            }).catch((error) => {
                this.setState({ error, loading: false });
                console.log("Error gelondongan = "+error); //ini yang bener
            });
    }

    filterDetailProgress = (data) => {
        let temp = [];
        data.map(ele=>{
        if (ele.type === "GSIMPOK-P2K"){            
            return(
                temp.push(ele)
            )
        }
        return temp;
        })
        // console.log("ini tmp" + JSON.stringify(temp))
        this.setState({
            progress: temp,
            searchProgress : temp,
            loading: false,
            error : null,
            refreshing: false,
        })
    }

    filterDetailHistory = (data) => {
        let temp = [];
        data.map(ele=>{
        if (ele.type === "GSIMPOK-P2K"){            
            return(
                temp.push(ele)
            )
        }
        return temp;
        })
        // console.log("ini tmp" + JSON.stringify(temp))
        this.setState({
            history : temp,
            searchHistory : temp, 
            loading: false,
            error : null,
            refreshing: false,
        })
    }

    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round  onChangeText={text => this.searchFilterFunction(text)}     value={this.state.value}/>;
    };

    renderSeparator = () => {
        return (
        <View
            style={{
                height: 1,
                width: "86%",
                backgroundColor: "#CED0CE",
                marginLeft: "14%"
            }}
            />
        );
    };

    renderFooter = () => {
        if (!this.state.loading) return null;
    
    renderMessage = () => {
        return <Text>Notification Empty</Text>;
        };
    
        return (
          <View
            style={{
              paddingVertical: 20,
              borderTopWidth: 1,
              borderColor: "#CED0CE"
            }}
          >
            <ActivityIndicator animating size="large" />
          </View>
        );
      };

      searchFilterFunction = (text) => {
        this.setState({
          value: text,
        });
    
        const newDataProgress = this.state.searchProgress.filter(item => {
            const itemDataProgress = `${item.dari.toUpperCase()} ${item.waktu.toUpperCase()}`;
            const textDataProgress = text.toUpperCase();
    
          return itemDataProgress.indexOf(textDataProgress) > -1;
        });
        const newDataHistory = this.state.searchHistory.filter(item => {
                const itemDataProgress = `${item.dari.toUpperCase()} ${item.waktu.toUpperCase()}`;
                const textDataHistory = text.toUpperCase();
      
            return itemDataHistory.indexOf(textDataHistory) > -1;
          });
        this.setState({
          progress : newDataProgress,
          history : newDataHistory
        });
      }

    render (){
        console.log ("ini seachHistory =" + JSON.stringify(this.state.searchHistory))
        // this.state.progress.map ((item) => {
        //     return console.log ("item =" + JSON.stringify(item))
        //   })
        // console.log ("ini search =" + JSON.stringify(this.state.search))
        return (
            <View style = { styles.container }> 
                {this.renderHeader()}
                <ScrollView>
                    <FlatList
                        data={this.state.progress}
                        renderItem={({ item }) => (
                            <ListItem
                            roundAvatar
                            component={TouchableHighlight}
                            title={`${item.dari}`}
                            subtitle={`${item.type}: ${item.waktu}`}
                            avatar={`${item.type}` == "GSIMPOK-P2K" ? avatarP2KProgress : console.log("ini gambar")}                            containerStyle={{ borderBottomWidth: 0 }}
                            onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                            />
                        )}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.renderSeparator}
                        //ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={50}
                    />
                     <FlatList
                        data={this.state.history}
                        renderItem={({ item }) => (
                            <ListItem
                            roundAvatar
                            component={TouchableHighlight}
                            title={`${item.dari}`}
                            subtitle={`${item.type}: ${item.waktu}`}
                            avatar={`${item.type}` == "GSIMPOK-P2K" ? avatarP2KAccept : console.log("ini gambar")}                            containerStyle={{ borderBottomWidth: 0 }}
                            // onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                            />
                        )}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.renderSeparator}
                        //ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={50}
                    />
                </ScrollView>
               
            </View>
        )
    }

}

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        notifState: state.notification,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

const styles = StyleSheet.create ({
    container : {
        width : screen.width,
        height : screen.height * 0.85,
        backgroundColor : '#FFFFFF',
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(NotifDetailSimpok);
import React, { Component } from "react";
import { StyleSheet, Alert, View, Text, FlatList, ActivityIndicator, 
  TouchableOpacity, AsyncStorage, TouchableHighlight, Dimensions, ImageBackground, ScrollView,
  InteractionManager, SafeAreaView } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import IconFA from 'react-native-vector-icons/FontAwesome';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import Modal from 'react-native-modal';
import GeneralStyle from '../../../styles/GeneralStyle';
import { background_image} from '../../../styles/assets';
// import type { Notification, NotificationOpen, RemoteMessage,  firebase } from 'react-native-firebase';
const avatarCheckInProgress = (<IconFA name="pause-circle" size={28} color="orange" />)
const avatarCheckInAccept =  (<IconFA name="arrow-circle-down" size={28} color="green" />)
const avatarCheckInDecline=  (<IconFA name="arrow-circle-down" size={28} color="red" />)
const avatarCheckOutProgress = (<IconFA name="pause-circle" size={28} color="orange" />)
const avatarCheckOutAccept =  (<IconFA name="arrow-circle-up" size={28} color="green" />)
const avatarCheckOutDecline=  (<IconFA name="arrow-circle-up" size={28} color="red" />)
const avatarCutiProgress = (<IconFA name="calendar" size={28} color="orange" />)
const avatarCutiAccept = (<IconFA name="calendar" size={28} color="green" />)
const avatarCutiDecline = (<IconFA name="calendar" size={28} color="red" />)
import VIcon from '../../../styles/VIcon';

var screen = Dimensions.get('window');

export class NotificationMenuProgress extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      progress: [],
      history: [],
      search : [],
      page: 1,
      seed: 1,
      banyakPesanProgress: 0,
      banyakPesanHistory: 0,
      error: null,
      refreshing: false,
      fcm_token: "",
      respon_msg: '',
      notif_status_absen: null,
      player_id: undefined,
      interactionsComplete: false,

      //spinner
      visibleSpinner: false,
      //spinkit
      isVisibleSpinkit: true,
      //modal
      isModalVisible: false,
    };

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent = (event) => {
    // handle a deep link
    if (event.type == 'DeepLink') {
    const parts = event.link.split('/'); // Link parts
    const payload = event.payload; // (optional) The payload
      if (parts[0] == 'tab3') {
        console.log("test deeplink notif");
        // this.makeRemoteRequest();
        this.switchTab();
        console.log("test deeplink notif end");
      }
    }

    //handle right navigator button
    if (event.type == 'NavBarButtonPress') { // this is the event type for button presses
      if (event.id == 'setting') { // this is the same id field from the static navigatorButtons definition
        console.log("setting press");
        this.gotoSetting();
      }
    }
  }

  // gotoSetting = () =>{
  //   this.setState({
  //     respon_msg: 'Under Construction'
  //   });
  //   this.toggleModal();
  // }

  switchTab  = () => {
    this.props.navigator.switchToTab({
      //tabIndex: 2 // (optional) if missing, this screen's tab will become selected
    });
  }

  Interacation() {
    this.setState({
      interactionsComplete: true
    })

  }

  // onSearch = (val) =>{
  //   const data = _.filter(this.state.progress, projectName =>{
  //     return contains(projectName, val);
  //   });
  //   this.setState({search: data});
  // }

  // componentWillMount () {
  //   this.makeRemoteRequestProgress();
  //   this.makeRemoteRequestHistory();
  // }

  componentDidMount() {
    console.log("ini did mount")
    this.Interacation()
    this.makeRemoteRequestProgress();
    this.makeRemoteRequestHistory();
  }

  onOpened = (openResult) => {
    // console.log('Message: ', openResult.notification.payload.body);
    // console.log('Data: ', openResult.notification.payload.additionalData);
    // console.log('isActive: ', openResult.notification.isAppInFocus);
    // console.log('openResult: ', openResult);
    this.makeRemoteRequestProgress();
    this.makeRemoteRequestHistory();
    this.gotoScreen();
  }

  onIds = (device) => {
    console.log('Device info user id: ', device.userId);
    this.setState(
      {
        player_id: device.userId
      },
      () => {
        this.setTokenUser(device.userId);
      }
    );
  }
  
  setTokenUser(token){
    //console.log("set token user = "+token);
    axios.get(this.props.apiState.apiSimpok+'1001'+'/'+this.props.profileState.nik+'/'+
    this.props.profileState.password+'/'+'2'+'|'+token+'/'+this.props.apiState.keyApiSimpok)
    .then(function (response) {
      //console.log(response);
    })
    .catch(function (error) {
      //console.log(error);
    });
  }

  _onPressButton(id) {
    Alert.alert('You tapped the button!');
    //console.log("id nya = "+JSON.stringify(id));
  }

  gotoScreen = () => {
    console.log("test goto screen");
    const {navigator} = this.props;
    var scr, ttl;
    scr = 'panelg.notifsearch';
    ttl = 'Attendance Menu';
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: scr,
        title: ttl,
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
        navigatorStyle: {
          navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
          navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
          navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
          navBarHidden: false, // make the nav bar hidden
          navBarComponentAlignment: 'center',
          navBarTitleTextCentered: true,
        },
        // passProps: {
        //   onUnmount: () => {
        //     console.log('Notif Detail dismissed');
        //     this.makeRemoteRequestProgress();
        //     this.makeRemoteRequestHistory();
        //   }
        // }
      });
    }).catch((error) => {
      console.log("Modul error");
      console.log("" + error);
    });
  }

  // componentWillReceiveProps (newProps) {
  //   if(newProps != null){
  //     console.log ("ini WIll =" + JSON.stringify(this.p));
  //   }
  // }

  bukaNotif = (idPesan, typePesan, jenisPesan, dariPesan, dariNikPesan, idType) =>{
    var own = 0;
    var dariOwn = '';
    console.log("dariNikPesan = "+dariNikPesan);
    console.log("profileState.nik = "+this.props.profileState.nik);
    if(dariNikPesan == this.props.profileState.nik){
      own = 1;
      dariOwn = this.props.profileState.full_name;
    } else{
      own = 0;
      dariOwn = dariPesan;
    }
    var notif_sumber = '0';
    var arr_typePesan = typePesan.split('-');
    this.props.actions.setNotifGeneral(idPesan, arr_typePesan[1], jenisPesan, dariOwn, dariNikPesan, own, notif_sumber, idType);
    const {navigator} = this.props;
    var index = 1;
    var scr, ttl;
    //console.log("type pesan = "+typePesan);
    //var arr_typePesan = typePesan.split('-');
    arr_typePesan[0] = 'ATTENDANCE'
    if(arr_typePesan[1] == 'IN' || arr_typePesan[1] == 'OUT' ||
        arr_typePesan[1] == 'IZIN' || arr_typePesan[1] == 'CUTI'){
        scr = 'panelg.notifscreen.attendance';
        ttl = 'Detail Notification';
    } else{
        scr = 'panelg.attendance.menu';
        ttl = 'Attendance';
    }
    // if(arr_typePesan[0] == 'ATTENDANCE'){ //buat buka detail attendance
    //   if(arr_typePesan[1] == 'IN' || arr_typePesan[1] == 'OUT' ||
    //       arr_typePesan[1] == 'IZIN' || arr_typePesan[1] == 'CUTI'){
    //     scr = 'panelg.notifscreen.attendance';
    //     ttl = 'Detail Notification';
    //   } else{
    //       scr = 'panelg.attendance.menu';
    //       ttl = 'Attendance';
    //   }
    // } else if(arr_typePesan[0] == 'GSIMPOK'){
    //   if(arr_typePesan[1] == 'P2K' || arr_typePesan[1] == 'PK2S'){
    //     scr = 'panelg.notifscreen.simpok';
    //     ttl = 'Detail Notification';
    //   } else{
    //       scr = 'panelg.attendance.menu';
    //       ttl = 'Attendance';
    //   }
    // } else{
    //   this.setState({
    //     respon_msg: 'Under Construction'
    //   });
    //   this.toggleModal();
    //   return;
    // }
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: scr,
        title: ttl,
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
        navigatorStyle: {
          navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
          navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
          navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
          navBarHidden: false, // make the nav bar hidden
          navBarComponentAlignment: 'center',
          navBarTitleTextCentered: true,
        },
        // passProps: {
        //   onUnmount: () => {
        //     console.log('Notif Detail dismissed');
        //     this.makeRemoteRequestProgress();
        //     this.makeRemoteRequestHistory();
        //   }
        // }
      });
    }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
    });
  };

  makeRemoteRequestProgress = () => {
    var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
    axios.get(this.props.apiState.apiSimpok + data)
    .then(response => {
      console.log("NOTIF PROGRESS ATTANDENCE = "+JSON.stringify(response.data));
      this.notifCheckProgress(response.data.info);
      // this.setState({
      //   progress : response.data.info,
      //   search : response.data.info,
      //   error: response.data.info || null,
      //   loading: false,
      //   refreshing: false
      // });
    }).catch((error) => {
      this.setState({ error, loading: false });
      console.log("Error gelondongan = "+error); //ini yang bener
    });
    // this.props.actions.setNotifRemoteHistory(true);
    /*
    const { page, seed } = this.state;
    //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
      */
  };

  notifCheckProgress = (data,countBadge) => {
    let temp = [];
    data.map(ele=>{
      if (ele.type === "ATTENDANCE-OUT" || ele.type === "ATTENDANCE-IN" || ele.type === "ATTENDANCE-CUTI" || ele.type === "ATTENDANCE-IZIN"){
        return(
          temp.push(ele)
        )
      }
      return temp;
    })
    // console.log("ini tmp" + JSON.stringify(temp))
    this.setState({
      progress: temp,
      search : temp,
      loading: false,
      error : null,
      refreshing: false,
      banyakPesanProgress: countBadge,
    })
  }

  notifCheckHistory = (data, countBadge) => {
    let temp = [];
    data.map(ele=>{
      if (ele.type === "ATTENDANCE-OUT" || ele.type === "ATTENDANCE-IN" || ele.type === "ATTENDANCE-CUTI" || ele.type === "ATTENDANCE-IZIN"){
        return(
          temp.push(ele)
        )
      }
      return temp;
    })
    // console.log("ini tmp" + JSON.stringify(temp))
    this.setState({
      history: temp,
      loading: false,
      error : null,
      refreshing: false,
      banyakPesanHistory: countBadge,
    })
  }

  makeRemoteRequestHistory = () => {
    var data = '11002/0/2/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
    console.log("api data " + this.props.apiState.apiSimpok + data)
    axios.get(this.props.apiState.apiSimpok + data)
    .then(response => {
      // console.log("NOTIF HISTORY ATTANDENCE = "+JSON.stringify(response.data));
      this.notifCheckHistory(response.data.info);
      // this.setState({
      //   banyakPesanHistory: countBadge,
      //   // history : response.data.info,
      //   error: response.data.info || null,
      //   loading: false,
      //   refreshing: false
      // });
    }).catch((error) => {
      this.setState({ error, loading: false });
      console.log("Error gelondongan = "+error); //ini yang bener
    });
    this.props.actions.setNotifRemoteHistory(true);
    /*
    const { page, seed } = this.state;
    //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
      */
  };

  setBadge(count){
    this.props.navigator.setTabBadge({
      badge: count
    });
  }

  // makeRemoteRequestHistory = () => {
  //   var data = new FormData();
  //   data.append('keyapi', this.props.apiState.keyApiAbsen);
  //   data.append('dataapi', 21);
  //   data.append('nik', this.props.profileState.nik);
  //   data.append('level', this.props.profileState.level);

  //   const config = {
  //     headers: { 'content-type': 'multipart/form-data' },
  //     timeout: 10000
  //   }
  //   var res_msg;
  //   axios.post(this.props.apiState.apiAbsen, data, config)
  //     .then(response => {
  //       console.log("NOTIF PROGRESS = "+JSON.stringify(response.data));
  //       var countBadge;
  //       if(response.data.rst == 0){
  //         countBadge = 0;
  //       } else{
  //         countBadge = Object.keys(response.data.info).length;
  //       }
  //       this.props.actions.setNotifCount(countBadge);
  //       this.props.navigator.setTabBadge({
  //         badge: countBadge
  //       });
  //       this.setState({
  //         banyakPesan: countBadge,
  //         data: response.data.info,
  //         error: response.data.info || null,
  //         loading: false,
  //         refreshing: false
  //       });
  //       console.log("rootnya = "+this.props.rootState.root);
  //     })
  //     .catch((error) => {
  //       //console.log(error);
  //       this.setState({ error, loading: false });
  //       console.log("================================================");
  //       console.log("Error gelondongan = "+error); //ini yang bener
  //       console.log("================================================");
  //     });
  //   /*
  //   const { page, seed } = this.state;
  //   //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
  //   const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
  //   this.setState({ loading: true });

  //   fetch(url)
  //     .then(res => res.json())
  //     .then(res => {
  //       this.setState({
  //         data: page === 1 ? res.results : [...this.state.data, ...res.results],
  //         error: res.error || null,
  //         loading: false,
  //         refreshing: false
  //       });
  //     })
  //     .catch(error => {
  //       this.setState({ error, loading: false });
  //     });
  //     */
  // };

  // handleRefresh = () => {
  //   this.setState(
  //     {
  //       page: 1,
  //       seed: this.state.seed + 1,
  //       refreshing: true
  //     },
  //     () => {
  //       this.makeRemoteRequestProgress();
  //       this.makeRemoteRequestHistory();
  //     }
  //   );
  // };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequestProgress();
        // this.makeRemoteRequestHistory();
      }
    );
  };

  toggleModal(){
    console.log("toggle modal");
    this.setState(
      {
        isModalVisible: !this.state.isModalVisible
      },
      () => {
        this.makeRemoteRequestProgress();
        // this.makeRemoteRequestHistory();
      }
    );
  }

  searchFilterFunction = (text) => {
    this.setState({
      value: text,
    });

    const newData = this.state.search.filter(item => {
      const itemData = `${item.dari.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      progress : newData,
    });
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar placeholder="Type Here..." lightTheme round containerStyle={{ opacity : 0.5}}  onChangeText={text => this.searchFilterFunction(text)}     value={this.state.value}/>;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

  renderMessage = () => {
    return <Text>Notification Empty</Text>;
  };

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    // if (!this.state.interactionsComplete) {
    //   return <Text>loading...</Text>;
    // }
    //  console.log("ini progress" + JSON.stringify(this.state.progress))
    //  console.log("ini search" + JSON.stringify(this.state.search))
    // console.log("ini history" + JSON.stringify(this.state.history))
    return (
      <SafeAreaView>
      <ImageBackground
        source={background_image}
        style={GeneralStyle.imageBackground}
      >
      {/* {this.renderHeader()} */}
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <Modal isVisible={this.state.isModalVisible}>
            <View style={GeneralStyle.modalParent}>
                <View style={GeneralStyle.modalContentAlert}>
                    <Text style={GeneralStyle.textModalContentAlert}>{this.state.respon_msgHistory}</Text>
                    <TouchableOpacity style={GeneralStyle.modalButtonAlert} onPress={this.toggleModal.bind(this)}>
                        <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        <ScrollView>
          {/* {this.state.banyakPesanProgress == 0 ? <Message /> :  */}
          <FlatList
            data={this.state.progress}
            renderItem={({ item, index }) => (
                <ListItem
                key= {index}
                  roundAvatar
                  component={TouchableHighlight}
                  title={`${item.dari}`}
                  subtitle={`${item.type}: ${item.waktu}`}
                  avatar={`${item.type}` == "ATTENDANCE-IN" ? avatarCheckInProgress : `${item.type}` == "ATTENDANCE-OUT" ? avatarCheckOutProgress : avatarCutiProgress}
                  containerStyle={{ borderBottomWidth: 0 }}
                 
                />
            )}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            //ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={50}
          />
          {/* } */}
          {/* {this.state.banyakPesanHistory == 0 ? <Message /> : 
          <FlatList
            data={this.state.history}
            renderItem={({ item }) => (
                <ListItem
                  roundAvatar
                  component={TouchableHighlight}
                  title={`${item.dari}`}
                  subtitle={`${item.type}: ${item.waktu}`}
                  avatar={`${item.type}` == "ATTENDANCE-IN" ? avatarCheckInAccept : `${item.type}` == "ATTENDANCE-OUT" ?  avatarCheckOutAccept :  avatarCheckInAccept }
                  containerStyle={{ borderBottomWidth: 0 }}
                  // onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                />
            )}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            //ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={50}
          />
          } */}
        </ScrollView>
      </List>
      {/* <View style={{backgroundColor : 'red', width : 60, height : 60, position : 'absolute',borderRadius : 60/2 , marginTop : screen.height * 0.7, marginLeft : screen.width * 0.8, justifyContent : 'center', alignItems : 'center',}}>
          <TouchableOpacity style={GeneralStyle. izinButton}  onPress={this.gotoScreen()}>
              <VIcon type={VIcon.TYPE_MATERIALICONS} name="search" size={28} color='#FFFFFF' />   
          </TouchableOpacity>
      </View> */}
      </ImageBackground>
    </SafeAreaView>
    );
  }
}


function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        notifState: state.notification,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export class Message extends Component {
  render(){
    return (
      <View style={{alignItems: 'center', justifyContent: 'center', }}>
        <Text></Text>
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationMenuProgress);
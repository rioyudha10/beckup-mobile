import React, { Component } from "react";
import { StyleSheet, Alert, View, Text, FlatList, ActivityIndicator, 
  TouchableOpacity, AsyncStorage, TouchableHighlight, Dimensions, ImageBackground, ScrollView,
  InteractionManager, SafeAreaView } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import IconFA from 'react-native-vector-icons/FontAwesome5';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import Modal from 'react-native-modal';
import GeneralStyle from '../../../styles/GeneralStyle';
import { background_image} from '../../../styles/assets';
// import type { Notification, NotificationOpen, RemoteMessage,  firebase } from 'react-native-firebase';
const avatarCutiProgress = (<IconFA name="calendar-times" size={28} color="orange" />)
const avatarCutiAccept = (<IconFA name="calendar-times" size={28} color="green" />)
const avatarCutiDecline = (<IconFA name="calendar-times" size={28} color="red" />)
const avatarIzinProgress = (<IconFA name="file-alt" size={28} color="orange" />)
const avatarIzinAccept = (<IconFA name="file-alt" size={28} color="green" />)
const avatarIzinDecline = (<IconFA name="file-alt" size={28} color="red" />)

var screen = Dimensions.get('window');

export class NotificationMenuProgress extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      progress: [],
      history: [],
      page: 1,
      seed: 1,
      banyakPesanProgress: 0,
      banyakPesanHistory: 0,
      error: null,
      refreshing: false,
      fcm_token: "",
      respon_msg: '',
      notif_status_absen: null,
      player_id: undefined,
      interactionsComplete: false,

      //spinner
      visibleSpinner: false,
      //spinkit
      isVisibleSpinkit: true,
      //modal
      isModalVisible: false,
    };

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent = (event) => {
    // handle a deep link
    if (event.type == 'DeepLink') {
    const parts = event.link.split('/'); // Link parts
    const payload = event.payload; // (optional) The payload
      if (parts[0] == 'tab3') {
        console.log("test deeplink notif");
        // this.makeRemoteRequest();
        this.switchTab();
        console.log("test deeplink notif end");
      }
    }

    //handle right navigator button
    if (event.type == 'NavBarButtonPress') { // this is the event type for button presses
      if (event.id == 'setting') { // this is the same id field from the static navigatorButtons definition
        console.log("setting press");
        this.gotoSetting();
      }
    }
  }

  gotoSetting = () =>{
    this.setState({
      respon_msg: 'Under Construction'
    });
    this.toggleModal();
  }

  switchTab  = () => {
    this.props.navigator.switchToTab({
      //tabIndex: 2 // (optional) if missing, this screen's tab will become selected
    });
  }

  Interacation() {
    this.setState({
      interactionsComplete: true
    })

  }

  componentDidMount() {
    // InteractionManager.runAfterInteractions(() => {
    //   this.setState({interactionsComplete: true});
    // });
    this.Interacation();
    // if(this.props.profileState.nik == undefined){
    //   let nik = await AsyncStorage.getItem('nik');
    //   let password = await AsyncStorage.getItem('password');
    //   console.log("nik = "+nik);
    //   console.log("password = "+password);
    //   let response = await fetch(this.props.apiState.apiSimpok +
    //       '1' + '/' +
    //       nik + '/' +
    //       password + '/' +
    //       '0' + '/' +
    //       this.props.apiState.keyApiSimpok
    //   )

    //   let responseJSON = await response.json();
    //   console.log("responJSON" + responseJSON)
    //   if (response.status >= 200 && response.status < 300) {
    //       if (responseJSON.rst == 1) {
    //           this.props.actions.setProfile(
    //               responseJSON.info.username, responseJSON.info.full_name, responseJSON.info.password,
    //               responseJSON.info.email, responseJSON.info.level, responseJSON.info.divisi,
    //               responseJSON.info.nmdivisi, responseJSON.info.subdiv, responseJSON.info.nmsubdiv,
    //               responseJSON.info.jabatan, responseJSON.info.nmjbtn, responseJSON.info.setatasan,
    //               responseJSON.info.user_type);
    //       }
    //   }
    // }else {
    //   this.setState({interactionsComplete: true});
    //   console.log("loading")
    // }
    
    this.makeRemoteRequestProgress();
    this.makeRemoteRequestHistory();
    //firebase notif
    // const channel = new firebase.notifications.Android.Channel('gratika-gina', 'Channel Gina', firebase.notifications.Android.Importance.Max)
    //                 .setDescription('gratikaa gina channel');
    // firebase.notifications().android.createChannel(channel);
    // firebase.messaging().getToken()
    // .then(fcmToken => {
    //   if (fcmToken) {
    //     console.log("fcmToken = "+fcmToken);
    //     this.setTokenUser(fcmToken);
    //   } else {
    //     // user doesn't have a device token yet
    //     console.log("user doesn't have a token");
    //   } 
    // });
    // this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
    //   // Process your message as required
    //   console.log("MESSAGE LISTENER = "+message);
    //   console.log("MESSAGE LISTENER = "+JSON.stringify(message));
    //   console.log("MESSAGE TITLE = "+message.data.title);
    //   const localNotification = new firebase.notifications.Notification({
    //     sound: 'default',
    //     show_in_foreground: true,
    //   })
    //   .setNotificationId(message.messageId)
    //   .setTitle(message.data.title)
    //   .setSubtitle(message.data.subtitle)
    //   .setBody(message.data.body)
    //   .setData(message.data)
    //   .android.setChannelId('gratika-gina') // e.g. the id you chose above
    //   .android.setSmallIcon('ic_gina') // create this icon in Android Studio
    //   .android.setColor('#ffffff') // you can set a color here
    //   .android.setPriority(firebase.notifications.Android.Priority.High)
    //   .android.setAutoCancel( true )
    //   .android.setVisibility(firebase.notifications.Android.Visibility.Public);
    //   firebase.notifications().displayNotification(localNotification);
    //   this.makeRemoteRequest();
    // });
    // firebase.notifications().onNotificationOpened(n => {
    //   console.log("Akhirnya dibuka: ", n);
    //   this.props.navigator.popToRoot({
    //     animated: true, // does the pop have transition animation or does it happen immediately (optional)
    //     animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
    //   });
    //   this.props.navigator.handleDeepLink({
    //     link: 'tab3/in/any/format',
    //     payload: '' // (optional) Extra payload with deep link
    //   });
    // });
  }

  componentWillUnmount() {
    // this.messageListener();
  }

  onReceived = (notification) => {
    //console.log("notification received "+notification);
    this.makeRemoteRequestProgress();
    this.makeRemoteRequestHistory();
    //this.props.actions.pushNotificationReceived();
    //console.log("keterima");
  }

  onOpened = (openResult) => {
    // console.log('Message: ', openResult.notification.payload.body);
    // console.log('Data: ', openResult.notification.payload.additionalData);
    // console.log('isActive: ', openResult.notification.isAppInFocus);
    // console.log('openResult: ', openResult);
    this.makeRemoteRequestProgress();
    this.makeRemoteRequestHistory();
    this.gotoScreen();
  }

  onIds = (device) => {
    console.log('Device info user id: ', device.userId);
    this.setState(
      {
        player_id: device.userId
      },
      () => {
        this.setTokenUser(device.userId);
      }
    );
  }
  
  setTokenUser(token){
    //console.log("set token user = "+token);
    axios.get(this.props.apiState.apiSimpok+'1001'+'/'+this.props.profileState.nik+'/'+
    this.props.profileState.password+'/'+'2'+'|'+token+'/'+this.props.apiState.keyApiSimpok)
    .then(function (response) {
      //console.log(response);
    })
    .catch(function (error) {
      //console.log(error);
    });
  }

  _onPressButton(id) {
    Alert.alert('You tapped the button!');
    //console.log("id nya = "+JSON.stringify(id));
  }

  gotoScreen = () => {
    console.log("test goto screen");
    const {navigator} = this.props;
    var scr, ttl;
    scr = 'panelg.attendance.menu';
    ttl = 'Attendance Menu';
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: scr,
        title: ttl,
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
        navigatorStyle: {
          navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
          navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
          navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
          navBarHidden: false, // make the nav bar hidden
          navBarComponentAlignment: 'center',
          navBarTitleTextCentered: true,
        },
        passProps: {
          onUnmount: () => {
            console.log('Notif Detail dismissed');
            this.makeRemoteRequestProgress();
            this.makeRemoteRequestHistory();
          }
        }
      });
    }).catch((error) => {
      console.log("Modul error");
      console.log("" + error);
    });
  }

  bukaNotif = (idPesan, typePesan, jenisPesan, dariPesan, dariNikPesan, idType) =>{
    var own = 0;
    var dariOwn = '';
    console.log("dariNikPesan = "+dariNikPesan);
    console.log("profileState.nik = "+this.props.profileState.nik);
    if(dariNikPesan == this.props.profileState.nik){
      own = 1;
      dariOwn = this.props.profileState.full_name;
    } else{
      own = 0;
      dariOwn = dariPesan;
    }
    var notif_sumber = '0';
    var arr_typePesan = typePesan.split('-');
    this.props.actions.setNotifGeneral(idPesan, arr_typePesan[1], jenisPesan, dariOwn, dariNikPesan, own, notif_sumber, idType);
    const {navigator} = this.props;
    var index = 1;
    var scr, ttl;
    //console.log("type pesan = "+typePesan);
    //var arr_typePesan = typePesan.split('-');
    arr_typePesan[0] = 'ATTENDANCE'
    if(arr_typePesan[1] == 'IN' || arr_typePesan[1] == 'OUT' ||
        arr_typePesan[1] == 'IZIN' || arr_typePesan[1] == 'CUTI'){
        scr = 'panelg.notifscreen.attendance';
        ttl = 'Detail Notification';
    } else{
        scr = 'panelg.attendance.menu';
        ttl = 'Attendance';
    }
    // if(arr_typePesan[0] == 'ATTENDANCE'){ //buat buka detail attendance
    //   if(arr_typePesan[1] == 'IN' || arr_typePesan[1] == 'OUT' ||
    //       arr_typePesan[1] == 'IZIN' || arr_typePesan[1] == 'CUTI'){
    //     scr = 'panelg.notifscreen.attendance';
    //     ttl = 'Detail Notification';
    //   } else{
    //       scr = 'panelg.attendance.menu';
    //       ttl = 'Attendance';
    //   }
    // } else if(arr_typePesan[0] == 'GSIMPOK'){
    //   if(arr_typePesan[1] == 'P2K' || arr_typePesan[1] == 'PK2S'){
    //     scr = 'panelg.notifscreen.simpok';
    //     ttl = 'Detail Notification';
    //   } else{
    //       scr = 'panelg.attendance.menu';
    //       ttl = 'Attendance';
    //   }
    // } else{
    //   this.setState({
    //     respon_msg: 'Under Construction'
    //   });
    //   this.toggleModal();
    //   return;
    // }
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: scr,
        title: ttl,
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
        navigatorStyle: {
          navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
          navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
          navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
          navBarHidden: false, // make the nav bar hidden
          navBarComponentAlignment: 'center',
          navBarTitleTextCentered: true,
        },
        passProps: {
          onUnmount: () => {
            console.log('Notif Detail dismissed');
            this.makeRemoteRequestProgress();
            this.makeRemoteRequestHistory();
          }
        }
      });
    }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
    });
  };

  makeRemoteRequestProgress = () => {
    var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
    axios.get(this.props.apiState.apiSimpok + data)
    .then(response => {
      console.log("NOTIF PROGRESS = "+JSON.stringify(response.data));
      var countBadge;
      if(response.data.rst == 0){
        countBadge = 0;
      } else{
        countBadge = Object.keys(response.data.info).length;
      }
      console.log("countBadge before = "+countBadge);
      //this.props.actions.setNotifCount(countBadge);
      countBadge = countBadge + this.props.notifState.notif_count;
      console.log("countBadge after = "+countBadge);
      this.props.navigator.setTabBadge({
        badge: countBadge
      });
      this.notifCheckProgress(response.data.info);
      this.setState({
        banyakPesanProgress: countBadge,
        // progress : response.data.info,
        error: response.data.info || null,
        loading: false,
        refreshing: false
      });
    }).catch((error) => {
      this.setState({ error, loading: false });
      console.log("Error gelondongan = "+error); //ini yang bener
    });
    this.props.actions.setNotifRemoteHistory(true);
    /*
    const { page, seed } = this.state;
    //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
      */
  };

  notifCheckProgress = (data) => {
    let temp = [];
    data.map(ele=>{
      if (ele.type === "ATTENDANCE-CUTI" || ele.type === "ATTENDANCE-IZIN"){
        return(
          temp.push(ele)
        )
      }
      return temp;
    })
    // console.log("ini tmp" + JSON.stringify(temp))
    this.setState({
      progress: temp,
    })
  }

  notifCheckHistory = (data) => {
    let temp = [];
    data.map(ele=>{
      if (ele.type === "ATTENDANCE-CUTI" || ele.type === "ATTENDANCE-IZIN"){
        return(
          temp.push(ele)
        )
      }
      return temp;
    })
    // console.log("ini tmp" + JSON.stringify(temp))
    this.setState({
      history: temp
    })
  }

  makeRemoteRequestHistory = () => {
    var data = '11002/0/2/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
    console.log("api data " + this.props.apiState.apiSimpok + data)
    axios.get(this.props.apiState.apiSimpok + data)
    .then(response => {
      console.log("NOTIF HISTORY = "+JSON.stringify(response.data));
      var countBadge;
      if(response.data.rst == 0){
        countBadge = 0;
      } else{
        countBadge = Object.keys(response.data.info).length;
      }
      console.log("countBadge before = "+countBadge);
      //this.props.actions.setNotifCount(countBadge);
      countBadge = countBadge + this.props.notifState.notif_count;
      console.log("countBadge after = "+countBadge);
      this.props.navigator.setTabBadge({
        badge: countBadge
      });
      this.notifCheckHistory(response.data.info);
      this.setState({
        banyakPesanHistory: countBadge,
        // history : response.data.info,
        error: response.data.info || null,
        loading: false,
        refreshing: false
      });
    }).catch((error) => {
      this.setState({ error, loading: false });
      console.log("Error gelondongan = "+error); //ini yang bener
    });
    this.props.actions.setNotifRemoteHistory(true);
    /*
    const { page, seed } = this.state;
    //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
      */
  };

  setBadge(count){
    this.props.navigator.setTabBadge({
      badge: count
    });
  }

  // makeRemoteRequestHistory = () => {
  //   var data = new FormData();
  //   data.append('keyapi', this.props.apiState.keyApiAbsen);
  //   data.append('dataapi', 21);
  //   data.append('nik', this.props.profileState.nik);
  //   data.append('level', this.props.profileState.level);

  //   const config = {
  //     headers: { 'content-type': 'multipart/form-data' },
  //     timeout: 10000
  //   }
  //   var res_msg;
  //   axios.post(this.props.apiState.apiAbsen, data, config)
  //     .then(response => {
  //       console.log("NOTIF PROGRESS = "+JSON.stringify(response.data));
  //       var countBadge;
  //       if(response.data.rst == 0){
  //         countBadge = 0;
  //       } else{
  //         countBadge = Object.keys(response.data.info).length;
  //       }
  //       this.props.actions.setNotifCount(countBadge);
  //       this.props.navigator.setTabBadge({
  //         badge: countBadge
  //       });
  //       this.setState({
  //         banyakPesan: countBadge,
  //         data: response.data.info,
  //         error: response.data.info || null,
  //         loading: false,
  //         refreshing: false
  //       });
  //       console.log("rootnya = "+this.props.rootState.root);
  //     })
  //     .catch((error) => {
  //       //console.log(error);
  //       this.setState({ error, loading: false });
  //       console.log("================================================");
  //       console.log("Error gelondongan = "+error); //ini yang bener
  //       console.log("================================================");
  //     });
  //   /*
  //   const { page, seed } = this.state;
  //   //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
  //   const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
  //   this.setState({ loading: true });

  //   fetch(url)
  //     .then(res => res.json())
  //     .then(res => {
  //       this.setState({
  //         data: page === 1 ? res.results : [...this.state.data, ...res.results],
  //         error: res.error || null,
  //         loading: false,
  //         refreshing: false
  //       });
  //     })
  //     .catch(error => {
  //       this.setState({ error, loading: false });
  //     });
  //     */
  // };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequestProgress();
        this.makeRemoteRequestHistory();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequestProgress();
        this.makeRemoteRequestHistory();
      }
    );
  };

  toggleModal(){
    console.log("toggle modal");
    this.setState(
      {
        isModalVisible: !this.state.isModalVisible
      },
      () => {
        this.makeRemoteRequestProgress();
        this.makeRemoteRequestHistory();
      }
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar placeholder="Type Here..." lightTheme round />;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

  renderMessage = () => {
    return <Text>Notification Empty</Text>;
  };

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    if (!this.state.interactionsComplete) {
      return <Text>loading...</Text>;
    }
    console.log("ini progress" + JSON.stringify(this.state.progress))
    console.log("ini history" + JSON.stringify(this.state.history))
    return (
      <SafeAreaView>
      <ImageBackground
        source={background_image}
        style={GeneralStyle.imageBackground}
      >
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
        <Modal isVisible={this.state.isModalVisible}>
            <View style={GeneralStyle.modalParent}>
                <View style={GeneralStyle.modalContentAlert}>
                    <Text style={GeneralStyle.textModalContentAlert}>{this.state.respon_msgHistory}</Text>
                    <TouchableOpacity style={GeneralStyle.modalButtonAlert} onPress={this.toggleModal.bind(this)}>
                        <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        <ScrollView keyboardShouldPersistTaps="always" style={GeneralStyle.scrollContainerAttendance}>
          {this.state.banyakPesanProgress == 0 ? <Message /> : 
          <FlatList
            data={this.state.progress}
            renderItem={({ item }) => (
                <ListItem
                  roundAvatar
                  component={TouchableHighlight}
                  title={`${item.dari}`}
                  subtitle={`${item.type}: ${item.waktu}`}
                  avatar={`${item.type}` == "ATTENDANCE-IN" ? avatarCheckInProgress : `${item.type}` == "ATTENDANCE-OUT" ? avatarCheckOutProgress : avatarCutiProgress}
                  containerStyle={{ borderBottomWidth: 0 }}
                  // onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                />
            )}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            //ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={50}
          />
          }
          {this.state.banyakPesanHistory == 0 ? <Message /> : 
          <FlatList
            data={this.state.history}
            renderItem={({ item }) => (
                <ListItem
                  roundAvatar
                  component={TouchableHighlight}
                  title={`${item.dari}`}
                  subtitle={`${item.type}: ${item.waktu}`}
                  avatar={`${item.type}` == "ATTENDANCE-IN" ? avatarCheckInAccept : `${item.type}` == "ATTENDANCE-OUT" ?  avatarCheckOutAccept :  avatarCheckInAccept }
                  containerStyle={{ borderBottomWidth: 0 }}
                  // onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                />
            )}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            //ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={50}
          />
          }
        </ScrollView>
        {/* {this.state.banyakPesanProgress == 0 ? <Message /> : 
        <FlatList
          data={this.state.progress}
          renderItem={({ item }) => (
              <ListItem
                roundAvatar
                component={TouchableHighlight}
                title={`${item.dari}`}
                subtitle={`${item.type}: ${item.waktu}`}
                avatar={avatarCheckInProgress}
                containerStyle={{ borderBottomWidth: 0 }}
                onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
              />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={this.renderSeparator}
          //ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />
        }
        {this.state.banyakPesanHistory == 0 ? <Message /> : 
        <FlatList
          data={this.state.history}
          renderItem={({ item }) => (
              <ListItem
                roundAvatar
                component={TouchableHighlight}
                title={`${item.dari}`}
                subtitle={`${item.type}: ${item.waktu}`}
                avatar={`${item.type}` == "ATTENDANCE-IN" ? avatarCheckInAccept : `${item.type}` == "ATTENDANCE-OUT" ?  avatarCheckOutAccept :  avatarCheckInAccept }
                containerStyle={{ borderBottomWidth: 0 }}
                onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
              />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={this.renderSeparator}
          //ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />
        } */}
      </List>
      </ImageBackground>
      </SafeAreaView>
    );
  }
}


function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        notifState: state.notification,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export class Message extends Component {
  render(){
    return (
      <View style={{alignItems: 'center', justifyContent: 'center', }}>
        <Text></Text>
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationMenuProgress);
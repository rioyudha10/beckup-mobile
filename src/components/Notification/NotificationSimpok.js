import React, { Component } from 'react';
import {StyleSheet, View, Text, Dimensions, Button, TouchableOpacity, ScrollView, 
  AsyncStorage, ImageBackground, Picker, Switch, TextInput, Image} from 'react-native';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import { TextField } from 'react-native-material-textfield';
import IconFA from 'react-native-vector-icons/FontAwesome';
import IconE from 'react-native-vector-icons/Entypo';
import IconMI from 'react-native-vector-icons/MaterialIcons';
import Spinkit from 'react-native-spinkit';
import Spinner from 'react-native-loading-spinner-overlay';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { Fumi } from 'react-native-textinput-effects';
import Modal from 'react-native-modal';
import MapView from 'react-native-maps';
import Lightbox from 'react-native-lightbox';
import LottieView from 'lottie-react-native';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../redux/action/index';
// import Image from 'react-native-image-progress';
import Progress from 'react-native-progress/Pie';
import GeneralStyle from '../../styles/GeneralStyle';
import {anim_gratika_red, background_image} from '../../styles/assets';
import Loading from 'react-native-whc-loading';
import VIcon from '../../styles/VIcon';
import moment from 'moment'

var screen = Dimensions.get('window');



export class NotificationSimpok extends Component {
    constructor(props){
      super(props);
      this.state = {
        id_p2k: '',
        id_carDriver: '',
        id_car: '',
        id_driver: '',
        nik: '',
        error: '',
        place: '',
        lineWidth: 0,
        img_uri: '',
        keterangan: '',
        respon_msg: '',
        own: '',
        status_approval: '',
        uris: this.props.apiState.pathImageAbsen + this.props.notifState.notif_absen_image,
        shortName: '',
        ishitch: false,
        mapRegion: {
          latitude: -36.82339,
          longitude: -73.03569,
          latitudeDelta: 0.0018,
          longitudeDelta: 0.0018,
        },
        peminjam: '',
        nikPeminjam: '',
        tujuan: '',
        alamatTujuan: '',
        activity: '',
        tanggal: '',
        date : '',
        waktu: '',
        project: '',
        keterangan: '',
        kategori: '-1',
        statusPinjam: '',
        statusId: -1,
        isPush: 0,
        carDriver_array: [],
        namaDriver: '',
        nopolDriver: '',
        //modal
        isModalVisible: false,
        isModalLoading: false,
        isPop: 0,
      };
    }
  
    static navigatorStyle = {
      tabBarHidden: true,
      tabBarShowLabels: 'hidden',
    }

      
  
    componentDidMount() {
      this.refs.loading1.show();
      console.log("props status = "+this.props.notifState.notif_absen_status);
      var shortName = '';
      if(this.props.profileState.level == '3'){
        shortName = this.props.profileState.full_name;
      } else{
        shortName = this.props.notifState.notif_dari;
      }
      if(shortName.length > 19){
        shortName = shortName.slice(0, 18) + '...';
      }
      this.setState({
        shortName: shortName,
      });
      this.getDetailNotification();
      this.getListCarDriver();
      console.log("kategori = "+this.state.kategori);
      console.log("statusId = "+this.state.statusId);
    }
  
    // componentWillUnmount() {
    //   this.props.onUnmount(this);
    // }
  
    getDetailNotification(){
      var data = '3001/'+this.props.profileState.nik+'/'+this.props.notifState.notif_id+'/0/'+this.props.apiState.keyApiSimpok;    
      console.log("api sinpok = "+JSON.stringify(this.props.apiState.apiSimpok + data));  
      var config = {
        timeout: 10000
      }
      axios.get(this.props.apiState.apiSimpok + data, config)
      .then(response => {
        console.log("Simpok Info = "+JSON.stringify(response.data.info));
        var shortAlamat = response.data.info.alamat_tujuan.split(',');
        console.log("shortAlamat = "+shortAlamat[0]);
        this.setState({
          id_p2k: response.data.info.id,
          peminjam: response.data.info.nama,
          nikPeminjam: response.data.info.nik,
          tujuan: response.data.info.tujuan,
          alamatTujuan: shortAlamat[0],
          activity: response.data.info.keperluan,
          tanggal: response.data.info.tgl,
          waktu: response.data.info.jam,
          project: response.data.info.proyek,
          keterangan: response.data.info.keterangan.ketPinjam_info,
          kategori: response.data.info.kategori,
          statusId: response.data.info.status.id,
          isPush: response.data.info.ispush,
          statusPinjam: response.data.info.status.info,
          namaDriver: response.data.info.driver,
          nopolDriver: response.data.info.nopol,
          img_uri : response.data.info.imguserpesan,
        });
        console.log("ISPUSH state = "+this.state.isPush);
        console.log("ISPUSH response = "+response.data.info.ispush);
        this.refs.loading1.close();
      }).catch((error) => {
        this.setState({ respon_msg : error });
        console.log("Error gelondongan = "+error);
        this.refs.loading1.close();
        this.toggleModal();
      });
    }

    textFunc(){
      var data = '3001/'+this.props.profileState.nik+'/'+this.props.notifState.notif_id+'/0/'+this.props.apiState.keyApiSimpok;      
      return axios.get(this.props.apiState.apiSimpok + data)
      .then(response => response)
      .catch(err => {
        throw err;
      })
    }
  
    approval(idApprove){
      this.refs.loading1.show();
      var data = '3002/'+this.props.profileState.nik+'/'+this.props.notifState.notif_id+'/'+idApprove+encodeURIComponent("|0|0|0")+'/'+this.props.apiState.keyApiSimpok;
      axios.get(this.props.apiState.apiSimpok + data)
      .then(response => {
        this.refs.loading1.close();
        console.log("DETAIL approval = "+JSON.stringify(response.data));
        this.setState({respon_msg: response.data.info, isPop: 1});
        this.toggleModal();
      }).catch((error) => {
        this.refs.loading1.close();
        this.setState({ respon_msg: error, isPop: 1 });
        console.log("Error gelondongan = "+error); //ini yang bener
        this.toggleModal();
      });
    }

    saveAdmin(){
      if(this.state.id_carDriver == 0){
        this.setState({respon_msg: 'Please Select Driver First', isPop: 0});
        this.toggleModal();
      } else{
        this._toggleLoading();
        var idCarDriver = this.state.id_carDriver.split('-');
        var join = 0;
        if(this.state.ishitch == false){
          join = 1;
        } else{
          join = 2;
        }
        var dataDriver = join+'|'+idCarDriver[0]+'|'+idCarDriver[1];
        var data = '11005/'+this.props.profileState.nik+'/'+this.state.id_p2k+'/'+encodeURIComponent(dataDriver)+'/'+this.props.apiState.keyApiSimpok;
        console.log("saveAdmin = "+this.props.apiState.apiSimpok + data);
        axios.get(this.props.apiState.apiSimpok + data)
        .then(response => {
          this._toggleLoading();
          console.log("DETAIL SaveAdmin = "+JSON.stringify(response.data));
          this.setState({respon_msg: response.data.info, isPop: 1});
          this.toggleModal();
        }).catch((error) => {
          this._toggleLoading();
          this.setState({ respon_msg: error, isPop: 1 });
          console.log("error SaveAdmin = "+error);
          this.toggleModal();
        });
      }
    }

    closeOrder(){
      this._toggleLoading();
      var data = '11007/'+this.props.profileState.nik+'/'+this.props.notifState.notif_id+'/'+'1'+'/'+this.props.apiState.keyApiSimpok;
      axios.get(this.props.apiState.apiSimpok + data)
      .then(response => {
        this._toggleLoading();
        console.log("DETAIL closeOrder = "+JSON.stringify(response.data));
        this.setState({respon_msg: response.data.info, isPop: 1});
        this.toggleModal();
      }).catch((error) => {
        this._toggleLoading();
        this.setState({ respon_msg: error, isPop: 1 });
        console.log("Error closeOrder = "+error); //ini yang bener
        this.toggleModal();
      });
    }

    async getListCarDriver(){
      var data = '11004/0/0/1/'+this.props.apiState.keyApiSimpok;
      axios.get(this.props.apiState.apiSimpok + data)
      .then(response => {
        if(response.status == 200 && response.status < 300){
          if(response.data.rst == 1){ //car available
            this.setState({
              carDriver_array: response.data.info.map(function(item){
                return {
                    key: item.idc+'-'+item.idd,
                    nopol: item.nopol,
                    idd: item.idd,
                    nama: item.nama
                }
              })
            });
          }
        } else{
          this.setState({respon_msg: 'Network Error'})
        }
      }).catch((error) => {
        console.log("error getListCarDriver = "+error);
      });
    }

    submitPush(){
      this._toggleLoading();
      var data = '11006/'+this.props.profileState.nik+'/'+this.props.notifState.notif_id+'/0/'+this.props.apiState.keyApiSimpok;
      axios.get(this.props.apiState.apiSimpok + data)
      .then(response => {
        console.log("DETAIL submitPush = "+JSON.stringify(response.data));
        this._toggleLoading();
        this.setState({respon_msg: response.data.info, isPop: 1});
        this.toggleModal();
      }).catch((error) => {
        this._toggleLoading();
        console.log("error submitPush = "+error);
        this.setState({respon_msg: error, isPop: 1});
        this.toggleModal();
      });
    }

    cancelOrder(){
      this._toggleLoading();
      var data = '11008/'+this.props.profileState.nik+'/'+this.props.notifState.notif_id+'/0/'+this.props.apiState.keyApiSimpok;
      axios.get(this.props.apiState.apiSimpok + data)
      .then(response => {
        console.log("DETAIL cancelOrder = "+JSON.stringify(response.data));
        this._toggleLoading();
        this.setState({respon_msg: response.data.info, isPop: 1});
        this.toggleModal();
      }).catch((error) => {
        this._toggleLoading();
        console.log("error cancelOrder = "+error);
        this.setState({respon_msg: error, isPop: 1});
        this.toggleModal();
      });
    }

    onLottieLoad = () => {
      console.log('play lottie');
      this.animation.play();
    }
  
    closeBack(){
      if(this.state.isPop == 0){
        this.toggleModal();
      } else{
        const {navigator} = this.props;
          this.props.navigator.pop({
          //screen: 'panelg.attendance.menu',
          animated: true, // does the pop have transition animation or does it happen immediately (optional)
          animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
        });
      }
    }
  
    toggleModal(){
      this.setState({ isModalVisible: !this.state.isModalVisible });
      console.log("toggle modal");
    }

    _toggleLoading = () => {
      this.setState({ isModalLoading: !this.state.isModalLoading });
      console.log("ini toggle loading");
    }
  
    testMethod(){
      console.log("test method");
    }

    userHeaderContainer(){
      return(
        <View>
          <View style = {{ height : screen.height*0.15, width : screen.width, flexDirection : 'row' }}>
            <View>
              <Image 
                style= {styles.image}
                source={{uri:this.props.apiState.pathImageUser+this.state.img_uri}}
              />
            </View>
            <View style = {{ height : 60, marginLeft : 10, width : 280, marginTop : 30}}>
              <View style = {{ height : 30, width : 280, justifyContent : 'center', paddingLeft : 10, paddingRight : 10 }}>
                <Text style = {{ color : '#000000', fontSize : 20, fontWeight : 'bold' }}>{this.state.peminjam}</Text>
              </View>
              <View style = {{ height : 30, width : 280, justifyContent : 'center', paddingLeft : 10, paddingRight : 10 }}>
                <Text style = {{ fontSize : 18 }}>{this.state.nikPeminjam}</Text>
              </View>
            </View>
            
              {/* <View style={{width: 100, height: 100, borderRadius: 100/2, backgroundColor : 'red'}}>
              </View> */}
            </View>
            {/* <Text style={{marginTop: screen.height*0.01}}>{this.state.peminjam}</Text>
            <Text>{this.state.nikPeminjam}</Text> */}
          </View>
      );
    }

    managerUserDriver () {
      return(
        <View>
          <View style = {{ height : screen.height*0.15, width : screen.width, flexDirection : 'row' }}>
            <View>
              <Image 
                style= {styles.image}
                source={{uri:this.props.apiState.pathImageUser+this.state.img_uri}}
              />
            </View>
            <View style = {{ height : 60, marginLeft : 10, width : 280, marginTop : 30}}>
              <View style = {{ height : 30, width : 280, justifyContent : 'center', paddingLeft : 10, paddingRight : 10 }}>
                <Text style = {{ color : '#000000', fontSize : 20, fontWeight : 'bold' }}>{this.state.peminjam}</Text>
              </View>
              <View style = {{ height : 30, width : 280, justifyContent : 'center', paddingLeft : 10, paddingRight : 10 }}>
                <Text style = {{ fontSize : 18 }}>{this.state.nikPeminjam}</Text>
              </View>
            </View>
            
              {/* <View style={{width: 100, height: 100, borderRadius: 100/2, backgroundColor : 'red'}}>
              </View> */}
            </View>
            {/* <Text style={{marginTop: screen.height*0.01}}>{this.state.peminjam}</Text>
            <Text>{this.state.nikPeminjam}</Text> */}
          </View>
      )
    }

    userDriverContainer(){
      return(
        <View>
          <View style={{alignItems: 'center', justifyContent: 'center', 
          marginTop: screen.height*0.05, marginLeft: screen.width*0.02, marginRight: screen.width*0.02}}>
            <Text>Your Driver</Text>
            <View style={{borderColor: '#808080', width: screen.width*0.2, borderWidth: 2,
              height: screen.width*0.2, borderRadius: screen.width*0.2*0.5, alignItems: 'center',
              justifyContent: 'center', marginTop: screen.height*0.01}}>
              <IconFA name="user" size={screen.width*0.15} color="#808080" />
            </View>
            <Text style={{marginTop: screen.height*0.01}}>{this.state.namaDriver} - {this.state.nopolDriver}</Text>
          </View>
        </View>
      );
    }

    userFooterContainer(){
      return(
        <View>
          {this.state.statusId == 0 && this.state.isPush == 1 ? 
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
              <TouchableOpacity style={{width: screen.width*0.4, height: screen.height*0.06, backgroundColor: '#E31F26', justifyContent: 'center', alignItems: 'center', borderRadius: 10}} onPress={() => {this.submitPush()}}>
                  <Text style={GeneralStyle.textButtonSubmitAttendance}>Push Admin</Text>
              </TouchableOpacity>
            </View> : this.state.statusId == 6 ? 
            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 30}}>
              <TouchableOpacity style={{width: screen.width*0.4, height: screen.height*0.06, backgroundColor: '#E31F26', justifyContent: 'center', alignItems: 'center', borderRadius: 10}} onPress={() => {this.cancelOrder()}}>
                  <Text style={GeneralStyle.textButtonSubmitAttendance}>Cancel</Text>
              </TouchableOpacity>
            </View> : null
            /*
            this.state.statusId != 0 ? 
            <View>
              <TextField
                label='Hitch With'
                value={'N/A'}
                editable={false}
                //onChangeText={ (phone) => this.setState({ phone }) }
              />
            </View> : null
            */
          }
        </View>
      );
    }

    menuSimpokAlamatContainer(){
      console.log("alamat asal :" + this.state.s_alamat_asal);
      return(
          <View style = {{ marginLeft : 5, marginRight : 5, borderBottomWidth: 1, borderTopWidth : 1 , borderColor : 'grey' }}>
              <View style = {{ height : 30, flexDirection : 'row', alignItems : 'center', paddingLeft : 10}}>
                  <VIcon type={VIcon.TYPE_FONTAWESOME5} name="map-pin" size={15} color='#FF0000'/>
                  <Text style = {{marginLeft : 10, fontSize : 15, fontWeight : 'bold'}}> Pickup</Text>
              </View>
              <View style = {{ height : 30, flexDirection : 'row', alignItems : 'center', paddingLeft : 10}}>
                  <VIcon type={VIcon.TYPE_FONTAWESOME5} name="ellipsis-v" size={15} color='grey'/> 
              </View>
              <View style = {{ height : 30, flexDirection : 'row', alignItems : 'center', paddingLeft : 10}}>
                  <VIcon type={VIcon.TYPE_FONTAWESOME5} name="map-pin" size={15} color='#00FF00'/>
                  <Text style = {{marginLeft : 10, fontSize : 15, fontWeight : 'bold'}}> {this.state.alamatTujuan}  </Text>
              </View>
          </View>
      );
    }

    mainContainer(){
      return(
        <View>
          <View style = {{borderBottomWidth : 1, borderTopWidth : 1, borderColor : 'grey', }}>
            <View style = {{flexDirection : 'row', alignItems : 'center', marginTop : 10}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="map-pin" size={15} color='grey'/>
              <Text style = { styles.text }>  Lokasi Jemput </Text>
            </View>
            <View style={styles.textAreaContainer} >
              <TextInput
                  style={ styles.textinput }
                  underlineColorAndroid="transparent"
                  placeholderTextColor="grey"
                  numberOfLines={10}
                  multiline={true}
                  editable={false}
                  value={ "Undercostruction" }
                  lineWidth={0}
                  multiline={true}
              />
            </View>
            <View style = {{flexDirection : 'row', alignItems : 'center'}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="map-pin" size={15} color='grey'/>
              <Text style = { styles.text } > Lokasi Antar  </Text>
            </View>
            <View style={styles.textAreaContainer} >
              <TextInput
                  style={ styles.textinput }
                  underlineColorAndroid="transparent"
                  placeholderTextColor="grey"
                  numberOfLines={10}
                  multiline={true}
                  editable={false}
                  value={ this.state.alamatTujuan }
                  lineWidth={0}
                  multiline={true}
              />
            </View>
          </View>
          <View style = {{borderBottomWidth : 1, borderColor : 'grey', }}>
            <View style = {{flexDirection : 'row', marginTop : 10, alignItems : 'center'}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="briefcase" size={15} color='grey'/>  
              <Text style = { styles.text }>  Projek </Text>
            </View>
            <View style={styles.textAreaContainer} >
              <TextInput
                  style={ styles.textinput }
                  underlineColorAndroid="transparent"
                  placeholderTextColor="grey"
                  numberOfLines={10}
                  multiline={true}
                  editable={false}
                  value={ this.state.project }
                  lineWidth={0}
                  multiline={true}
              />
            </View>
            <View style = {{flexDirection : 'row', alignItems : 'center'}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="paperclip" size={15} color='grey'/>  
              <Text style = { styles.text }>  Keperluan </Text>
            </View>
            <View style={styles.textAreaContainer} >
              <TextInput
                  style={ styles.textinput }
                  underlineColorAndroid="transparent"
                  placeholderTextColor="grey"
                  numberOfLines={10}
                  multiline={true}
                  editable={false}
                  value={this.state.keterangan}
                  lineWidth={0}
                  multiline={true}
              />
            </View>
          </View>
          <View style = {{borderBottomWidth : 1, borderColor : 'grey', }}>
            <View style = {{flexDirection : 'row', marginTop : 10, alignItems : 'center'}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="calendar" size={15} color='grey'/>  
              <Text style = { styles.text }>  Jadwal </Text>
            </View>
            <View style={styles.textAreaContainer} >
              <TextInput
                  style={ styles.textinput }
                  underlineColorAndroid="transparent"
                  placeholderTextColor="grey"
                  numberOfLines={10}
                  multiline={true}
                  editable={false}
                  value={  moment( this.state.tanggal ).format('ll') + " at " + this.state.waktu }
                  lineWidth={0}
                  multiline={true}
              />
            </View>
            <View style = {{flexDirection : 'row', alignItems : 'center'}}>
              <VIcon type={VIcon.TYPE_FONTAWESOME} name="car" size={15} color='grey'/>  
              <Text style = { styles.text }>  Type</Text>
            </View>
            <View style={styles.textAreaContainer} >
              <TextInput
                  style={ styles.textinput }
                  underlineColorAndroid="transparent"
                  placeholderTextColor="grey"
                  numberOfLines={10}
                  multiline={true}
                  editable={false}
                  value={  this.state.keterangan }
                  lineWidth={0}
                  multiline={true}
              />
            </View>
          </View>
        </View>
        // <View>
        //   <ScrollView>
        //     <Fumi
        //       label={'Tujuan'}
        //       iconClass={FontAwesomeIcon}
        //       iconName={'building'}
        //       iconColor={'#000000'}
        //       iconSize={screen.width*0.06}
        //       value={this.state.tujuan}
        //       editable={false}
        //     />
        //     <Fumi
        //       label={'Alamat Tujuan'}
        //       iconClass={FontAwesomeIcon}
        //       iconName={'map-marker'}
        //       iconColor={'#000000'}
        //       iconSize={screen.width*0.06}
        //       value={this.state.alamatTujuan}
        //       editable={false}
        //     />
        //     <Fumi
        //       label={'Activity'}
        //       iconClass={FontAwesomeIcon}
        //       iconName={'list'}
        //       iconColor={'#000000'}
        //       iconSize={screen.width*0.06}
        //       value={this.state.activity}
        //       editable={false}
        //     />
        //   </ScrollView>
        //   <View style={{flexDirection: 'row'}}>
        //     <ScrollView style={{width: screen.width*0.4, height: screen.height*0.1}}>
        //       <Fumi
        //         label={'Date'}
        //         iconClass={FontAwesomeIcon}
        //         iconName={'calendar'}
        //         iconColor={'#000000'}
        //         iconSize={screen.width*0.06}
        //         value={this.state.tanggal}
        //         editable={false}
        //       />
        //     </ScrollView>
        //     <ScrollView style={{width: screen.width*0.4, height: screen.height*0.1}}>
        //       <Fumi
        //         label={'Time'}
        //         iconClass={FontAwesomeIcon}
        //         iconName={'clock-o'}
        //         iconColor={'#000000'}
        //         iconSize={screen.width*0.06}
        //         value={this.state.waktu}
        //         editable={false}
        //       />
        //     </ScrollView>
        //   </View>
        //   <View style={{flexDirection: 'row'}}>
        //     <ScrollView style={{width: screen.width*0.4, height: screen.height*0.1}}>
        //       <Fumi
        //         label={'Project'}
        //         iconClass={FontAwesomeIcon}
        //         iconName={'line-chart'}
        //         iconColor={'#000000'}
        //         iconSize={screen.width*0.06}
        //         value={this.state.project}
        //         editable={false}
        //       />
        //     </ScrollView>
        //     <ScrollView style={{width: screen.width*0.4, height: screen.height*0.1}}>
        //       <Fumi
        //         label={'Order Type'}
        //         iconClass={FontAwesomeIcon}
        //         iconName={'car'}
        //         iconColor={'#000000'}
        //         iconSize={screen.width*0.06}
        //         value={this.state.keterangan}
        //         editable={false}
        //       />
        //     </ScrollView>
        //   </View>
        // </View>
      );
    }
  
    approvalContainer(){
      return(
        <View>
          {this.state.statusId == 0 ? 
          <View style={{paddingTop:20, justifyContent: 'center', alignItems: 'center', flexDirection: 'row',}}>
          <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.approval(0)}}>
              <Text style={styles.buttonText}>Decline</Text>
            </TouchableOpacity>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.approval(1)}}>
              <Text style={styles.buttonText}>Approve</Text>
            </TouchableOpacity>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.submitPush()}}>
              <Text style={styles.buttonText}>Push Admin</Text>
            </TouchableOpacity>
          </View>
          </View> : 
          <ScrollView style={{width: screen.width*0.9}}>
            <View>
              <Fumi
                label={'Approval'}
                iconClass={FontAwesomeIcon}
                iconName={'info-circle'}
                iconColor={'#000000'}
                iconSize={screen.width*0.06}
                value={this.state.statusPinjam}
                editable={false}
              />
            </View>
          </ScrollView> }
        </View>
      );
    }

    changeHitch(val){
      console.log(val);
      this.setState({ishitch: val});
    }

    adminContainer(){
      return(
        <View>
          {this.state.statusId == 1 ?
            <View>
              <View style={{flexDirection: 'row'}}>
                <Switch
                  onValueChange={(value) => this.changeHitch(value)}
                  //style={{marginBottom: 10}}
                  value={this.state.ishitch} 
                />
                <View style={{flexDirection:'column'}}>
                  <Text style={{fontSize: 10}}>Choose Driver (Slide to let user hitch on with other driver)</Text>
                  <Picker
                    selectedValue={this.state.id_carDriver}
                    style={{flex: 1}}
                    onValueChange={(itemValue, itemIndex) => this.setState({ id_carDriver: itemValue})}>
                    <Picker.Item label="Available Car" value="0" />
                    {this.state.carDriver_array.map((res, i) => {return <Picker.Item value={res.key} label={"" + res.nopol + " - " + res.nama} key={res.key}/> })}
                  </Picker>
                </View>
              </View>
              <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
                <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.saveAdmin()}}>
                  <Text style={styles.buttonText}>Save</Text>
                </TouchableOpacity>
              </View>
            </View> : 
            <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
              <TouchableOpacity style={styles.buttonContainer} onPress={() => {this.saveAdmin()}}>
                <Text style={styles.buttonText}>Close Order</Text>
              </TouchableOpacity>
            </View>
          }
          
        </View>
      );
    }
  
    statusContainer(){
      return(
        <Fumi
          label={'Status'}
          iconClass={FontAwesomeIcon}
          iconName={'info-circle'}
          iconColor={'#f95a25'}
          iconSize={20}
          value={this.state.status_approval == '1' ? 'Approved' : 
                this.state.status_approval == '2' ? 'Decline' : 'Waiting for approval' }
          editable={false}
        />
      );
    }
    
    render() {
      const _userDriverContainer = this.userDriverContainer();
      const _userHeaderContainer = this.userHeaderContainer();
      const _managerContainer = this.managerUserDriver();
      const _mainContainer = this.mainContainer();
      const _userFooterContainer = this.userFooterContainer();
      const _approvalContainer = this.approvalContainer();
      const _adminContainer = this.adminContainer();
      return (
        <ImageBackground
          source={background_image}
          style={GeneralStyle.imageBackground}
        >
        <Loading ref='loading1' image = {require('../image/loading.png')}/>
        <ScrollView keyboardShouldPersistTaps="always">
        <View style={{paddingLeft: 15, paddingRight: 15, paddingBottom : 20}}>
          <Modal isVisible={this.state.isModalLoading}>
            <View style={GeneralStyle.modalLoading}>
                <LottieView
                    onLayout={this.onLottieLoad}
                    ref={animation => {
                        this.animation = animation;
                    }}
                    style={GeneralStyle.lottieLoading}
                    source={anim_gratika_red}
                />
            </View>
          </Modal>
          <Modal isVisible={this.state.isModalVisible}>
              <View style={styles.modalParent}>
                  <View style={styles.modalContent}>
                      <Text>{this.state.respon_msg}</Text>
                      <TouchableOpacity onPress={this.closeBack.bind(this)}>
                          <Text style={{color: 'red', marginTop: 10 }}>DONE</Text>
                      </TouchableOpacity>
                  </View>
              </View>
          </Modal>
          {this.state.kategori == -1 ? null : this.state.kategori == 0 ? _managerContainer : this.state.kategori == 1 ? _userHeaderContainer : _userHeaderContainer}
          {_mainContainer}
          {this.state.kategori == -1 ? null : 
            (this.state.kategori == 0) ? _userFooterContainer : ( this.state.kategori == 1 || this.state.kategori == 2) ? _approvalContainer :
            null}
        </View>
        </ScrollView>
        </ImageBackground>
      );
    }
  }
  
  
  const styles = StyleSheet.create({
    scrolview: {
      padding: 50, 
      justifyContent: 'center', 
      alignItems: 'center',
    },container: {
      width: Dimensions.get('window').width * 0.8,
      height: Dimensions.get('window').height * 0.6,
      //backgroundColor: '#ffffff',
      borderRadius: 5,
      padding: 16,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text : {
      color : 'grey',
      marginLeft : 10
    },
    textinput : {
      color : '#000000',
      fontSize : 15,
      fontWeight : 'bold',
      height: 40, 
      marginRight : 10, 
      marginLeft : 10,
    },
    title: {
      fontSize: 17,
      fontWeight: '700',
    },
    content: {
      marginTop: 8,
    },
    buttonContainer: {
      backgroundColor: '#E31F26',
      //paddingVertical: 10,
      //marginBottom: 100
      width: screen.width*0.3,
      height: screen.height*0.06,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10
    },
    buttonText: {
      textAlign: 'center',
      textAlignVertical: 'center',
      color: '#FFF',
      fontWeight: 'bold',
      fontSize: 18
    },
    imageContainer:{
      justifyContent: 'center', 
      alignItems: 'center',
      position: 'relative',
      flex: 1,
      borderRadius: 100
      //height: 100,
      //width: 100,
      //height: screen.height*0.4
      //top: 0,
      //left: 0,
      //bottom: 0,
      //right: 0,
    },
    image: {
      //height: screen.height,
      //width: screen.width,
      //alignSelf: 'stretch',
      height: 100,
      width : 100,
      borderRadius: 100/2,
      alignItems : 'center',
      justifyContent : 'center',
      marginTop : 10,
      borderWidth : 1,
      borderColor : '#E8E8E8'
      //borderRadius:100,
      //width: 100,
      //borderRadius: 100,
      //width: 100,
      //borderRadius: 100,
      //position: 'absolute',
      //top: 0,
      //left: 0,
      //bottom: 0,
      //right: 0,
    },
    spinner: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    modalParent: {
      overflow: 'hidden',
      //height: screen.height * 0.3,
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalHeader: {
      overflow: 'hidden',
      flex: 0,
      width: screen.width*0.9,
      height: screen.height*0.08,
      borderBottomWidth: 2,
      borderBottomColor: 'grey',
      backgroundColor: 'grey',
      justifyContent: 'center',
      alignItems: 'center',
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
    },
    modalContent: {
      //backgroundColor: 'red',
      height: screen.height * 0.15,
      //padding: 22,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    map: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
    textAreaContainer: {
      marginRight : 10, 
      marginLeft : 10
    },
  });
  
  function mapStateToProps(state, ownProps) {
      return {
          rootState: state.root,
          apiState: state.api,
          notifState: state.notification,
          profileState: state.profile,
          spinkitState: state.spinkit,
      };
  }
  
  function mapDispatchToProps(dispatch) {
      return {
          actions: bindActionCreators(appActions, dispatch)
      };
  }
  
  //AppRegistry.registerComponent('HomeMenu', () => HomeMenu);
  export default connect(mapStateToProps, mapDispatchToProps)(NotificationSimpok);
import React, {Component} from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView, FlatList, TouchableHighlight } from 'react-native'
import { List, ListItem, SearchBar } from "react-native-elements";
import axios from 'axios'
import {connect} from 'react-redux';
import IconFA from 'react-native-vector-icons/FontAwesome';
import { iconsMapFa, iconsLoadedFa } from '../../../config/app-icons-fa';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../../redux/action';
import Loading from 'react-native-whc-loading';



const avatarCheckInProgress = (<IconFA name="pause-circle" size={28} color="orange" />)
const avatarCheckInAccept =  (<IconFA name="arrow-circle-down" size={28} color="green" />)
const avatarCheckInDecline=  (<IconFA name="arrow-circle-down" size={28} color="red" />)
const avatarCheckOutProgress = (<IconFA name="pause-circle" size={28} color="orange" />)
const avatarCheckOutAccept =  (<IconFA name="arrow-circle-up" size={28} color="green" />)
const avatarCheckOutDecline=  (<IconFA name="arrow-circle-up" size={28} color="red" />)
const avatarCutiProgress = (<IconFA name="calendar" size={28} color="orange" />)
const avatarCutiAccept = (<IconFA name="calendar" size={28} color="green" />)
const avatarCutiDecline = (<IconFA name="calendar" size={28} color="red" />)

screen = Dimensions.get('window')

export class NotifDetailAttandance extends Component {

    constructor (props) {
        super(props)
        this.state = {
            progress : [],
            history : [],
            searchProgress : [],
            searchHistory : [],
            allData : [],
        }
    }

    componentDidMount () {
        this.getDetailProgress();
        this.getDetailHistory();
    }

    getDetailProgress () {
        this.refs.loading1.show();
        var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        axios.get ( this.props.apiState.apiSimpok + data )
          .then ( response  => {
              this.filterDetailProgress(response.data.info)
              this.refs.loading1.close();
          }).catch((error) => {
              this.setState({ error, loading: false });
              console.log("Error gelondongan = "+error); //ini yang bener
              this.refs.loading1.close();
          });
    }

    getDetailHistory () {
        var data = '11002/0/2/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        axios.get ( this.props.apiState.apiSimpok + data )
            .then ( response => {
                this.filterDetailHistory (response.data.info)
            }).catch((error) => {
                this.setState({ error, loading: false });
                console.log("Error gelondongan = "+error); //ini yang bener
            });
    }

    filterDetailProgress = (data) => {
        let temp = [];
        data.map(ele=>{
        if (ele.type === "ATTENDANCE-OUT" || ele.type === "ATTENDANCE-IN" || ele.type === "ATTENDANCE-CUTI" || ele.type === "ATTENDANCE-IZIN"){
            return(
            temp.push(ele)
            )
        }
        return temp;
        })
        // console.log("ini tmp" + JSON.stringify(temp))
        this.setState({
            progress: temp,
            searchProgress : temp,
            loading: false,
            error : null,
            refreshing: false,
        })
    }

    filterDetailHistory = (data) => {
        let temp = [];
        data.map(ele=>{
        if (ele.type === "ATTENDANCE-OUT" || ele.type === "ATTENDANCE-IN" || ele.type === "ATTENDANCE-CUTI" || ele.type === "ATTENDANCE-IZIN"){
            return(
            temp.push(ele)
            )
        }
        return temp;
        })
        // console.log("ini tmp" + JSON.stringify(temp))
        this.setState({
            history : temp,
            searchHistory : temp, 
            loading: false,
            error : null,
            refreshing: false,
        })
    }

    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round onChangeText={text => this.searchFilterFunction(text)}     value={this.state.value} />;
    };

    renderSeparator = () => {
        return (
        <View
            style={{
                height: 1,
                width: "86%",
                backgroundColor: "#CED0CE",
                marginLeft: "14%"
            }}
            />
        );
    };

    renderFooter = () => {
        if (!this.state.loading) return null;
    
    renderMessage = () => {
        return <Text>Notification Empty</Text>;
        };
    
        return (
          <View
            style={{
              paddingVertical: 20,
              borderTopWidth: 1,
              borderColor: "#CED0CE"
            }}
          >
            <ActivityIndicator animating size="large" />
          </View>
        );
      };

      searchFilterFunction = (text) => {
        this.setState({
          value: text,
        });
    
        const newDataProgress = this.state.searchProgress.filter(item => {
          const itemDataProgress = `${item.dari.toUpperCase()} ${item.waktu.toUpperCase()}`;
          const textDataProgress = text.toUpperCase();
    
          return itemDataProgress.indexOf(textDataProgress) > -1;
        });
        const newDataHistory = this.state.searchHistory.filter(item => {
            const itemDataHistory = `${item.dari.toUpperCase()} ${item.waktu.toUpperCase()}`;
            const textDataHistory = text.toUpperCase();
      
            return itemDataHistory.indexOf(textDataHistory) > -1;
          });
        this.setState({
          progress : newDataProgress,
          history : newDataHistory
        });
      }

      bukaNotif = (idPesan, typePesan, jenisPesan, dariPesan, dariNikPesan, idType) =>{
        var own = 0;
        var dariOwn = '';
        if(dariNikPesan == this.props.profileState.nik){
          own = 1;
          dariOwn = this.props.profileState.full_name;
        } else{
          own = 0;
          dariOwn = dariPesan;
        }
        var notif_sumber = '0';
        var arr_typePesan = typePesan.split('-');
        this.props.actions.setNotifGeneral(idPesan, arr_typePesan[1], jenisPesan, dariOwn, dariNikPesan, own, notif_sumber, idType);
        const {navigator} = this.props;
        var index = 1;
        var scr, ttl;
        //console.log("type pesan = "+typePesan);
        //var arr_typePesan = typePesan.split('-');
        arr_typePesan[0] = 'ATTENDANCE'
        if(arr_typePesan[1] == 'IN' || arr_typePesan[1] == 'OUT' ||
            arr_typePesan[1] == 'IZIN' || arr_typePesan[1] == 'CUTI'){
            scr = 'panelg.notifscreen.attendance';
            ttl = 'Detail Notification';
        } else{
            scr = 'panelg.attendance.menu';
            ttl = 'Attendance';
        }
        iconsLoadedFa.then(() => {
          navigator.push({
            screen: scr,
            title: ttl,
            navigatorButtons: {
              leftButtons: [
                {
                  icon: iconsMapFa['angle-left'],
                  buttonColor: 'white',
                  id: 'back',
                }
              ]
            },
            navigatorStyle: {
              navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
              navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
              navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
              navBarHidden: false, // make the nav bar hidden
              navBarComponentAlignment: 'center',
              navBarTitleTextCentered: true,
            },
          });
        }).catch((error) => {
            console.log("Modul error");
            console.log("" + error);
        });
      };

    render (){
      // { this.state.progress.map((item) => {
      //   return console.log("item" + JSON.stringify(item.progres))
      // })}
      // console.log ("ini progress =" + JSON.stringify(this.state.progress[0].progress))
      //  this.state.history.map ((item) => {
      //    return console.log ("item =" + JSON.stringify(item))
      //  })
        // console.log ("ini search =" + JSON.stringify(this.state.search))
        return (
            <View style = { styles.container }> 
                <Loading ref='loading1' image = {require('../../image/loading.png')}/>
                {this.renderHeader()}
                <ScrollView>
                    <FlatList
                        data={this.state.progress}
                        renderItem={({ item }) => (
                            <ListItem
                            roundAvatar
                            component={TouchableHighlight}
                            title={`${item.dari}`}
                            subtitle={`${item.type}: ${item.waktu}`}
                            avatar={`${item.type}` == "ATTENDANCE-IN" ? avatarCheckInProgress : `${item.type}` == "ATTENDANCE-OUT" ? avatarCheckOutProgress : avatarCutiProgress}
                            containerStyle={{ borderBottomWidth: 0 }}
                            onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                            />
                        )}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.renderSeparator}
                        //ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={50}
                    />
                     <FlatList
                        data={this.state.history}
                        renderItem={({ item }) => (
                            <ListItem
                            roundAvatar
                            component={TouchableHighlight}
                            title={`${item.dari}`}
                            subtitle={`${item.type}: ${item.waktu}`}
                            avatar={`${item.type}` == "ATTENDANCE-IN" ? avatarCheckInAccept : `${item.type}` == "ATTENDANCE-OUT" ?  avatarCheckOutAccept :  avatarCheckInAccept }
                            containerStyle={{ borderBottomWidth: 0 }}
                            onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                            />
                        )}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.renderSeparator}
                        //ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={50}
                    />
                </ScrollView>
               
            </View>
        )
    }
}

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        notifState: state.notification,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

const styles = StyleSheet.create ({
    container : {
        width : screen.width,
        height : screen.height * 0.85,
        backgroundColor : '#FFFFFF',
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(NotifDetailAttandance);
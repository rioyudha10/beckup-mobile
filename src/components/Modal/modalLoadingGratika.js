import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Image, 
    TouchableOpacity, ActivityIndicator, Animated, Easing} from "react-native";
import {Icon} from "native-base";
import {styleConst} from '../../styles/styleconst';
import GeneralStyle from '../../styles/GeneralStyle';
import LottieView from 'lottie-react-native';
import {anim_gratika_red, background_image, iconMenuSize} from '../../styles/assets';
import { connect } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from "redux";
import * as appActions from '../../redux/action';

const screen = Dimensions.get('window');

const Styles = StyleSheet.create({
    wrapper : {
        width : screen.width,
        height: screen.height,
        borderRadius : 15,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export class lightboxLoading extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
            text: ''
        }
    }

    componentDidMount(){
        if(this.props.utilState.isLoading){
            this.setState({text: 'benar'});
        } else{
            this.setState({text: 'salah'});
        }
    }

    componentDidUpdate(){
        if(!this.props.utilState.isLoading){
            this.props.navigator.dismissLightBox();
        }
    }

    onLottieLoad = () => {
        console.log('play lottie');
        this.animation.play();
    }

    render(){
        return (
            <View style={Styles.wrapper}>
                <View style={GeneralStyle.modalLoading}>
                    <LottieView
                        onLayout={this.onLottieLoad}
                        ref={animation => {
                            this.animation = animation;
                        }}
                        style={GeneralStyle.lottieLoading}
                        source={anim_gratika_red}
                    />
                </View>
            </View>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        utilState: state.util,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(appActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(lightboxLoading);
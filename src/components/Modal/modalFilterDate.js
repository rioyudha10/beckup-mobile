import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Image, 
    TouchableOpacity, Platform, BackHandler, TextInput, Keyboard} from "react-native";
import {Icon, Item, Input, Label, Button} from "native-base";
import {styleConst} from '../../styles/styleconst';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';

const screen = Dimensions.get('window');

const Styles = StyleSheet.create({
    wrapper : {
        width : screen.width * 90/100,
        height: screen.height * 0.35,
        borderRadius : 15,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        height: screen.height*0.07,
        width: screen.width*0.7,
        backgroundColor: 'rgba(255,255,255,0.7)',
        marginBottom: 10,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,
        color: '#000000',
        paddingHorizontal: 10
    },
});

export default class lightboxAlert extends Component{
    constructor(props){
        super(props);
        this.state = {
            isDatePickerVisible: false,
            dateStart: '',
            dateEnd: '',
            status: ''
        }
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        console.log(JSON.stringify(event));
        switch (event.id) {
          case 'willAppear':
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
            break;
          case 'willDisappear':
            this.backHandler.remove();
            break;
          default:
            break;
        }
    }
    
    handleBackPress = () => {
        console.log('ini backpress');
        if(this.props.data().act){
            this.handleAction();
        } else{
            this.handleClose();
        }
        return true;
    }

    handleClose = () =>{
        this.props.navigator.dismissLightBox();
    }

    handleAction = () =>{
        this.props.setFilter(this.state.dateStart+' 00:00:00', this.state.dateEnd+' 23:59:59');
        this.props.navigator.dismissLightBox();
    }

    _handleDatePicked = (date) => {
        this._hideDatePicker();
        Moment.locale('en');
        if(this.state.status == 'start'){
            this.setState({ dateStart: Moment(date).format('YYYY-MM-DD') })
        } else if(this.state.status == 'end'){
            this.setState({ dateEnd: Moment(date).format('YYYY-MM-DD') })
        }
    };

    _hideDatePicker = () => this.setState({ isDatePickerVisible: false });

    _showDatePicker = (param) => {
        this.setState({ isDatePickerVisible: true, status: param })
        Keyboard.dismiss();
    };

    render(){
        // const data = this.props.data();
        return (
            <View style={[{backgroundColor:"white"} , Styles.wrapper]}>
                <View style={{position:"absolute", top:10, right:15, zIndex:1000}}>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <TextInput
                        value={this.state.dateStart}
                        placeholder={'Start Date'}
                        placeholderTextColor="rgba(0,0,0,0.3)"
                        returnKeyType="next"
                        onFocus={() => this._showDatePicker('start')}
                        style={Styles.input}
                        underlineColorAndroid='rgba(0,0,0,0)'
                    />
                    <DateTimePicker
                        isVisible={this.state.isDatePickerVisible}
                        mode={'date'}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDatePicker}
                    />
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <TextInput
                        value={this.state.dateEnd}
                        placeholder={'End Date'}
                        placeholderTextColor="rgba(0,0,0,0.3)"
                        returnKeyType="next"
                        onFocus={() => this._showDatePicker('end')}
                        style={Styles.input}
                        underlineColorAndroid='rgba(0,0,0,0)'
                    />
                </View>
                <Button light style={{justifyContent: 'center', alignItems: 'center', width: screen.width*0.4, height: screen.height*0.07, borderRadius: 5, alignSelf: 'center'}} onPress={this.handleAction} >
                    <Text style={{color: '#000000', fontWeight: 'bold'}}>Search</Text>
                </Button>
            </View>
        )
    }
}
import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, Platform, BackHandler} from "react-native";
import {Icon, Button} from "native-base";
import GeneralStyle from '../../styles/GeneralStyle';
import {styleConst} from '../../styles/styleconst';

const screen = Dimensions.get('window');

const Styles = StyleSheet.create({
    wrapper : {
        width : screen.width * 90/100,
        height: screen.height * 0.15,
        borderRadius : 15,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default class lightboxAlert extends Component{
    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        console.log(JSON.stringify(event));
        switch (event.id) {
          case 'willAppear':
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
            break;
          case 'willDisappear':
            this.backHandler.remove();
            break;
          default:
            break;
        }
    }
    
    handleBackPress = () => {
        console.log('ini backpress');
        if(this.props.data().act){
            this.handleAction();
        } else{
            this.handleClose();
        }
        return true;
    }

    handleClose = () =>{
        this.props.navigator.dismissLightBox();
    }

    handleAction = () =>{
        if(this.props.data().status == 2){
            this.props.actions.appInitialized();
        } else{
            this.props.data().goAct();
            this.props.navigator.dismissLightBox();
        }
    }

    render(){
        const data = this.props.data();
        return (
            <View style={[{backgroundColor:"white"} , Styles.wrapper]}>
                <View style={{position:"absolute", top:10, right:15, zIndex:1000}}>
                </View>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{textAlign: 'center'}}>{data.message}</Text>
                </View>
                <Button light style={{justifyContent: 'center', alignItems: 'center', width: '30%', height: '25%', alignSelf: 'center', marginTop: 10}} onPress={data.act ? this.handleAction : this.handleClose} >
                    <Text style={{color: styleConst.colorTextPrimary , fontWeight: 'bold'}}>{data.confirm}</Text>
                </Button>
            </View>
        )
    }
}
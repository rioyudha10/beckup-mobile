import React, { Component } from 'react';
import { AppRegistry, View, StatusBar, Button, Text, ScrollView, 
    AsyncStorage, TouchableOpacity, Dimensions, ImageBackground, Image } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import { Navigation } from 'react-native-navigation';
import * as axios from 'axios';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as appActions from '../../redux/action';
import Modal from 'react-native-modal';
import GeneralStyle from '../../styles/GeneralStyle';
import {anim_gratika_red, background_image} from '../../styles/assets';
import { Fumi } from 'react-native-textinput-effects';
import IconFA from 'react-native-vector-icons/FontAwesome';
import Progress from 'react-native-progress/Pie';
var screen = Dimensions.get('window');

export class ProfileMenu extends Component {
    static navigatorStyle = {
        //navBarBackgroundColor: 'blue'
      };

    constructor(props){
        super(props);
        this.state = {
            nama: "",
            nik: "",
            departemen: "",
            divisi: "",
            imageProfile: undefined,
            textModalAlert: undefined,

            //change password
            old_password: undefined,
            new_password: undefined,
            new_password2: undefined,
        }

        this.loadProfile();
    }

    // componentWillMount(){
    //     console.log('props = '+ JSON.stringify(this.props.profileState));
    // }

    onClick(){
        navigation = this.props.navigation;
        navigation.navigate('ProfileChangePassword');
        console.log("ini contoh onclick");
    }

    async goLogout(){
        try {
            await AsyncStorage.setItem('isLogin', '0');
            await AsyncStorage.removeItem('nik');
            await AsyncStorage.removeItem('password');
            await AsyncStorage.removeItem('full_name');
            await AsyncStorage.removeItem('nmdivisi');
            await AsyncStorage.removeItem('nmsubdiv');
            await AsyncStorage.removeItem('user_type');
            this.removeTokenUser();
            this.props.actions.appInitialized();
            Navigation.startSingleScreenApp({
                screen: {
                screen: 'panelg.loginscreen', // unique ID registered with Navigation.registerScreen
                //title: 'Welcome', // title of the screen as appears in the nav bar (optional)
                navigatorStyle: {
                    navBarHidden: true, // make the nav bar hidden
                }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                navigatorButtons: {} // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
                },
                passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                animationType: 'slide-down', // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                portraitOnlyMode: true
            });
        } catch (error) {
            console.log("something went wrong");
            console.log("error " + error);
        }
    }

    removeTokenUser(){
        axios.get(this.props.apiState.apiSimpok+'1001'+'/'+this.props.profileState.nik+'/'+
        this.props.profileState.password+'/'+'1'+'|'+'0'+'/'+this.props.apiState.keyApiSimpok)
        .then(function (response) {
            console.log("remove token user respons = "+response);
        })
        .catch(function (error) {
            console.log("remove token user respons = "+error);
        });
    }

    _changePassword = () => {
        //proses ke api change password
        if(this.state.old_password && this.state.new_password && this.state.new_password2){
            if(this.state.new_password == this.state.new_password2){
                this._toggleModal();
                let url = this.props.apiState.apiSimpok+'10' + '/' + this.props.profileState.nik + '/' + 
                    this.hashmd5(this.state.old_password) + '/' + this.hashmd5(this.state.new_password2) + '/' + 
                    this.props.apiState.keyApiSimpok;
                axios.get(url)
                .then((res) => {
                    console.log("changepass result = "+res.data.info);
                    this.setState(() => ({ textModalAlert: res.data.info }))
                    if(res.data.rst == 1){
                        this.storeNewPassword(this.hashmd5(this.state.new_password2));
                        this.setState({
                            old_password: '',
                            new_password: '',
                            new_password2: ''
                        });
                    }
                }).catch((error) => { // catch is called after then
                    console.log("error portal = "+error);
                });
            } else{
                this.setState({
                    textModalAlert: "New Password Didn't Match"
                });
            }
        } else{
            this.setState({
                textModalAlert: "Field can't be empty"
            });
        }
        
        this._toggleModalAlert();
    }

    async storeNewPassword(newpass){
        try{
            await AsyncStorage.setItem('password', newpass);
        } catch(err){
            this.setState({
                textModalAlert: "Error: "+err
            });
            this._toggleModalAlert
        }
    }

    hashmd5(textin) {
        md5 = require('js-md5');
        md5(textin);
        var hash = md5.create();
        hash.update(textin);
        return hash.hex();
    }

    _toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
        console.log("ini toggle modal change password");
        //console.log("location state = "+JSON.stringify(this.props.locationState));
    }

    _toggleModalAlert = () => {
        this.setState({ isModalAlert: !this.state.isModalAlert });
        console.log("ini toggle modal alert");
        //console.log("location state = "+JSON.stringify(this.props.locationState));
    }
    
    async loadProfile(){
        try{
            this.setState({nama: await AsyncStorage.getItem('full_name')});
            this.setState({nik: await AsyncStorage.getItem('nik')});
            this.setState({departemen: await AsyncStorage.getItem('nmdivisi')});
            this.setState({divisi: await AsyncStorage.getItem('nmsubdiv')});
        } catch(error){
            console.log("error load profile = "+error);
        }
    }

    getImageProfile(){
        let url = this.props.apiState.apiSimpok+'9002' + '/' + this.props.profileState.nik + '/' + 
            '1' + '/' + '0' + '/' + this.props.apiState.keyApiSimpok;
        axios.get(url)
        .then((res) => {
            console.log("getimage profile result = "+res.data.info);
            this.setState(() => ({ imageProfile: res.data.info }))
        }).catch((error) => { // catch is called after then
            console.log("error getimage profile = "+error);
        });
    }

  render() {
      //var uris = this.props.apiState.pathImageAbsen + this.props.notifState.notif_absen_image;
    //   console.log("profileState = " + JSON.stringify ( this.props.profileState))
    return (
    <ImageBackground
        source={background_image}
        style={GeneralStyle.imageBackground}
    >
    <ScrollView>
        <StatusBar barStyle='light-content' />
        <Modal isVisible={this.state.isModalAlert}>
            <View style={GeneralStyle.modalParent}>
                <View style={GeneralStyle.modalContentAlert}>
                    <Text style={GeneralStyle.textModalContentAlert}>{this.state.textModalAlert}</Text>
                    <TouchableOpacity style={GeneralStyle.modalButtonAlert} onPress={this._toggleModalAlert}>
                        <Text style={GeneralStyle.textModalButtonAlert}>DONE</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        <Modal isVisible={this.state.isModalVisible}>
            <View style={GeneralStyle.modalParentChangePassword}>
                <View style={GeneralStyle.modalTitleChangePassword}>
                    <Text style={GeneralStyle.textTitleChangePassword}>Change Password</Text>
                </View>
                <View style={GeneralStyle.modalContentChangePassword}>
                    <ScrollView>
                    <Fumi
                        label={'Old Password'}
                        iconClass={IconFA}
                        iconName={'edit'}
                        iconColor={'#f95a25'}
                        iconSize={20}
                        value={this.state.old_password}
                        secureTextEntry={true}
                        onChangeText={(val) => this.setState({ old_password: val })}
                    />
                    <Fumi
                        label={'New Password'}
                        iconClass={IconFA}
                        iconName={'edit'}
                        iconColor={'#f95a25'}
                        iconSize={20}
                        value={this.state.new_password}
                        secureTextEntry={true}
                        onChangeText={(val) => this.setState({ new_password: val })}
                    />
                    <Fumi
                        label={'Confirm Password'}
                        iconClass={IconFA}
                        iconName={'edit'}
                        iconColor={'#f95a25'}
                        iconSize={20}
                        value={this.state.new_password2}
                        secureTextEntry={true}
                        onChangeText={(val) => this.setState({ new_password2: val })}
                    />
                    </ScrollView>
                </View>
                <View style={GeneralStyle.modalButtonContainerChangePassword}>
                    <TouchableOpacity style={GeneralStyle.modalButtonChangePassword}
                                onPress={this._toggleModal}>
                        <Text style={{color: 'white'}}>CANCEL</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={GeneralStyle.modalButtonChangePassword}
                                onPress={this._changePassword}>
                            <Text style={{color: 'white'}}>CHANGE</Text>
                        </TouchableOpacity>
                    </View>
            </View>
        </Modal>
        <Image 
            resizeMode='cover'
            style={{
                height: screen.height*0.25,
                width: screen.height*0.25,
                borderRadius: screen.height*0.25*0.5,
                flex: 1,
                alignSelf: 'center',
                marginTop: 20
            }}
            source={{uri:this.props.apiState.pathImageUser+this.props.profileState.pic}}
        />
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
            {/* <View style={{
                height: 150,
                width: 150,
                borderRadius: 75,
                borderWidth: 1,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: 'red',}}>
                <IconFA name="user" size={screen.height*0.15} color="#4F8EF7" />
            </View> */}
            <Text style={{
                fontSize: 16,
                fontWeight: 'bold'
            }}>{this.props.namaState==undefined ? this.state.nama : this.props.namaState}</Text>
            <Text style={{
                fontSize: 16,
                fontWeight: 'bold'
            }}>{this.props.nikState==undefined ? this.state.nik : this.props.nikState}</Text>
        </View>
        <View style={{
            padding: 30
        }}>
            <TextField
                label='Direktorat'
                value={this.props.departemenState==undefined ? this.state.departemen : this.props.departemenState}
                editable={false}
            />
            <TextField
                label='Sub Direktorat'
                value={this.props.divisiState==undefined ? this.state.divisi : this.props.divisiState}
                editable={false}
            />

            <View style={{justifyContent: 'center', marginTop: 20}} >
                <Button title="Change Password" color="#E31F26" onPress={this._toggleModal}/>
                <View style={{ height: 10}} />
                <Button title="Logout" color="#E31F26" onPress={this.goLogout.bind(this)}/>
            </View>
        </View>
    </ScrollView>
    </ImageBackground>
    );
  }
};

function mapStateToProps(state, ownProps) {
	return {
        apiState: state.api,
        profileState: state.profile,
        namaState: state.profile.full_name,
        nikState: state.profile.nik,
        divisiState: state.profile.nmsubdiv,
        departemenState: state.profile.nmdivisi,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileMenu);
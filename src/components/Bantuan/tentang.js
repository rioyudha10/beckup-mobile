import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { Navigation } from 'react-native-navigation';
const background_image = require('../../images/logo_gina.png');

const screen = Dimensions.get("window")

export default class Tentang extends Component {

    renderTitle = () => {
        return (
                <Text style = {{ color : '#000000', fontSize : 15, fontWeight : 'bold'}}> PT Graha Informatika Nusantara </Text>
        )
    }
   
    render () {
        return (
        <View style = { styles.container }>
            <View style = { styles.iconContainer }>
                <View style = {{ backgroundColor : 'blue', width : 200, height : 200, borderRadius : 20}}>
                <Image 
                    resizeMode='cover'
                    style={{
                        width : 200, 
                        height : 200,
                        alignSelf: 'center',
                        borderWidth : 2,
                        borderColor : '#FFFFFF'
                    }}
                    source= {require('../../images/logo_gina.png')}
                />
                </View>
                <View style = {{ width : 200, height : 30, justifyContent : 'center', alignItems : 'center'}}>
                    <Text style = {{ color : '#000000', fontSize : 15, fontWeight : 'bold'}}> Versi 0.5b </Text>
                </View>
            </View>
            <View style = { styles.textContainer }>
                <Text style = {{ fontSize : 15, color : 'grey', fontWeight : '200'}}>Gina merupakan Aplikasi Internal berbasis Android khusus untuk karyawan { this.renderTitle() } Efisiensi dan kemudahan menjadi kunci utama yang menjadi landasan utama dalam setiap aktifitas karyawan dan memadukan kebutuhan dan kemampuan dalam mobilitas karyawan yang fleksibel. Untuk saat ini fitur utama yang sedang berjalan adalah G-Simpok, Attandance, dan Logbook</Text>
            </View>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        alignItems : 'center',
        height : screen.height,
        width : screen.width,
        backgroundColor : '#FFFFFF'
    },
    iconContainer : {
        height : screen.height*0.4,
        width : screen.width,
        alignItems : 'center',
        justifyContent : 'center'
    },
    textContainer : {
        height : screen.height*0.4,
        width : screen.width*0.8,
        paddingHorizontal : 10
        // alignSelf : 'center',
        // justifyContent : 'center'
    }
})

import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, ScrollView} from 'react-native';

const screen = Dimensions.get("window")

export default class Bantuan extends Component {

      
    render () {
        return (
        <View style = { styles.container }>
            <ScrollView>
                <View style = { styles.title }>
                    <Text style = { styles.textTitle }> G-Simpok </Text>
                </View>
                <View style = { styles.content }>
                    <Text style = { styles.textContent }>G-Simpok memberikan kemudahan dalam mobilitas karyawan, pemesanan dapat dilakukan dimana saja dan kapan saja. Setiap pemesanan dilakukan harus disetujui oleh pihak Manager dan Vice President Technical Support untuk P2K dan disetujui oleh Vice President Technical Support dan Vice President Admin Kendaraan setiap pemesanan akan dipilihkan driver atau kendaraan oleh Admin Kendaraan</Text>
                </View>
                <View style = { styles.title }>
                    <Text style = { styles.textTitle }> Attandence </Text>
                </View>
                <View style = { styles.content }>
                    <Text style = { styles.textContent }>Attandence merupakan metode absensi berbasis check-in, dimana karyawan ketika akan menandai dirinya sudah hadir di kantor penggunaan optimalnya ada jika pada saat karyawaan hadir di tempat pertemuan dan juga jika on-site, karyawan dapat melakukan absensi dengan aplikasi ini dengan memanfaatkan GPS yang telahtertanam pada ponsel pintar. Begitu juga attendance akan meminta karyawan untuk mencatumkan foto dirinya dan perjaan yang sudah / sedang dilakukanya</Text>
                </View>
                <View style = { styles.title }>
                    <Text style = { styles.textTitle }> Loogbook </Text>
                </View>
            </ScrollView>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : '#FFFFFF',
        alignItems : 'center',
    },
    title : {
        backgroundColor : '#FFFFFF',
        width : screen.width*0.95,
        height : screen.height * 0.05,
        justifyContent : 'center',
        borderBottomWidth : 1,
        borderColor : '#808080'
    },
    textTitle : {
        fontSize : 18,
        fontWeight: 'bold',
    },
    textContent : {
        fontSize : 15,
        color : '#808080'
    },
    content : {
        backgroundColor : '#FFFFFF',
        width : screen.width*0.95,
        height : screen.height * 0.25,
        justifyContent : 'center'
    },
})

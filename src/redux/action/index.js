import * as types from './actiontypes';

/*
Action Creators
*/

export function changeAppRoot(root) {
  return {
    type: types.ROOT_CHANGED, 
    root: root
  };
}

export function changeLoading(isLoading){
  return {
    type: types.SET_LOADING,
    isLoading
  }
}

export function changeIdUser(iduser) {
  return {
    type: types.SET_IDUSER, 
    iduser
  };
}

export function changeProfile(nik, full_name, password, email, level, divisi, 
  nmdivisi, subdiv, nmsubdiv, jabatan, nmjbtn, setatasan, user_type){
  return{
    type: types.PROFILE_CHANGED,
    nik: nik,
    full_name: full_name,
    password: password,
    email: email,
    level: level,
    divisi: divisi,
    nmdivisi: nmdivisi,
    subdiv: subdiv,
    nmsubdiv: nmsubdiv,
    jabatan: jabatan,
    nmjbtn: nmjbtn,
    setatasan: setatasan,
    user_type: user_type
  }
}

export function changeImageUser(pic) {
  return {
    type: types.SET_IMAGE_PROFILE, 
    pic
  };
}

export function changeNotifCount(notif_count) {
  return {
    type: types.NOTIFICATION_COUNT, 
    notif_count: notif_count
  };
}

export function changeNotifRemoteHistory(notif_remote_history){
  return {
    type: types.NOTIFICATION_REMOTE_HISTORY,
    notif_remote_history: notif_remote_history
  }
}

export function changeNotifGeneral(notif_id, notif_type, notif_jenis, notif_dari, 
  notif_dari_nik, notif_own, notif_sumber, notif_id_type){
  return{
    type: types.NOTIFICATION_GENERAL_CHANGED, 
    notif_id: notif_id,
    notif_type: notif_type,
    notif_jenis: notif_jenis,
    notif_dari: notif_dari,
    notif_dari_nik: notif_dari_nik,
    notif_own: notif_own,
    notif_sumber: notif_sumber,
    notif_id_type: notif_id_type
  }
}

export function changeNotifDetailAbsen(notif_absen_id, notif_absen_tanggal, notif_absen_jam, notif_absen_image,
notif_absen_caption, notif_absen_latitude, notif_absen_longitude, notif_absen_status){
  return{
    type: types.NOTIFICATION_DETAIL_ABSEN_CHANGED, 
    notif_absen_id: notif_absen_id,
    notif_absen_tanggal: notif_absen_tanggal,
    notif_absen_jam: notif_absen_jam,
    notif_absen_image: notif_absen_image,
    notif_absen_caption: notif_absen_caption,
    notif_absen_latitude: notif_absen_latitude,
    notif_absen_longitude: notif_absen_longitude,
    notif_absen_status: notif_absen_status
  }
}

export function changeNotifDetailAbsenIzin(notif_absen_izin_id, notif_absen_izin_tanggal, notif_absen_izin_jam, 
  notif_absen_izin_jenis, notif_absen_izin_caption, notif_absen_izin_status){
  return{
    type: types.NOTIFICATION_DETAIL_ABSEN_IZIN_CHANGED, 
    notif_absen_izin_id: notif_absen_izin_id,
    notif_absen_izin_tanggal: notif_absen_izin_tanggal,
    notif_absen_izin_jam: notif_absen_izin_jam,
    notif_absen_izin_jenis: notif_absen_izin_jenis,
    notif_absen_izin_caption: notif_absen_izin_caption,
    notif_absen_izin_status: notif_absen_izin_status
  }
}

export function changeNotifDetailAbsenCuti(notif_absen_cuti_id, notif_absen_cuti_tanggal, notif_absen_cuti_tanggal_awal, 
  notif_absen_cuti_tanggal_akhir, notif_absen_cuti_caption, notif_absen_cuti_lokasi, notif_absen_cuti_status){
  return{
    type: types.NOTIFICATION_DETAIL_ABSEN_CUTI_CHANGED, 
    notif_absen_cuti_id: notif_absen_cuti_id,
    notif_absen_cuti_tanggal: notif_absen_cuti_tanggal,
    notif_absen_cuti_tanggal_awal: notif_absen_cuti_tanggal_awal,
    notif_absen_cuti_tanggal_akhir: notif_absen_cuti_tanggal_akhir,
    notif_absen_cuti_caption: notif_absen_cuti_caption,
    notif_absen_cuti_lokasi: notif_absen_cuti_lokasi,
    notif_absen_cuti_status: notif_absen_cuti_status
  }
}

export function changeLocation(lat, long){
  return {
    type: types.LOCATION_CHANGED,
    lat: lat,
    long: long
  }
}

export function changeLocationRegion(region_change){
  return {
    type: types.LOCATION_REGION_CHANGED,
    region_change: region_change
  }
}

export function changeLocationRegionCount(region_change_count){
  return {
    type: types.LOCATION_REGION_CHANGED_COUNT,
    region_change_count: region_change_count
  }
}

export function getDataSpinkit(SpinkitType, SpinkitSize, SpinkitColor){
  return{
    type: types.DATA_SPINKIT,
    SpinkitType: SpinkitType,
    SpinkitSize: SpinkitSize,
    SpinkitColor: SpinkitColor
  }
}

export function changeSimpokType(simpok_type){
  return{
    type: types.SIMPOK_TYPE_CHANGED,
    simpok_type: simpok_type
  }
}

export function changeSimpokDestination(simpok_lat, simpok_long){
  return{
    type: types.SIMPOK_DESTINATION_CHANGED,
    simpok_lat: simpok_lat,
    simpok_long: simpok_long
  }
}

/*
dispatch the actionCreators 
*/

export function appInitialized() {
  return async function(dispatch, getState) {
    // since all business logic should be inside redux actions
    // this is a good place to put your app initialization code
    dispatch(changeAppRoot('login'));
  };
}

export function login() {
  return async function(dispatch, getState) {
    // login logic would go here, and when it's done, we switch app roots
    dispatch(changeAppRoot('after-login'));
  };
}

export function setLoading(isLoading){
  return async function(dispatch, getState) {
    dispatch(changeLoading(isLoading));
  };
}

export function setIdUser(iduser) {
  return async function(dispatch, getState) {
    // login logic would go here, and when it's done, we switch app roots
    dispatch(changeIdUser(iduser));
  };
}

export function setProfile(nik, full_name, password, email, level, divisi, 
  nmdivisi, subdiv, nmsubdiv, jabatan, nmjbtn, setatasan, user_type){
  return async function(dispatch, getState){
    dispatch(changeProfile(nik, full_name, password, email, level, divisi, 
      nmdivisi, subdiv, nmsubdiv, jabatan, nmjbtn, setatasan, user_type));
  }
}

export function setImageUser(pic) {
  return async function(dispatch, getState) {
    // login logic would go here, and when it's done, we switch app roots
    dispatch(changeImageUser(pic));
  };
}

export function setNotifCount(count) {
  return async function(dispatch, getState) {
    // login logic would go here, and when it's done, we switch app roots
    dispatch(changeNotifCount(count));
  };
}

export function setNotifRemoteHistory(remoteHistory){
  return async function(dispatch, getState){
    dispatch(changeNotifRemoteHistory(remoteHistory));
  };
}

export function setNotifGeneral(notif_id, notif_type, notif_jenis, notif_dari, 
  notif_dari_nik, notif_own, notif_sumber, notif_id_type){
  return async function(dispatch, getState){
    dispatch(changeNotifGeneral(notif_id, notif_type, notif_jenis, notif_dari, 
      notif_dari_nik, notif_own, notif_sumber, notif_id_type));
  }
}

export function setNotifDetailAbsen(notif_absen_id, notif_absen_tanggal, notif_absen_jam, notif_absen_image,
  notif_absen_caption, notif_absen_latitude, notif_absen_longitude, notif_absen_status){
  return async function(dispatch, getState){
    dispatch(changeNotifDetailAbsen(notif_absen_id, notif_absen_tanggal, notif_absen_jam, notif_absen_image,
      notif_absen_caption, notif_absen_latitude, notif_absen_longitude, notif_absen_status));
  }
}

export function setNotifDetailAbsenIzin(notif_absen_izin_id, notif_absen_izin_tanggal, notif_absen_izin_jam, 
  notif_absen_izin_jenis, notif_absen_izin_caption, notif_absen_izin_status){
  return async function(dispatch, getState){
    dispatch(changeNotifDetailAbsenIzin(notif_absen_izin_id, notif_absen_izin_tanggal, notif_absen_izin_jam, 
      notif_absen_izin_jenis, notif_absen_izin_caption, notif_absen_izin_status));
  }
}

export function setNotifDetailAbsenCuti(notif_absen_cuti_id, notif_absen_cuti_tanggal, notif_absen_cuti_tanggal_awal, 
  notif_absen_cuti_tanggal_akhir, notif_absen_cuti_caption, notif_absen_cuti_lokasi, notif_absen_cuti_status){
  return async function(dispatch, getState){
    dispatch(changeNotifDetailAbsenCuti(notif_absen_cuti_id, notif_absen_cuti_tanggal, notif_absen_cuti_tanggal_awal, 
      notif_absen_cuti_tanggal_akhir, notif_absen_cuti_caption, notif_absen_cuti_lokasi, notif_absen_cuti_status));
  }
}

export function setLocation(lat, long){
  return async function(dispatch, getState){
    dispatch(changeLocation(lat, long));
  }
}

export function setLocationRegion(region_change){
  return async function(dispatch, getState){
    dispatch(changeLocationRegion(region_change));
  }
}

export function setLocationRegionCount(region_change_count){
  return async function(dispatch, getState){
    dispatch(changeLocationRegionCount(region_change_count));
  }
}

export function setSimpokType(simpok_type){
  return async function(dispatch, getState){
    dispatch(changeSimpokType(simpok_type));
  }
}

export function setSimpokDestination(simpok_lat, simpok_long){
  return async function(dispatch, getState){
    dispatch(changeSimpokDestination(simpok_lat, simpok_long));
  }
}
import * as types from '../action/actiontypes';

const reducer = {
  simpok_type: undefined,
  simpok_lat: undefined,
  simpok_long: undefined
};

//location reducer
export function simpok(state = reducer, action = {}) {

  switch (action.type) {

    case types.SIMPOK_TYPE_CHANGED:
      return {
        ...state,
        simpok_type: action.simpok_type
      };

    case types.SIMPOK_DESTINATION_CHANGED:
      return {
        ...state,
        simpok_lat: action.simpok_lat,
        simpok_long: action.simpok_long
      };

    default:
      return state;
  }
}
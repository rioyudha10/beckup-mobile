import * as types from '../action/actiontypes';

const reducer = {
  isLoading: true,
};

export function util(state = reducer, action = {}) {

  switch (action.type) {
    
    case types.SET_LOADING:
      return {
        ...state, 
        isLoading: action.isLoading
      }

    default:
      return state;
  }
}
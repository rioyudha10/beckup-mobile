import * as types from '../action/actiontypes';

const reducer = {
  root: undefined, // 'login' / 'after-login'
};

//root reducer
export function root(state = reducer, action = {}) {

  switch (action.type) {
    
    case types.ROOT_CHANGED:
      return {
        ...state,
        root: action.root
      };

    default:
      return state;
  }
}
import {root} from './rootReducer';
import {api} from './apiReducer';
import {notification} from './notificationReducer';
import {profile} from './profileReducer';
import {spinkit} from './spinkitReducer';
import {location} from './locationReducer';
import {simpok} from './simpokReducer';
import {util} from './utilReducer';

/*
This file exports the reducers as an object which 
will be passed onto combineReducers method at src/app.js
*/
export default {
    root,
    api,
    notification,
    profile,
    spinkit,
    location,
    simpok,
    util
}
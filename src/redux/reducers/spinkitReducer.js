import * as types from '../action/actiontypes';

const reducer = {
  SpinkitType: 'ThreeBounce',
  SpinkitSize: 100,
  SpinkitColor: '#E31F26',
};

//root reducer
export function spinkit(state = reducer, action = {}) {

  switch (action.type) {
    
    case types.DATA_SPINKIT:
      return {
        ...state,
        SpinkitType: action.SpinkitType,
        SpinkitSize: action.SpinkitSize,
        SpinkitColor: action.SpinkitColor
      };

    default:
      return state;
  }
}
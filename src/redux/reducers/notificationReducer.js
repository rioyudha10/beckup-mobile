import * as types from '../action/actiontypes';

const reducer = {
  notif_count: 0,
  notif_remote_history: false,
  //tipe data umum untuk pesan
  notif_id: undefined,
  notif_type: undefined,
  notif_jenis: undefined,
  notif_dari: undefined,
  notif_dari_nik: undefined,
  notif_own: undefined,
  notif_sumber: undefined,
  notif_id_type: undefined,
  //tipe data pesan absen in/out
  notif_absen_id: undefined,
  notif_absen_tanggal: undefined,
  notif_absen_jam: undefined,
  notif_absen_image: undefined,
  notif_absen_caption: undefined,
  notif_absen_latitude: undefined,
  notif_absen_longitude: undefined,
  notif_absen_status: undefined,
  //tipe data pesan absen izin
  notif_absen_izin_id: undefined,
  notif_absen_izin_tanggal: undefined,
  notif_absen_izin_jam: undefined,
  notif_absen_izin_jenis: undefined,
  notif_absen_izin_caption: undefined,
  notif_absen_izin_status: undefined,
  //tipe data pesan absen cuti
  notif_absen_cuti_id: undefined,
  notif_absen_cuti_tanggal: undefined,
  notif_absen_cuti_tanggal_awal: undefined,
  notif_absen_cuti_tanggal_akhir: undefined,
  notif_absen_cuti_caption: undefined,
  notif_absen_cuti_lokasi: undefined,
  notif_absen_cuti_status: undefined,
};

//notification reducer
export function notification(state = reducer, action = {}) {

  switch (action.type) {

    case types.NOTIFICATION_COUNT:
      return {
        ...state,
        notif_count: action.notif_count,
      };
    
    case types.NOTIFICATION_REMOTE_HISTORY:
      return {
        ...state,
        notif_remote_history: action.notif_remote_history
      };

    case types.NOTIFICATION_GENERAL_CHANGED:
      return {
        ...state,
        notif_id: action.notif_id,
        notif_type: action.notif_type,
        notif_jenis: action.notif_jenis,
        notif_dari: action.notif_dari,
        notif_dari_nik: action.notif_dari_nik,
        notif_own: action.notif_own,
        notif_sumber: action.notif_sumber,
        notif_id_type: action.notif_id_type
      };

    case types.NOTIFICATION_DETAIL_ABSEN_CHANGED:
      return {
        ...state,
        notif_absen_id: action.notif_absen_id,
        notif_absen_tanggal: action.notif_absen_tanggal,
        notif_absen_jam: action.notif_absen_jam,
        notif_absen_image: action.notif_absen_image,
        notif_absen_caption: action.notif_absen_caption,
        notif_absen_latitude: action.notif_absen_latitude,
        notif_absen_longitude: action.notif_absen_longitude,
        notif_absen_status: action.notif_absen_status
      };
    
    case types.NOTIFICATION_DETAIL_ABSEN_IZIN_CHANGED:
      return {
        ...state,
        notif_absen_izin_id: action.notif_absen_izin_id,
        notif_absen_izin_tanggal: action.notif_absen_izin_tanggal,
        notif_absen_izin_jam: action.notif_absen_izin_jam,
        notif_absen_izin_jenis: action.notif_absen_izin_jenis,
        notif_absen_izin_caption: action.notif_absen_izin_caption,
        notif_absen_izin_status: action.notif_absen_izin_status,
      };
    
    case types.NOTIFICATION_DETAIL_ABSEN_CUTI_CHANGED:
      return {
        ...state,
        notif_absen_cuti_id: action.notif_absen_cuti_id,
        notif_absen_cuti_tanggal: action.notif_absen_cuti_tanggal,
        notif_absen_cuti_tanggal_awal: action.notif_absen_cuti_tanggal_awal,
        notif_absen_cuti_tanggal_akhir: action.notif_absen_cuti_tanggal_akhir,
        notif_absen_cuti_caption: action.notif_absen_cuti_caption,
        notif_absen_cuti_lokasi: action.notif_absen_cuti_lokasi,
        notif_absen_cuti_status: action.notif_absen_cuti_status,
      };

    default:
      return state;
  }
}
import * as types from '../action/actiontypes';

const reducer = {
  apiSimpok: 'http://www.gratika.co.id/apigrtk/index.php/web_service/api/',
  //apiSimpok: 'http://10.0.1.90/api_grtk/index.php/web_service/api/',
  apiAbsen: 'http://www.gratika.co.id/apiabsen/index.php/web_service/api/',
  // apiAbsen: 'http://10.0.1.90/api_absen/index.php/web_service/api/',
  apiLogbook: 'http://aplikasi2.gratika.id/logbook-api/',
  apiPortal: 'http://www.gratika.co.id/apigrtk/index.php/web_service/api/90001/K140056/a7bc1a260fadb0bc3801c6b2ef67800d/0/350ea7e2af60c9d3824791dd122272d8',
  apiReverseLocation: 'https://maps.googleapis.com/maps/api/geocode/json?address=',
  apiDistanceMatrix: 'https://maps.googleapis.com/maps/api/distancematrix/json?&',
  pathImageAbsen: 'http://gratika.co.id/apiabsen',
  //pathImageAbsen: 'http://10.0.1.90/api_absen',
  pathImageUser: 'http://aplikasi2.gratika.id/gtkroom/user-images/user/',
  keyApiSimpok: '350ea7e2af60c9d3824791dd122272d8',
  keyApiAbsen: '350ea7e2af60c9d3824791dd122272d8',
  keyApiLogbook: 'Basic YWRtaW46YWRtaW4=',
  keyGoogleApi: 'AIzaSyALHgBNeF82-k70czXmcMCH9coHyDPBRC4',
  brandPhone: ['OPPO','vivo'],
  //keyGoogleApi: 'AIzaSyACoDVg7cCA5AnCJ0LcLtqAIYHsl0rnJb8',
  //keyReverseLocation: 'AIzaSyACoDVg7cCA5AnCJ0LcLtqAIYHsl0rnJb8',
  //keyReverseLocation: ' AIzaSyA3dZ9IpmuK7rIWaKqw0JhDTziZDHBROiA ',
  //keyPlacesAutoComplete: 'AIzaSyACoDVg7cCA5AnCJ0LcLtqAIYHsl0rnJb8',
  //keyPlacesAutoComplete: ' AIzaSyD0fwL-yEOqbB1whWyWsLy9r-rCIxKqAL0 ',
};

export function api(state = reducer, action = {}) {

  switch (action.type) {
    
    case types.DATA_API:
      return {
        ...state,
        apiSimpok: action.apiSimpok,
        apiAbsen: action.apiAbsen,
      };

    default:
      return state;
  }
}
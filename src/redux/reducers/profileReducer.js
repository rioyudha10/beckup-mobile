import * as types from '../action/actiontypes';

const reducer = {
  iduser: undefined,
  nik: undefined,
  full_name: undefined,
  password: undefined,
  email: undefined,
  level: undefined,
  divisi: undefined,
  nmdivisi: undefined,
  subdiv: undefined,
  nmsubdiv: undefined,
  jabatan: undefined,
  nmjbtn: undefined,
  setatasan: undefined,
  user_type: undefined,
  pic: undefined
};

//profile reducer
export function profile(state = reducer, action = {}) {

  switch (action.type) {

    case types.SET_IDUSER:
      return {
        ...state,
        iduser: action.iduser
      };
    
    case types.PROFILE_CHANGED:
      return {
        ...state,
        nik: action.nik,
        full_name: action.full_name,
        password: action.password,
        email: action.email,
        level: action.level,
        divisi: action.divisi,
        nmdivisi: action.nmdivisi,
        subdiv: action.subdiv,
        nmsubdiv: action.nmsubdiv,
        jabatan: action.jabatan,
        nmjbtn: action.nmjbtn,
        setatasan: action.setatasan,
        user_type: action.user_type
      };

    case types.SET_IMAGE_PROFILE:
      return {
        ...state,
        pic: action.pic
      };

    default:
      return state;
  }
}
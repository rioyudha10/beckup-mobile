import * as types from '../action/actiontypes';

const reducer = {
  lat_gratika: '-6.200185355163949',
  long_gratika: '106.7994654737413',
  lat: undefined,
  long: undefined,
  region_change: false,
  region_change_count: 0
};

export function location(state = reducer, action = {}) {

  switch (action.type) {
    
    case types.LOCATION_CHANGED:
      return {
        ...state,
        lat: action.lat,
        long: action.long
      };
    
    case types.LOCATION_REGION_CHANGED:
      return {
        ...state,
        region_change: action.region_change,
      };

    case types.LOCATION_REGION_CHANGED_COUNT:
      return {
        ...state,
        region_change_count: action.region_change_count,
      };

    default:
      return state;
  }
}
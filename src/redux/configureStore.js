import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';
import rootReducer from './reducers/index';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';

let middleware = [thunk];

const persistConfig = {
	key: 'root',
	storage,
	blacklist: ['spinkit']
}
const combinedReducer = persistCombineReducers(persistConfig, rootReducer);
export default function configureStore(initialState) {
	let store = createStore(
		combinedReducer,
		initialState,
		applyMiddleware(...middleware)
	);
	let persistor = persistStore(store)

  	return { persistor, store };
}

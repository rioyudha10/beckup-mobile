import firebase from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';

export default async (message: RemoteMessage) => {
    console.log("RemoteMessage", message);
    const localNotification = new firebase.notifications.Notification({
        sound: 'default',
        show_in_foreground: true,
      })
      .setNotificationId(message.messageId)
      .setTitle(message.data.title)
      .setSubtitle(message.data.subtitle)
      .setBody(message.data.body)
      .setData(message.data)
      .android.setChannelId('gratika-gina') // e.g. the id you chose above
      .android.setSmallIcon('ic_gina') // create this icon in Android Studio
      .android.setColor('#ffffff') // you can set a color here
      .android.setPriority(firebase.notifications.Android.Priority.High)
      .android.setAutoCancel( true )
      .android.setVisibility(firebase.notifications.Android.Visibility.Public);
      firebase.notifications().displayNotification(localNotification);
      firebase.notifications().onNotificationOpened(n => {
        console.log("Akhirnya dibuka: ", n);
        this.props.navigator.handleDeepLink({
          link: 'tab3/in/any/format',
          payload: '' // (optional) Extra payload with deep link
        });
      });
    return Promise.resolve();
}
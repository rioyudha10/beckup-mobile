// Import an iconset
import Icon from 'react-native-vector-icons/FontAwesome';

// define your suffixes by yourself..
// here we use active, big, small, very-big..
const replaceSuffixPattern = /--(active|big|small|very-big)/g;
const icons = {
    'heart': [30, '#000000'],
    'heart--active': [30, 'red'],
    'home': [30, 'black'],
    'home--active': [30, 'red'],
    'bars': [20, '#FFFFFF'],
    'bars--active': [20, '#FFFFFF'],
    'home': [30, 'black'],
    'user': [20, '#FFFFFF'],
    'user--active': [20, '#FFFFFF'],
    'search': [23, '#000000'],
    'search--active': [20, '#000000'],
    'gear': [23, '#000000'],
    'gear--active': [30, '#000000'],
    'check': [20, '#000000'],
    'check--active': [20, '#000000'],
    'bookmark': [30, '#000000'],
    'bookmark--active': [30, '#000000'],
    'camera': [30, '#000000'],
    'photo': [30, '#000000'],
    'pencil': [30, '#000000'],
    'stop': [30, '#000000'],
    'pause': [30, '#000000'],
    'forward': [30, '#000000'],
    'play': [30, '#000000'],
    'share': [30, '#000000'],
    'eye': [30, '#000000'],
    'eye-slash': [30, '#000000'],
    'bookmark-o': [30, '#000000'],
    'thumbs-o-up': [30, '#000000'],
    'thumbs-o-down': [30, '#000000'],
    'facebook': [30, '#000000'],
    'twitter': [30, '#000000'],
    'comment-o': [30, '#000000'],
    'zoom-in': [30, '#000000'],
    'zoom-out': [30, '#000000'],
    'text-color': [30, '#000000'],
    'toggle-off': [30, '#000000'],
    'toggle-on': [30, '#000000'],
    'leanpub': [30, '#000000'],
    'user-plus': [23, '#000000'],
    'more-vert': [30, '#000000'],
    'location-arrow': [30, '#000000'],
    'ellipsis-v': [23, '#000000'],
    'send': [23, '#000000'],
    'history': [23, '#000000'],
    'bookmark-o': [23, '#000000'],
    'bell-o': [23, '#FFFFFF'],
    'bell': [20, '#FFFFFF'],
    'angle-left': [30, '#FFFFFF'],
    'calendar-times': [30, '#FFFFFF'],
};

const defaultIconProvider = Icon;

let iconsMapFa = {};
let iconsLoadedFa = new Promise((resolve, reject) => {
    new Promise.all(
        Object.keys(icons).map(iconName => {
            const Provider = icons[iconName][2] || defaultIconProvider; // Ionicons
            return Provider.getImageSource(iconName.replace(replaceSuffixPattern, ''), icons[iconName][0], icons[iconName][1]);
        })
    ).then(sources => {
        Object.keys(icons).forEach((iconName, idx) => (iconsMapFa[iconName] = sources[idx]));

        // Call resolve (and we are done)
        resolve(true);
    });
});

export { iconsMapFa, iconsLoadedFa };

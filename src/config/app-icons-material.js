// Import an iconset
import Icon from 'react-native-vector-icons/MaterialIcons';

// define your suffixes by yourself..
// here we use active, big, small, very-big..
const replaceSuffixPattern = /--(active|big|small|very-big)/g;
const icons = {
    'flash-on': [30, '#FFFFFF'],
    'flash-off': [30, '#FFFFFF'],
    'flash-auto': [30, '#FFFFFF'],
};

const defaultIconProvider = Icon;

let iconsMapMaterial = {};
let iconsLoadedMaterial = new Promise((resolve, reject) => {
    new Promise.all(
        Object.keys(icons).map(iconName => {
            const Provider = icons[iconName][2] || defaultIconProvider; // Ionicons
            return Provider.getImageSource(iconName.replace(replaceSuffixPattern, ''), icons[iconName][0], icons[iconName][1]);
        })
    ).then(sources => {
        Object.keys(icons).forEach((iconName, idx) => (iconsMapMaterial[iconName] = sources[idx]));

        // Call resolve (and we are done)
        resolve(true);
    });
});

export { iconsMapMaterial, iconsLoadedMaterial };

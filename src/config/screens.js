import { Navigation } from 'react-native-navigation';

import SampleScreen from './sample';
import LoginScreen from '../components/Login/Login';
import HomeMenuScreen from '../components/Home/HomeMenu';
import ProfileMenuScreen from '../components/Profile/ProfileMenu';
import NotifScreenAdminPush from "../components/Notification/AdminPush/NotificationMenuAdmin";
import NotifScreenProgress from '../components/Notification/Progress/NotificationMenuProgress';
import NotifScreenRequestIzin from '../components/Notification/Progress/NotificationRequestIzin';
import NotifScreenHistory from '../components/Notification/History/NotificationMenuHistory';
import SideBarHome from '../components/Home/Sidebar/sideBar'
import Bantuan from '../components/Bantuan/bantuan'
import Tentang from '../components/Bantuan/tentang'
import Pengaturan from '../components/Bantuan/pengaturan'
import NotifSearch from '../components/Notification/NotificationSearch'
import NotifDetailAttandance from '../components/Notification/NotificationDetailAttandance/notifDetailAttandece'
import NotifDetailSimpok from '../components/Notification/NotificationDetailSimpok/notifDetailSimpok'

//test
import Test from '../components/Home/message/test'



//NOTIF SUB MENU
import NotifAttendanceScreen from '../components/Notification/NotificationAttendance';
import NotifSimpokScreen from '../components/Notification/NotificationSimpok';

//SIMPOK
import SimpokMenuScreen from '../components/Home/Simpok/SimpokMenu';
import SimpokInputP2KScreen from '../components/Home/Simpok/SimpokInputP2K';
import SimpokInputPK2SScreen from '../components/Home/Simpok/SimpokInputPK2S';

//SOYAL
import AttendanceMenuScreen from '../components/Home/Attendance/AttendanceMenu';
import AttendanceMenuCheckInScreen from '../components/Home/Attendance/AttendanceMenuCheckIn';
import CheckInScreen from '../components/Home/Attendance/AttendanceCheckIn';
import CheckOutScreen from '../components/Home/Attendance/AttendanceCheckOut';
import IzinScreen from '../components/Home/Attendance/AttendanceIzin';
import CutiScreen from '../components/Home/Attendance/AttendanceCuti';

//LOGBOOK
import ViewLogbookScreen from '../components/Home/Logbook/ViewLogbook';
import AddLogbookScreen from '../components/Home/Logbook/AddLogbook';

//PLUGIN
import PlacePickerScreen from '../plugin/pluginPlacePicker';
import TopTabsNotification from './types/TopTabsNotification';
import TopTabsAttendance from './types/TopTabsAttendance';
import TopTabs from './types/TopTabs';

//MODAL
import ModalAlert from '../components/Modal/modalAlert';
import ModalLoadingGratika from '../components/Modal/modalLoadingGratika';
import ModalFilterDate from '../components/Modal/modalFilterDate';

// register all screens of the app (including internal ones)
export function registerScreens(store, Provider) {

  //test
  Navigation.registerComponent('example.test', () => Test, store, Provider);


  Navigation.registerComponent('example.sample', () => SampleScreen, store, Provider);
  Navigation.registerComponent('panelg.loginscreen', () => LoginScreen, store, Provider);
  Navigation.registerComponent('panelg.homemenuscreen', () => HomeMenuScreen, store, Provider);
  Navigation.registerComponent('panelg.profilemenuscreen', () => ProfileMenuScreen, store, Provider);
  Navigation.registerComponent('panelg.notifscreenadminpush', () => NotifScreenAdminPush, store, Provider);
  Navigation.registerComponent('panelg.notifscreenprogress', () => NotifScreenProgress, store, Provider);
  Navigation.registerComponent('panelg.notifscreenrequestizin', () => NotifScreenRequestIzin, store, Provider);
  Navigation.registerComponent('panelg.notifscreenhistory', () => NotifScreenHistory, store, Provider);
  Navigation.registerComponent('panelg.sidebar', () => SideBarHome, store, Provider);
  Navigation.registerComponent('panelg.bantuan', () => Bantuan, store, Provider);
  Navigation.registerComponent('panelg.tentang', () => Tentang, store, Provider);
  Navigation.registerComponent('panelg.pengaturan', () => Pengaturan, store, Provider);
  Navigation.registerComponent('panelg.notifsearch', () => NotifSearch, store, Provider);
  Navigation.registerComponent('panelg.notifDetailAttandance', () => NotifDetailAttandance, store, Provider);
  Navigation.registerComponent('panelg.notifDetailSimpok', () => NotifDetailSimpok, store, Provider);

  //NOTIF SUB MENU
  Navigation.registerComponent('panelg.notifscreen.attendance', () => NotifAttendanceScreen, store, Provider);
  Navigation.registerComponent('panelg.notifscreen.simpok', () => NotifSimpokScreen, store, Provider);
  
  //SIMPOK
  Navigation.registerComponent('panelg.simpok.menu', () => SimpokMenuScreen, store, Provider);
  Navigation.registerComponent('panelg.simpok.inputp2k', () => SimpokInputP2KScreen, store, Provider);
  Navigation.registerComponent('panelg.simpok.inputpk2s', () => SimpokInputPK2SScreen, store, Provider);

  //SOYAL
  Navigation.registerComponent('panelg.attendance.menu', () => AttendanceMenuScreen, store, Provider);
  Navigation.registerComponent('panelg.attendance.menucheckin', () => AttendanceMenuCheckInScreen, store, Provider);
  Navigation.registerComponent('panelg.attendance.checkin', () => CheckInScreen, store, Provider);
  Navigation.registerComponent('panelg.attendance.checkout', () => CheckOutScreen, store, Provider);
  Navigation.registerComponent('panelg.attendance.izin', () => IzinScreen, store, Provider);
  Navigation.registerComponent('panelg.attendance.cuti', () => CutiScreen, store, Provider);

  //LOGBOOK
  Navigation.registerComponent('panelg.logbook.menu', () => ViewLogbookScreen, store, Provider);
  Navigation.registerComponent('panelg.logbook.add', () => AddLogbookScreen, store, Provider);

  //PLUGIN
  Navigation.registerComponent('panelg.plugin.placepicker', () => PlacePickerScreen, store, Provider);
  Navigation.registerComponent('panelg.plugin.TopTabsNotification', () => TopTabsNotification, store, Provider);
  Navigation.registerComponent('panelg.plugin.TopTabsAttendance', () => TopTabsAttendance, store, Provider);
  Navigation.registerComponent('panelg.plugin.TopTabs', () => TopTabs, store, Provider);

  //MODAL
  Navigation.registerComponent('panelg.modal.alert', () => ModalAlert, store, Provider);
  Navigation.registerComponent('panelg.modal.loadingGratika', () => ModalLoadingGratika, store, Provider);
  Navigation.registerComponent('panelg.modal.filterDate', () => ModalFilterDate, store, Provider);
}

export function registerScreensVisibilityListener(){
  new ScreensVisibilityListener({
    
  }).register();
}
import React from 'react';
import {PixelRatio, Dimensions} from 'react-native';

var screen = Dimensions.get('window');

class TopTabsAttaendance extends React.Component {
  static navigatorStyle = {
    topTabTextColor: '#ffffff',
    topTabTextFontFamily: 'BioRhyme-Bold',
    selectedTopTabTextColor: '#ffffff',
    //navBarHeight: screen.height*0.046875,
    //topTabsHeight: screen.height*0.0390625,

    // Icons
    topTabIconColor: '#ffffff',
    selectedTopTabIconColor: '#ffffff',

    // Tab indicator
    selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
    selectedTopTabIndicatorColor: '#ffffff',

    //hide tab
    tabBarHidden: false
  };
}

export default TopTabsAttaendance;
import React from 'react';
import {PixelRatio, Dimensions, View, Text} from 'react-native';
import { Container, Header, Left, Body, Right, Title } from 'native-base';
import { iconsMapFa, iconsLoadedFa } from '../../config/app-icons-fa';

var screen = Dimensions.get('window');

class TopTabsNotification extends React.Component {
  static navigatorStyle = {
    topTabTextColor: '#ffffff',
    topTabTextFontFamily: 'BioRhyme-Bold',
    selectedTopTabTextColor: '#ffffff',
    //navBarHeight: screen.height*0.046875,
    //topTabsHeight: screen.height*0.0390625,

    // Icons
    topTabIconColor: '#ffffff',
    selectedTopTabIconColor: '#ffffff',

    // Tab indicator
    selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
    selectedTopTabIndicatorColor: '#ffffff',

    //hide tab
    tabBarHidden: false
  };

  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent = (event) => {
    if (event.type === 'NavBarButtonPress') {
      if(event.id === 'search'){
        this.goScreen()
      }
    }
  }

  goScreen() {
    console.log("ini masuk goScreen")
    const {navigator} = this.props;
    iconsLoadedFa.then(() => {
      navigator.push({
        screen: 'panelg.notifsearch',
        title: 'Leave Request Status',
        navigatorButtons: {
          leftButtons: [
            {
              icon: iconsMapFa['angle-left'],
              buttonColor: 'white',
              id: 'back',
            }
          ]
        },
        navigatorStyle: {
          navBarTextColor: 'white', // change the text color of the title (remembered across pushes)
          navBarBackgroundColor: 'red', // change the background color of the nav bar (remembered across pushes)
          navBarButtonColor: 'red', // change the button colors of the nav bar (eg. the back button) (remembered across pushes)
          navBarHidden: false, // make the nav bar hidden
          navBarComponentAlignment: 'center',
          navBarTitleTextCentered: true,
        },
        // passProps: {
        //   onUnmount: () => {
        //     console.log('Notif Detail dismissed');
        //     this.makeRemoteRequestProgress();
        //     this.makeRemoteRequestHistory();
        //   }
        // }
      });
    }).catch((error) => {
        console.log("Modul error");
        console.log("" + error);
    });
  }

  render () {
    return(
      <View style = {{backgroundColor : 'yellow', width : screen.width, height : screen.height, flex : 1}}>
        <Text>klkl</Text>
      </View>
    )
  }
}

export default TopTabsNotification;
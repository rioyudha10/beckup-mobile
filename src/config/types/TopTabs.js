import React from 'react';
import {PixelRatio} from 'react-native';

class TopTabs extends React.Component {
  static navigatorStyle = {
    topTabTextColor: '#ffffff',
    topTabTextFontFamily: 'BioRhyme-Bold',
    selectedTopTabTextColor: '#ffffff',

    // Icons
    topTabIconColor: '#ffffff',
    selectedTopTabIconColor: '#ffffff',

    // Tab indicator
    selectedTopTabIndicatorHeight: PixelRatio.get() * 2,
    selectedTopTabIndicatorColor: '#ffffff',

    //hide tab
    tabBarHidden: true
  };
}

export default TopTabs;
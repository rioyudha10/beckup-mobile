import React, { Component } from 'react';
import {StyleSheet, View, Text, Dimensions, Button, TouchableOpacity, ScrollView, Image, 
  AsyncStorage, Animated, Easing, ImageBackground, Picker, Keyboard, FlatList, TouchableHighlight} from 'react-native';
import { List, ListItem, SearchBar } from "react-native-elements";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {connect} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, bindActionCreators} from "redux";
import * as axios from 'axios';
import MapView from 'react-native-maps';
import Spinkit from 'react-native-spinkit';
import Spinner from 'react-native-loading-spinner-overlay';
import IconE from 'react-native-vector-icons/Entypo';
import IconFA from 'react-native-vector-icons/FontAwesome5';
import IconFM from 'react-native-vector-icons/MaterialIcons';
import * as appActions from '../redux/action/index';
import anim from '../images/bouncy_mapmaker.json';
import GeneralStyle from '../styles/GeneralStyle';
import Geolocation from 'react-native-geolocation-service';
import LottieView from 'lottie-react-native';
import {BoxShadow} from 'react-native-shadow';
import { iconsMapFa, iconsLoadedFa } from '../config/app-icons-fa';
import {openLightbox, openLightboxLoading} from '../components/Home/HomeNav';
import Modal from 'react-native-modal';
import DeviceInfo from 'react-native-device-info';
import Loading from 'react-native-whc-loading';
var screen = Dimensions.get('window');

const avatarP2KNotif = (<IconFM name="restore" size={28} color= "grey" />)

export class pluginPlacePicker extends Component {
    static propTypes = {
        provider: MapView.ProviderPropType,
    };

    constructor(props){
        super(props);
        this.state = {
            mapRegion: {
                latitude: 0.0,
                longitude: 0.0,
                latitudeDelta: 0.0018,
                longitudeDelta: 0.0018,
            },
            names: [
                {'name': 'Ben', 'id': 1},
                {'name': 'Susan', 'id': 2},
                {'name': 'Robert', 'id': 3},
                {'name': 'Mary', 'id': 4},
                {'name': 'Daniel', 'id': 5},
                {'name': 'Laura', 'id': 6},
                {'name': 'John', 'id': 7},
                {'name': 'Debra', 'id': 8},
                {'name': 'Aron', 'id': 9},
                {'name': 'Ann', 'id': 10},
                {'name': 'Steve', 'id': 11},
                {'name': 'Olivia', 'id': 12}
             ],
            loading: false,
            progress: [],
            history: [],
            search : [],
            page: 1,
            seed: 1,
            lastLat: null,
            lastLong: null,
            error: '',
            error_msg: '',

            //spinner & spinkit & modal
            visibleSpinner: false,
            isVisibleSpinkit: true,
            isModalVisible: false,
            isBack: true
        };
    }

    static navigatorStyle = {
        tabBarHidden: true,
        tabBarShowLabels: 'hidden',
    }

    setBadge(count){
        this.props.navigator.setTabBadge({
          badge: count
        });
      }

    makeRemoteRequestProgress = () => {
        this.refs.loading1.show();
        var data = '11002/0/1/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        axios.get(this.props.apiState.apiSimpok + data)
        .then(response => {
            console.log("NOTIF PROGRESS SINPOK PLUGIN = "+JSON.stringify(response.data.info));
            this.notifCheckProgress (response.data.info)
        }).catch((error) => {
          this.setState({ error, loading: false });
          console.log("Error gelondongan = "+error); //ini yang bener
        });
      };
    
      notifCheckProgress = (data) => {
        console.log("ini tmp" + JSON.stringify(data))
        let temp = [];
        data.map(ele=>{
          if (ele.type === "GSIMPOK-P2K"){
            return(
              temp.push(ele)
            )
          }
          return temp;
        })
        
        this.setState({
          progress: temp,
          search : temp,
        //   banyakPesanProgress: countBadge,
        })
      }
    
      notifCheckHistory = (data) => {
        let temp = [];
        data.map(ele=>{
          if (ele.type === "GSIMPOK-P2K"){
            return(
              temp.push(ele)
            )
          }
          return temp;
        })
        // console.log("ini tmp" + JSON.stringify(temp))
        this.setState({
          history: temp,
          loading: false,
          error : null,
          refreshing: false,
          banyakPesanHistory: countBadge,
        })
      }
    
      makeRemoteRequestHistory = () => {
        var data = '11002/0/2/'+this.props.profileState.nik+'/'+this.props.apiState.keyApiSimpok;
        console.log("api data " + this.props.apiState.apiSimpok + data)
        axios.get(this.props.apiState.apiSimpok + data)
        .then(response => {
          console.log("NOTIF HISTORY SINPOK PLUGIN = "+JSON.stringify(response.data));
          var countBadge;
          if(response.data.rst == 0){
            countBadge = 0;
          } else{
            countBadge = Object.keys(response.data.info).length;
          }
          console.log("countBadge before = "+countBadge);
          //this.props.actions.setNotifCount(countBadge);
          countBadge = countBadge + this.props.notifState.notif_count;
          console.log("countBadge after = "+countBadge);
          this.props.navigator.setTabBadge({
            badge: countBadge
          });
    
          this.notifCheckHistory(response.data.info, countBadge);
          this.setState({
            banyakPesanHistory: countBadge,
            // history : response.data.info,
            error: response.data.info || null,
            loading: false,
            refreshing: false
          });
        }).catch((error) => {
          this.setState({ error, loading: false });
          console.log("Error gelondongan = "+error); //ini yang bener
        });
        this.props.actions.setNotifRemoteHistory(true);
      };
    
    setBadge(count){
        this.props.navigator.setTabBadge({
            badge: count
        });
    }

    componentDidMount(){
        /*
        this.setState(({mapRegion}) => ({mapRegion: {
            ...mapRegion,
            latitude: this.props.locationState.lat,
            longitude: this.props.locationState.long,
        }}));
        */
        console.log("Brand = "+DeviceInfo.getBrand());
        // this.refs.loading1.show();
        this.findLocation();
        this.makeRemoteRequestProgress();
        this.makeRemoteRequestHistory();
    }

    componentWillUnmount(){
        console.log("modal unmount");
        //this.onClose();
        //this.props.onUnmount();
    }

    onRegionChange(region){
        console.log("region change nih = "+JSON.stringify(region));
        /*
        this.setState({
          lastLat: region.latitude,
          lastLong: region.longitude
        });
        */
    }

    findLocation(){
        Geolocation.getCurrentPosition(
            (position) => {
                console.log("STRINGIFY = "+JSON.stringify(position));
                console.log("latitude = "+position.coords.latitude);
                console.log("longitude = "+position.coords.longitude);
                this.setState(({mapRegion}) => ({mapRegion: {
                    ...mapRegion,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }}));
                this.onRegionChange(position.coords.latitude, position.coords.longitude),
                this.props.actions.setLocation(position.coords.latitude, position.coords.longitude);
                this.refs.loading1.close();
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
                this.refs.loading1.close();
                this.setState({
                    error_msg: error.message
                });
                const data = {
                    act: false,
                    message: this.state.error_msg,
                    confirm: 'OK',
                    status: 0
                  }
                  openLightbox(data, 0, this.props.navigator);
            },
            { enableHighAccuracy: true, timeout: 5000, maximumAge: 2000 }
        );
    }

    _toggleModal(){
        this.setState({ isModalVisible: !this.state.isModalVisible });
        console.log("toggle modal");
    }

    onRegionChangeApi(region, latitude, longitude) {
        this.setState({
            mapRegion: region,
            // If there are no new values set the current ones
            latitude: latitude || this.state.latitude,
            longitude: longitude || this.state.longitude
        });
    }

    checkBrand(){
        var troubleBrand = false;
        for (var i = 0; i < this.props.apiState.brandPhone.length; i++) {
            if(DeviceInfo.getBrand() == this.props.apiState.brandPhone[i]){
                troubleBrand = true;
            }
        }
        return troubleBrand;
    }

    onRegionChangeComplete(region){
        var lat,long;
        if(region.longitude != '0'){
            lat = region.latitude;
            long = region.longitude;
            console.log("region change complete nih = "+JSON.stringify(region));
            if(this.checkBrand()){
                this.setState(({mapRegion}) => ({mapRegion: {
                    ...mapRegion,
                    latitude: lat,
                    longitude: long,
                }}));
            } else{
                this.props.actions.setSimpokDestination(lat,long);
            }
            
            // axios.get(this.props.apiState.apiReverseLocation+ region.latitude +','+ region.longitude +'&key='+this.props.apiState.keyGoogleApi) // be sure your api key is correct and has access to the geocode api
            // .then(response => {
            //     /*
            //     console.log(response);
            //     this.setState({
            //         place: response.data.results[0].formatted_address, // access from response.data.results[0].formatted_address
            //     });
            //     */
            //     this.GooglePlacesRef.setAddressText(response.data.results[0].formatted_address);
            // }).catch((error) => { // catch is called after then
            //     this.setState({ 
            //         error: error.message,
            //     });
            // });
        }
        /*
        this.setState({
          //lastLat: region.latitude,
          //lastLong: region.longitude
          error_msg: 'berhasil'
        });
        */

        //console.log("last lat = "+this.state.lastLat);
        //console.log("last long = "+this.state.lastLong);
        //this.setRegion("berhasil");
        //this.onRegionChangeApi(region, region.latitude, region.longitude);
    }

    setRegion(param){
        this.setState({
          error_msg: param        
        });
        //console.log("state error = "+this.state.error);
    }

    onPanDragStop(test){
        console.log("drag view = "+test);
    }

    pickLocation(){
        console.log("masuk : " + JSON.stringify(this.props.simpokState ))
        const {navigator} = this.props;
        var scr, ttl;
        var isGo = 1;
        // if(this.props.simpokState.simpok_type == 'P2K'){
            scr = 'panelg.simpok.inputp2k';
            ttl = 'Input P2K';
            if(this.checkBrand()){
                this.props.actions.setSimpokDestination(this.state.mapRegion.latitude,this.state.mapRegion.longitude);
            }
            iconsLoadedFa.then(() => {
                navigator.push({
                    screen: scr,
                    title: ttl,
                    title: ttl,
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: iconsMapFa['angle-left'],
                                buttonColor: 'white',
                                id: 'back',
                            }
                        ]
                    },
                    navigatorStyle: {
                        // navBarTextFontSize: 16,
                        navBarTextColor: '#FFFFFF',
                        navBarBackgroundColor: 'red',
                        // navBarTextFontFamily: styleConst.fontFamily,
                        navBarTitleTextCentered: true,
                        navBarButtonColor: '#FFFFFF'
                    },
                });
            }).catch((error) => {
                console.log("Modul error");
                console.log("" + error);
            });
        // } 
        // else if(this.props.simpokState.simpok_type == 'PK2S'){
        //     scr = 'panelg.simpok.inputpk2s';
        //     ttl = 'Input PK2S';
        //     var urlMatrix = '';
        //     if(this.checkBrand()){
        //         this.props.actions.setSimpokDestination(this.state.mapRegion.latitude,this.state.mapRegion.longitude);
        //         console.log("this.props.simpokState.simpok_lat = "+this.props.simpokState.simpok_lat);
        //         console.log("this.props.simpokState.simpok_long = "+this.props.simpokState.simpok_long);
        //         urlMatrix = this.props.apiState.apiDistanceMatrix+"origins="+this.props.locationState.lat_gratika+','+this.props.locationState.long_gratika+'&destinations='+this.state.mapRegion.latitude+','+this.state.mapRegion.longitude+'&key='+this.props.apiState.keyGoogleApi;
        //     } else{
        //         urlMatrix = this.props.apiState.apiDistanceMatrix+"origins="+this.props.locationState.lat_gratika+','+this.props.locationState.long_gratika+'&destinations='+this.props.simpokState.simpok_lat+','+this.props.simpokState.simpok_long+'&key='+this.props.apiState.keyGoogleApi;
        // }
        //     //cek jarak lokasi tujuan
        //     axios.get(urlMatrix)
        //     .then(response => {
        //         if(response.data.rows[0].elements[0].distance.value > 150000){
        //             this.setState({
        //                 error_msg: "Distance Can't Over 150km",
        //                 isBack: false
        //             });
        //             isGo = 0;
        //             this._toggleModal();                    
        //         } else{
        //             iconsLoadedFa.then(() => {
        //                 navigator.push({
        //                     screen: scr,
        //                     title: ttl,
        //                     navigatorButtons: {
        //                         leftButtons: [
        //                             {
        //                                 icon: iconsMapFa['angle-left'],
        //                                 buttonColor: 'white',
        //                                 id: 'back',
        //                             }
        //                         ]
        //                     }
        //                 });
        //             }).catch((error) => {
        //                 console.log("Modul error");
        //                 console.log("" + error);
        //             });
        //         }
        //     }).catch((error) => { // catch is called after then
        //         this.setState({
        //             error_msg: error.message
        //         });
        //         isGo = 0;
        //         this._toggleModal();
        //         return;
        //     });
        // }
    }

    onLottieLoad = () => {
        console.log('play lottie');
        this.animation.play();
    }

    _markerLottie(){
        return(
            <LottieView
                onLayout={this.onLottieLoad}
                ref={animation => {
                    this.animation = animation;
                }}
                style={GeneralStyle.markerLottieAttendance}
                source={anim}
            />
        );
    }

    closeBack(){
        if(this.state.isBack == true){
            const {navigator} = this.props;
            this.props.navigator.pop({
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
                animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
            });
        } else{
            this.setState({ isModalVisible: false, isBack: true });
        }
    }

    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round containerStyle={{ opacity : 0.5}}  onChangeText={text => this.searchFilterFunction(text)}     value={this.state.value}/>;
    };

    searchFilterFunction = (text) => {
        this.setState({
          value: text,
        });
    
        const newData = this.state.search.filter(item => {
          const itemData = `${item.dari.toUpperCase()}`;
          const textData = text.toUpperCase();
    
          return itemData.indexOf(textData) > -1;
        });
        this.setState({
          progress : newData,
        });
      }

    render() {
        const shadowOpt = {
			width: screen.width*0.97, 
            height: screen.height*0.5,
			color:"#000",
			border:3,
			//radius:10,
			opacity:0.2,
			x:0,
            y:-3,
            side: 'top'
			//style:{marginVertical:5}
        }
        const markerLottie = this._markerLottie();
        console.log("ini progress plugin = " + JSON.stringify(this.state.progress))
        return(
            <ScrollView style={styles.container}  keyboardShouldPersistTaps="always">
                <Loading ref='loading1' image = {require('../components/image/loading.png')}/>
                <View style = {{backgroundColor : 'green', height: screen.height*0.5}}>
                    <MapView
                        style={ styles.map }
                        region={this.state.mapRegion}
                        //onRegionChange={this.onRegionChange.bind(this)}
                        //onPanDrag={this.onPanDragStop.bind(this)}
                        onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
                        showsMyLocationButton={true}
                        provider='google'
                    >
                    </MapView>
                    <View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent'}}>
                        {markerLottie}
                    </View>
                    <View style={{position: 'absolute', width: screen.width*0.97, height: screen.height*0.5 }}>
                        <GooglePlacesAutocomplete
                        ref={(instance) => { this.GooglePlacesRef = instance }}
                        placeholder='Cari Lokasi Tujuan'
                        minLength={2}
                        autoFocus={false}
                        returnKeyType={'default'}
                        fetchDetails={true}
                        query={{
                            // available options: https://developers.google.com/places/web-service/autocomplete
                            key: this.props.apiState.keyGoogleApi,
                            components: 'country:id',
                            language: 'id', // language of the results
                            types: 'establishment' // default: 'geocode'
                        }}
                        onPress={(data, details = null) => {
                            const region = {
                                latitude: details.geometry.location.lat,
                                longitude: details.geometry.location.lng,
                                latitudeDelta: 0.0018,
                                longitudeDelta: 0.0018
                            };
                            this.onRegionChangeApi(region, region.latitude, region.longitude);
                        }}
                        
                        styles={{
                            textInputContainer: {
                                width: screen.width,
                                paddingLeft : 10,
                                paddingRight : 10,
                                backgroundColor : '#C0C0C0',
                                opacity: 0.8
                            },
                            description: {
                                fontWeight: 'bold'
                            },
                            predefinedPlacesDescription: {
                                color: '#1faadb'
                            },
                            // textInputContainer: {
                            //     backgroundColor: 'rgba(0,0,0,0)',
                            //     borderTopWidth: 0,
                            //     borderBottomWidth:0,
                            //     height: 50
                            // },
                            textInput: {
                                marginLeft: 0,
                                marginRight: 0,
                                color: '#5d5d5d',
                                fontSize: 16
                            },
                            // predefinedPlacesDescription: {
                            //     color: 'yellow'
                            // },
                            listView: {
                                backgroundColor: 'white'
                            }
                        }}
                        //renderRightButton={() => <Text>X</Text>}
                        currentLocation={false}
                    />
                    </View>
                </View>
                <View style = {{backgroundColor : 'white', height: screen.height*0.4}}>
                    {/* {this.renderHeader()} */}
                    <ScrollView>
                    <FlatList
                        data={this.state.progress}
                        renderItem={({ item }) => (
                            <ListItem
                            roundAvatar
                            component={TouchableHighlight}
                            title={`${item.dari}`}
                            subtitle={`${item.type}: ${item.waktu}`}
                            avatar={`${item.type}` == "GSIMPOK-P2K" ? avatarP2KNotif : console.log("ini gambar")}
                            containerStyle={{ borderBottomWidth: 0 }}
                            // onPress={() => {this.bukaNotif(`${item.id}`,`${item.type}`,`${item.jns}`,`${item.dari}`,`${item.dari_nik}`,`${item.id_type}`)}}
                            />
                        )}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.renderSeparator}
                        //ListHeaderComponent={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={50}
                    />
                    {/* {
                        this.state.progress.map((item, index) => (
                            <View key = {item.id} style = {styles.item}>
                                <Text>{item.dari}</Text>
                            </View>
                        ))
                    } */}
                    </ScrollView>
                    <View style={{
                        position: 'absolute',
                        top: screen.height*0.33,
                        left: screen.width*0.85,
                        width: 100,
                        height: 100
                    }}>
                        <TouchableOpacity 
                            style={{
                                width: 50,
                                height: 50,
                                backgroundColor: 'red',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 25
                            }}
                            onPress={this.pickLocation.bind(this)}
                        >
                            <IconFA name="paste" size={30} color='white'/>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

export class Message extends Component {
    render(){
      return (
        <View style={{alignItems: 'center', justifyContent: 'center', }}>
          <Text></Text>
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  map: {
    // position: 'absolute',
    // top: 50,
    // left: 0,
    // right: 0,
    // bottom: 0,
    flex: 1,
    width: screen.width,
    height: screen.height*0.5,
  },
  spinner: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    flex: 1,
    marginTop: 10,
    backgroundColor: 'orange',
    marginLeft: 10,
    marginRight: 10,
    textAlign: 'center',
    fontSize: 20,
    paddingTop: 70,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 30,
    margin: 2,
    borderColor: '#2a4944',
    borderWidth: 1,
    backgroundColor: '#d2f7f1'
 }
  
});

function mapStateToProps(state, ownProps) {
	return {
        rootState: state.root,
        apiState: state.api,
        profileState: state.profile,
        spinkitState: state.spinkit,
        locationState: state.location,
        simpokState: state.simpok
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(appActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(pluginPlacePicker);
import { StyleSheet, Dimensions } from 'react-native';
var screen = Dimensions.get('window');

export default StyleSheet.create({
    imageBackground:{
        width: '100%', 
        height: '100%',
        backgroundColor : 'white'
    },
    //=====================batas atas swiper=========================//
    swiperWrapper: {
    },
    swiperDot:{
        backgroundColor: 'rgba(0,0,0,.2)', 
        width: 5, 
        height: 5, 
        borderRadius: 4, 
        marginLeft: 5, 
        marginRight: 5, 
        marginTop: 3, 
        marginBottom: 3
    },
    swiperActiveDot:{
        backgroundColor: 'red', 
        width: 8, 
        height: 8, 
        borderRadius: 4, 
        marginLeft: 5, 
        marginRight: 5, 
        marginTop: 3, 
        marginBottom: 3
    },
    swiperSlide: {
        flex: 1,
        height: screen.height*0.5,
        justifyContent: 'center',
        backgroundColor: 'transparent'
        //backgroundColor: 'blue'
    },
    swiperImage: {
        width: screen.width,
        flex: 1
    },
    swiperTitleContainer:{
        width: screen.width,
        height: screen.height*0.04,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: screen.height*0.35,
        borderRadius: 4,
    },
    swiperTitleText:{
        fontSize: 16,
        color: 'white'
    },
    //====================batas bawah swiper=========================//
    //=================batas atas modal loading======================//
    modalLoading: {
        overflow: 'hidden',
        //height: screen.height * 0.3,
        //backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    lottieLoading: {
        width: 300,
        height: 300,
    },
    //=================batas bawah modal loading======================//
    //===================batas atas modal alert=======================//
    modalParent: {
        overflow: 'hidden',
        //height: screen.height * 0.3,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalContentAlert: {
        height: screen.height * 0.15,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    textModalContentAlert: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    modalButtonAlert:{
        flex: 0.5, 
        justifyContent: 'center', 
        alignItems: 'center', 
        height: screen.height*0.05,
        width: screen.width*0.4,
        borderRadius: 10,
        marginHorizontal: 10
    },
    textModalButtonAlert:{
        color: 'red', 
        marginTop: 10,
    },
    //=================batas bawah modal alert=========================//
    //===================batas atas menu home==========================//
    menuHomeContainer:{
        height: 100, 
        paddingHorizontal: 10, 
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom : 10,
        marginTop : 10,
        backgroundColor : '#FFFFFF'
    },
    menuHomeContainerCheckBox:{
        height: 100, 
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor : 'blue',
        paddingLeft : 10,
        paddingRight : 10
    },
    underlineNotification:{
        height: 100, 
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor : 'blue',
        paddingLeft : 10,
        paddingRight : 10
    },
    menuHomeNotif:{
        height: 120, 
        //flex: 1,
        flexDirection: 'column',
        backgroundColor : 'green',
        paddingVertical : 10
        // borderBottomWidth: 2,
        // borderColor : 'grey'
    },
    menuHomeNotifIzin:{
        height: 50, 
        flex: 1,
        flexDirection: 'column',
        backgroundColor : 'green'
    },
    menuHomeNotifRow:{
        height: 28, 
        flex: 1,
        // paddingLeft : 10,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor : 'white'
    },
    menuHomeNotifRowLine:{
        height: 28, 
        flex: 1,
        // paddingLeft : 10,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor : 'white',
        borderBottomWidth: 2,
        borderColor : 'grey'
    },
    menuContainerNotifRow:{
        height: 140, 
        flex: 1,
        // paddingLeft : 10,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor : 'white'
    },
    menuHomeContainerDate:{
        height: 30, 
        paddingHorizontal: 10, 
        paddingVertical: 10,
        //flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor : 'white',
        borderBottomWidth: 2,
        borderColor : 'grey'
    },
    buttonHome:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        height: screen.height*0.1, 
        backgroundColor: 'white',
        borderRadius: 10
    },

    buttonHomeMenu:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        height: screen.height*0.1, 
        backgroundColor: 'red',
        borderRadius: 10
    },

    buttonHomeCheck:{
        flex: 1, 
        paddingHorizontal : 150,
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderColor : 'grey',
    },

    buttonIconContainer:{
        width: screen.width*0.35, 
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    buttonIconContainerCoba:{
        width: screen.width*0.35, 
        flex: 1, 
        backgroundColor : 'orange'
    },
    textMenuButtonHome:{
        color: '#E31F26', 
        fontSize : 10,
        //fontWeight: 'bold'
    },
    textMenuButtonHomeMenu:{
        color: '#FFFFFF', 
        //fontWeight: 'bold'
    },
    textDate:{
        color: 'black', 
        fontWeight: 'bold',
        fontSize: 15
    },
    textMenuButtonHomeCheck:{
        color: 'black', 
        fontWeight: 'bold',
        fontSize: 20
    },
    textSmallMenuButtonHome:{
        color: 'grey',  
        fontSize: 8,
        paddingHorizontal : 5,
        paddingVertical : 5,
        alignItems : 'center',
        justifyContent : 'center'
    },
    textNotif:{
        color: 'grey', 
        fontWeight: 'bold',
        fontSize: 15,
        paddingLeft: 10
    },
    textNotifLine:{
        color: 'grey', 
        fontWeight: 'bold',
        fontSize: 15,
        paddingLeft: 10,
        borderBottomWidth: 2,
        borderColor : 'grey',
    },
    separatorButtonHomeSpace:{
        width: 30
    },
    separatorContainerVertical:{
        marginTop: 45
    },
    
    //==================batas bawah menu home==========================//
    //==============batas atas modal change password===================//
    modalParentChangePassword:{
        overflow: 'hidden',
        backgroundColor: 'white',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    textTitleChangePassword:{
        fontSize: 16,
        fontWeight: 'bold',
    },
    modalTitleChangePassword:{
        marginTop: 20,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalContentChangePassword: {
        backgroundColor: 'gray',
        borderRadius: 8,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalButtonContainerChangePassword: {
        alignItems: 'center', 
        justifyContent: 'center',
        height: 50,
        marginBottom: 10,
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    modalButtonChangePassword: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        height: 30, 
        backgroundColor: '#E31F26',
        borderRadius: 10,
        marginHorizontal: 10
    },
    //==============batas bawah modal change password==================//
    //==================batas atas attendance menu=====================//
    containerParentAttendance:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: 'red'
    },
    mapContainerAttendance:{
        height: screen.height*0.9, 
        flex: 0.8,
        backgroundColor: 'red'

    },
    mapviewAttendance:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    mapCurrentLocationAttendanceContainer:{
        top:screen.height*0.3,
        left: screen.width*0.8,
        //right: screen.width,
        position: 'absolute',
    },
    mapCurrentLocationAttendanceButton:{
        width: screen.width * 0.05,
        height: screen.width * 0.05,
        borderRadius: screen.width * 0.05 * 0.5,
        width: 50,
        height: 50,
        borderRadius: 25,
        //backgroundColor: '#689DF6',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    mapPlacePicker:{
        top:screen.height*0.35,
        left: screen.width*0.45,
        //right: screen.width,
        position: 'absolute',
    },
    markerLottieAttendance:{
        width: screen.width*0.15,
        height: screen.width*0.15,
    },
    menuContainerAttendance:{
        //flex: 1, 
        paddingVertical: 5,
        height: screen.height*0.3,
        elevation: 10, 
        backgroundColor: 'white',
    },
    izinButton:{
        width: screen.width * 0.05,
        height: screen.width * 0.05,
        borderRadius: screen.width * 0.05 * 0.5,
        width: 50,
        height: 50,
        borderRadius: 25,
        //backgroundColor: '#689DF6',
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        // borderColor: 'black',
        // borderWidth: 2,
    },
    menuContainerCollumnAttendance:{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonContainerAttendance:{
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    buttonWidthAttendanceMenu:{
        width: screen.width*0.4
    },
    //=================batas bawah attendance menu=====================//
    //=================batas atas attendance check=====================//
    scrollContainerAttendance:{
        paddingLeft: 12, 
        paddingRight: 12,
    },
    iconCameraAttendance: {
        height: 150,
        width: screen.width,
        borderWidth: 1,
        borderColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageCameraAttendance:{
        height: 150,
        width: screen.width*0.4,
        backgroundColor : 'blue',
        alignItems : 'center',
        justifyContent : 'center'
    },
    imageCameraAttendance1:{
        height: 150,
        width: screen.width*0.4,
        backgroundColor : 'red',
        alignItems : 'center',
        justifyContent : 'center',
        marginLeft : 10
    },
    imageAttendance: {
        height: 150,
        width: screen.width*0.5,
    },
    textAreaContainerCheckIn:{
        alignItems : 'center',
    },
    textAreaContainerCheckOut:{
        flexDirection: 'row',
        paddingLeft : 20
    },
    buttonContainerAttendanceCheck:{
        justifyContent: 'center', 
        alignItems: 'center', 
        flex: 1,
        marginTop: 30,
    },
    buttonContainerDetail:{
        alignItems: 'flex-end', 
        flex: 1,
        marginTop: 20,
    },
    buttonSubmitAttendance:{
        backgroundColor: '#E31F26',
        //paddingVertical: 10,
        //marginBottom: 100
        width: screen.width*0.3,
        height: screen.height*0.06,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    buttonSubmitCuti:{
        backgroundColor: '#E31F26',
        //paddingVertical: 10,
        //marginBottom: 100
        width: screen.width*0.3,
        height: screen.height*0.06,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 2,
    },
    textButtonSubmitAttendance:{
        textAlign: 'center',
        textAlignVertical: 'center',
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 18
    },
    //=================batas bawah attendance check====================//

});
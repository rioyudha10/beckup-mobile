import { Platform, StatusBar} from 'react-native';

export const styleConst = {
    colorPrimary: '#0068A4',
    colorSecondary: '#0482FD',
    colorBackgroundWhite: '#FFFFFF',
    colorBackgroundGray: '#E9E9E9',
    colorBackgroundTab: '#F9F9F7',
    colorTextPrimary: '#000000',
    colorTextPlaceholder: '#000000',
    colorBackgroundSearch: '#E8E8EA',
    colorBorder: '#B3B3B3',
    colorButtonTab: '#A1A1A1',
    colorIconPhone: '#2CC24E',
    colorIconEmail: '#FFBD07',
    fontRegular: Platform.OS == 'android' ? 'SF Pro Text Regular' : 'System',
    fontBold: Platform.OS == 'android' ? 'SF Pro Text Bold' : 'System',
    fontHeavy: Platform.OS == 'android' ? 'SF Pro Text Heavy' : 'System',
    fontLight: Platform.OS == 'android' ? 'SF Pro Text Light' : 'System',
    fontMedium: Platform.OS == 'android' ? 'SF Pro Text Medium' : 'System',
    fontSemiBold: Platform.OS == 'android' ? 'SF Pro Text Semibold' : 'System',
    statusBarHeight: Platform.OS == 'android' ? 0 : 20,
    textErrorModalNetwork: 'Request Failed Network Error',
    textCompleteForm: 'Please Complete All Field'
}